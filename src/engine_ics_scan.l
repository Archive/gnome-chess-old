%{
/* 
 * Copyright (C) 1999 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <string.h>
#include "debug.h"
#include "engine_ics.h"
#include "engine_ics_scan.h"


#undef YY_INPUT
#define YY_INPUT(b,r,ms) (r=engine_ics_input(b, ms))

char text[255];
int in_prompt = 0;

%}

invalidusername \".*\"[ ]is[ ]not[ ]a[ ]registered[ ]name.
invalidpassword [Ii]nvalid[ ]+[Pp]assword

style		<12>
piece		[-prnbkqPRNBKQ]
board		({piece}{8}[ ]){8}

tf		[01]
prev		((-1)|[0-7])
num		\-?[0-9]+
rel		((-3)|(-2)|(-1)|(0)|(1)|(2))
name		[a-zA-Z0-9\-_()*.,]+
verb		[^ \t\n]+
time		\({num}:{num}\)
not		[^ \t\n]+

win		"1-0"
lose		"0-1"
draw		"1/2-1/2"
unknown         "*"
result		[{]Game.*[}].*({win}|{lose}|{draw}|{unknown})

ws              [\t\r ]*
%%
^{ws}login:	        {engine_ics_user (engine);}

^{ws}password: 	        {engine_ics_password (engine);}

{invalidusername} 	{engine_ics_invaliduser (engine);}

{invalidpassword} 	{engine_ics_invalidpassword (engine);}

.ics%			{engine_ics_prompt (engine, yytext);}

{result}		{engine_ics_result (engine, yytext);}

^{name}{ws}s-shouts.*\n  {engine_ics_output (engine, yytext, 1);}

^{name}[(]{num}[)]:.*\n  {engine_ics_output (engine, yytext, 2);}

^{name}{ws}kibitzes:.*\n {engine_ics_output (engine, yytext, 3);}

^{name}{ws}whispers:.*\n {engine_ics_output (engine, yytext, 4);}

{style}[ ]{board}(W|B)[ ]{prev}[ ]({tf}[ ]){4}({num}[ ]){2}({name}[ ]){2}{rel}[ ]({num}[ ]){7}{verb}[ ]{time}[ ]{not}[ ]{tf}.*\n {engine_ics_update_board (engine, yytext);}

.		{strncat (text, yytext, 1);}
\n		{strncat (text, yytext, 1);
		engine_ics_output (engine, text, -1);
		strcpy (text,"");}

%%

