/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-window-registry.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include "engine-window-registry.h"

GList *windows = NULL;

static gboolean
idle_quit_cb (gpointer data)
{
	gtk_main_quit ();

	return FALSE;
}

static void
window_destroy_cb (GtkWidget *widget, gpointer data)
{
	engine_window_registry_remove (ENGINE_WINDOW (widget));
	if (engine_window_registry_count () == 0)
		g_idle_add (idle_quit_cb, NULL);
}

static void
exit_cb (void)
{
	GList *l;
	
	for (l = windows; l != NULL; l = l->next) {
		g_signal_handlers_disconnect_matched (l->data, G_SIGNAL_MATCH_FUNC, 
						      0, 0, NULL, window_destroy_cb, NULL);
		gtk_widget_destroy (GTK_WIDGET (l->data));
	}	
	g_list_free (windows);
}

void
engine_window_registry_add (EngineWindow *window)
{
	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_ENGINE_WINDOW (window));

	if (windows == NULL)
		g_atexit (exit_cb);
	
	windows = g_list_append (windows, window);

	g_signal_connect (G_OBJECT (window), "destroy", 
			  G_CALLBACK (window_destroy_cb), NULL);
}

void 
engine_window_registry_remove (EngineWindow *window)
{
	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_ENGINE_WINDOW (window));

	windows = g_list_remove (windows, window);
}

int
engine_window_registry_count (void)
{
	return g_list_length (windows);
}

