/* 
 * Copyright (C) 1999 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG_NONE	0
#define DEBUG_NORMAL	1
#define DEBUG_VERBOSE	2

extern int debug;

#ifdef __GNUC__
void debug_print (int level, char *format, ...) __attribute__ (( __format__(printf,2,3)));
#else
void debug_print (int level, char *format, ...); 
#endif

#endif /* DEBUG_H */
