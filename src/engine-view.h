/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-view.h
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _ENGINE_VIEW_H_
#define _ENGINE_VIEW_H_

#include <gnome.h>
#include "game-view.h"

G_BEGIN_DECLS

#define ENGINE_TYPE_VIEW			(engine_view_get_type ())
#define ENGINE_VIEW(obj)			(GTK_CHECK_CAST ((obj), ENGINE_TYPE_VIEW, EngineView))
#define ENGINE_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), ENGINE_TYPE_VIEW, EngineViewClass))
#define ENGINE_IS_VIEW(obj)			(GTK_CHECK_TYPE ((obj), ENGINE_TYPE_VIEW))
#define ENGINE_IS_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), ENGINE_TYPE_VIEW))


typedef struct _EngineView        EngineView;
typedef struct _EngineViewPrivate EngineViewPrivate;
typedef struct _EngineViewClass   EngineViewClass;

struct _EngineView {
	GtkVBox parent;

	EngineViewPrivate *priv;
};

struct _EngineViewClass {
	GtkVBoxClass parent_class;
};



GtkType    engine_view_get_type                 (void);
GtkWidget *engine_view_new                      (void);
void       engine_view_add_game_view            (EngineView *view,
						 GameView   *game_view);
void       engine_view_remove_game_view         (EngineView *view,
						 GameView   *game_view);
void       engine_view_remove_current_game_view (EngineView *view);
void       engine_view_remove_all_game_views    (EngineView *view);
GameView * engine_view_get_current_game_view    (EngineView *view);
void       engine_view_add_info_view            (EngineView *view,
						 const char *text,
						 GtkWidget  *child);
void       engine_view_remove_info_view         (EngineView *view,
						 GtkWidget  *child);


G_END_DECLS

#endif

