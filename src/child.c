/* 
 * Copyright (C) 1999 Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "debug.h"
#include "child.h"

int 
start_child (char **arg, 
	     GIOChannel **read_chan, 
	     GIOChannel **write_chan, 
	     pid_t *childpid)
{
	gint in, out, err;
	
	if (!g_spawn_async_with_pipes (NULL, arg, NULL, G_SPAWN_SEARCH_PATH,
				       NULL, NULL, childpid, &in, &out, &err, NULL))
		return -1;
	
	*read_chan = g_io_channel_unix_new (out);
	*write_chan = g_io_channel_unix_new (in);

	return *childpid;
}

void 
write_child (GIOChannel *write_chan, char *format, ...) 
{
	GIOError err;
	va_list ap;
	char *buf;
	int len;	

	va_start (ap, format);

	buf = g_strdup_vprintf (format, ap);

	err = g_io_channel_write (write_chan, buf, strlen (buf), &len);
	if (err != G_IO_ERROR_NONE)
		g_warning ("Writing to child process failed");

	debug_print (DEBUG_NORMAL, buf);  

	va_end (ap);

	g_free (buf);
}

/* Kill Child */
int 
stop_child (pid_t childpid) 
{
	if (childpid && kill (childpid, SIGTERM) ) { 
		debug_print (DEBUG_NONE,"Failed to kill child!\n");
		return 1;
	}

	waitpid(childpid, NULL, 0);

	return 0;
}
