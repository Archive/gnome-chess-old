/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-pgn.h
 *
 * Copyright (C) 2001  JP Rosevear
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _ENGINE_PGN_H_
#define _ENGINE_PGN_H_

#include <gtk/gtk.h>
#include "engine.h"

G_BEGIN_DECLS

#define ENGINE_TYPE_PGN		   (engine_pgn_get_type ())
#define ENGINE_PGN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ENGINE_TYPE_PGN, EnginePgn))
#define ENGINE_PGN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), ENGINE_TYPE_PGN, EnginePgnClass))
#define ENGINE_IS_PGN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ENGINE_TYPE_PGN))
#define ENGINE_IS_PGN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ENGINE_TYPE_PGN))


typedef struct _EnginePgn        EnginePgn;
typedef struct _EnginePgnPrivate EnginePgnPrivate;
typedef struct _EnginePgnClass   EnginePgnClass;

struct _EnginePgn {
	Engine parent;

	EnginePgnPrivate *priv;
};

struct _EnginePgnClass {
	EngineClass parent_class;
};



GType    engine_pgn_get_type (void);
GObject *engine_pgn_new      (const char *filename);

G_END_DECLS

#endif
