/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* board-window.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <config.h>
#include "engine-view.h"

static void class_init (EngineViewClass *klass);
static void init (EngineView *view);
static void destroy (GtkObject *object);

static void title_cb (GtkWidget *widget, const char *title, gpointer data);
static void switch_page_cb (GtkNotebook *nb, GtkNotebook *page, gint page_num, gpointer user_data);

enum columns {
	COL_ICON,
	COL_TITLE,
	COL_COUNT
};

static GnomeAppClass *parent_class = NULL;

struct _EngineViewPrivate {
	GtkNotebook *view_nb;
	GtkNotebook *info_nb;

	gint num;
};

GtkType
engine_view_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static const GtkTypeInfo info =
			{
				"EngineView",
				sizeof (EngineView),
				sizeof (EngineViewClass),
				(GtkClassInitFunc) class_init,
				(GtkObjectInitFunc) init,
				/* reserved_1 */ NULL,
				/* reserved_2 */ NULL,
				(GtkClassInitFunc) NULL,
			};

		type = gtk_type_unique (gtk_vbox_get_type (), &info);
	}

	return type;
}

static void
class_init (EngineViewClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (gtk_vbox_get_type ());
	
	object_class->destroy = destroy;
}

static void
init (EngineView *view)
{
	EngineViewPrivate *priv;
	GtkWidget *vpane;
	
	priv = g_new0 (EngineViewPrivate, 1);

	view->priv = priv;

	/* The game and info views */
	vpane = gtk_vpaned_new ();
	gtk_widget_show (vpane);
	
	priv->view_nb = GTK_NOTEBOOK (gtk_notebook_new ());
	gtk_widget_show (GTK_WIDGET (priv->view_nb));
	gtk_paned_pack1 (GTK_PANED (vpane), GTK_WIDGET (priv->view_nb), TRUE, TRUE);
	
	priv->info_nb = GTK_NOTEBOOK (gtk_notebook_new ());
	gtk_widget_show (GTK_WIDGET (priv->info_nb));
	gtk_paned_pack2 (GTK_PANED (vpane), GTK_WIDGET (priv->info_nb), FALSE, TRUE);

	gtk_signal_connect_after (GTK_OBJECT (priv->view_nb), "switch_page",
				  (GtkSignalFunc) switch_page_cb, view);

	gtk_container_add (GTK_CONTAINER (view), vpane);
}


static void 
destroy (GtkObject *object)
{
	EngineView *view;
	EngineViewPrivate *priv;

	view = ENGINE_VIEW (object);
	priv = view->priv;

	g_free (priv);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}



GtkWidget *
engine_view_new (void)
{
	GtkWidget *widget;
	
	widget = gtk_type_new (ENGINE_TYPE_VIEW);

	return widget;
}

void 
engine_view_add_game_view (EngineView *view, GameView *game_view)
{
	EngineViewPrivate *priv;
	GtkWidget *lbl;
	char *txt;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ENGINE_IS_VIEW (view));
	g_return_if_fail (game_view != NULL);
	g_return_if_fail (IS_GAME_VIEW (game_view));

	priv = view->priv;

	gtk_signal_connect (GTK_OBJECT (game_view), "title", (GtkSignalFunc) title_cb, view);

	priv->num++;
	txt = g_strdup_printf (_("Board %d"), priv->num);
	lbl = gtk_label_new (txt);
	g_free (txt);
	gtk_widget_show (lbl);

	gtk_widget_show (GTK_WIDGET (game_view));

	gtk_notebook_append_page (priv->view_nb, GTK_WIDGET (game_view), lbl);
	gtk_notebook_set_page (GTK_NOTEBOOK (priv->view_nb), -1);
//	board_window_set_game_menu (window);
}

static void
real_remove (EngineView *view, gint page_num)
{
	EngineViewPrivate *priv;

	priv = view->priv;
	
	gtk_notebook_remove_page (priv->view_nb, page_num);
}

void 
engine_view_remove_game_view (EngineView *view, GameView *game_view)
{
	EngineViewPrivate *priv;
	gint page_num;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ENGINE_IS_VIEW (view));
	g_return_if_fail (game_view != NULL);
	g_return_if_fail (IS_GAME_VIEW (game_view));

	priv = view->priv;
	
	page_num = gtk_notebook_page_num (priv->view_nb, GTK_WIDGET (game_view));
	real_remove (view, page_num);
}

void
engine_view_remove_current_game_view (EngineView *view)
{
	EngineViewPrivate *priv;
	gint page_num;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ENGINE_IS_VIEW (view));
	
	priv = view->priv;
	
	page_num = gtk_notebook_get_current_page (priv->view_nb);
	if (page_num < 0) 
		return;
	
	real_remove (view, page_num);
}

void
engine_view_remove_all_game_views (EngineView *view)
{
	EngineViewPrivate *priv;
	gint page_num;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ENGINE_IS_VIEW (view));

	priv = view->priv;
	
	while ((page_num = gtk_notebook_get_current_page (priv->view_nb)) >= 0)
		real_remove (view, page_num);
}

GameView *
engine_view_get_current_game_view (EngineView *view)
{
	EngineViewPrivate *priv;
	GtkWidget *game_view = NULL;
	gint page_num;
	
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (ENGINE_IS_VIEW (view), NULL);

	priv = view->priv;

	page_num = gtk_notebook_get_current_page (priv->view_nb);
	if (page_num >= 0) {
		game_view =  gtk_notebook_get_nth_page (GTK_NOTEBOOK (priv->view_nb), page_num);
		return GAME_VIEW (game_view);
	}
	
	return NULL;
}

void 
engine_view_add_info_view (EngineView *view, const char *text, GtkWidget *child)
{
	EngineViewPrivate *priv;
	GtkWidget *lbl;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ENGINE_IS_VIEW (view));
	g_return_if_fail (text != NULL);
	g_return_if_fail (child != NULL);
	g_return_if_fail (GTK_IS_WIDGET (child));

	priv = view->priv;

	lbl = gtk_label_new (text);
	gtk_widget_show (lbl);

	gtk_widget_show (child);

	gtk_notebook_append_page (priv->info_nb, child, lbl);
}

void 
engine_view_remove_info_view (EngineView *view, GtkWidget *child)
{
	EngineViewPrivate *priv;
	gint page_num;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ENGINE_IS_VIEW (view));
	g_return_if_fail (child != NULL);
	g_return_if_fail (GTK_IS_WIDGET (child));

	priv = view->priv;
	
	page_num = gtk_notebook_page_num (priv->info_nb, child);
	if (page_num < 0)
		return;
	
	gtk_notebook_remove_page (priv->info_nb, page_num);
}

static void
title_cb (GtkWidget *game_view, const char *title, gpointer data)
{
	EngineView *view = ENGINE_VIEW (data);
	EngineViewPrivate *priv;
	GtkWidget *lbl;
		
	priv = view->priv;
	
	lbl = gtk_notebook_get_tab_label (priv->view_nb, GTK_WIDGET (game_view));
	
	gtk_label_set_text (GTK_LABEL (lbl), title);
}

static void
switch_page_cb (GtkNotebook *nb, GtkNotebook *page,
		   gint page_num, gpointer user_data)
{
#if 0
	EngineView *view = user_data;
	
	board_window_set_game_menu (window);
#endif
}

