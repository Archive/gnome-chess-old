/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-local.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <config.h>
#include <gnome.h>
#include <ctype.h>
#include "debug.h"
#include "makros.h"
#include "child.h"
#include "notation.h"
#include "dialogs.h"
#include "game-view.h"
#include "board.h"
#include "server-term.h"
#include "engine_local.h"

/* Prototypes */
static void class_init (EngineLocalClass *klass);
static void init (EngineLocal *engine);
static void finalize (GObject *obj);
static void dispose (GObject *obj);

static void fill_menu (GtkMenuShell *shell, gint pos, gpointer data);
static void board_move_cb (GtkWidget *widget, Square from, Square to, gpointer data);
static void el_write_position (EngineLocal *engine);

static gboolean engine_local_cb     (GIOChannel *source,
				     GIOCondition condition,
				     gpointer data);
static gboolean engine_local_err_cb (GIOChannel *source,
				     GIOCondition condition,
				     gpointer data);


struct _EngineLocalPrivate 
{
	GameView *view;
	
	ServerTerm *analyse_info;
	gboolean analyse_info_added;

	GtkWidget *whitecb;
	GtkWidget *blackcb;
	GtkWidget *analyse;
	int computer;

	GIOChannel *read_chan;
	GIOChannel *write_chan;
	
	pid_t childpid;

	gint read_cb;
	gint err_cb;
};

static GObjectClass *parent_class = NULL;


GType
engine_local_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (EngineLocalClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof (EngineLocal),
			0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (TYPE_ENGINE, "EngineLocal", &info, 0);
	}

	return type;
}

static void
class_init (EngineLocalClass *klass)
{
	GObjectClass *object_class;
	EngineClass *engine_class;
	
	object_class = G_OBJECT_CLASS (klass);
	engine_class = ENGINE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->dispose = dispose;
	object_class->finalize = finalize;
}


static void
init (EngineLocal *local)
{
	EngineLocalPrivate *priv;

	priv = g_new0 (EngineLocalPrivate, 1);

	local->priv = priv;

	priv->computer = BLACK;

	priv->view = GAME_VIEW (game_view_new (fill_menu, local));

	board_set_white_lock (BOARD (priv->view->board), BOARD_LOCK_NONE);
	board_set_black_lock (BOARD (priv->view->board), BOARD_LOCK_COMPLETE);

	g_signal_connect (G_OBJECT (priv->view->board), "move",
			  G_CALLBACK (board_move_cb), local);	

	priv->analyse_info = SERVER_TERM (server_term_new ());
	server_term_hide_prompt (priv->analyse_info);
}


static void
dispose (GObject *obj) 
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	
	engine = ENGINE_LOCAL (obj);
	priv = engine->priv;

	g_source_remove(priv->read_cb);
	g_source_remove(priv->err_cb);
	
	g_io_channel_close (priv->read_chan);
	g_io_channel_unref (priv->read_chan);

	g_io_channel_close (priv->write_chan);
	g_io_channel_unref (priv->write_chan);

	stop_child (priv->childpid);
	
	G_OBJECT_CLASS (parent_class)->dispose (obj);
}

static void
finalize (GObject *obj) 
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	
	engine = ENGINE_LOCAL (obj);
	priv = engine->priv;

	g_free (priv);
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}



GObject *
engine_local_new (char **arg)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;

	engine = g_object_new (ENGINE_TYPE_LOCAL, NULL);

	priv = engine->priv;

	engine_view_add_game_view (engine_get_engine_view (ENGINE (engine)), GAME_VIEW (priv->view));

	start_child (arg, &priv->read_chan,
		     &priv->write_chan, &priv->childpid);
	
	priv->read_cb = g_io_add_watch (priv->read_chan, G_IO_IN,
					engine_local_cb, engine);
	priv->err_cb = g_io_add_watch (priv->read_chan, G_IO_HUP,
				       engine_local_err_cb, engine);

	write_child (priv->write_chan, "xboard\n");
	/*	write_child (priv->write_chan, "output long\n"); */
	write_child (priv->write_chan, "post\n");
	write_child (priv->write_chan, "easy\n");
	write_child (priv->write_chan, "level 60 2 0\n");

	return G_OBJECT (engine);
}

static void
show_analyse_info (EngineLocal *engine)
{
	EngineLocalPrivate *priv;
	
	priv = engine->priv;

	if (priv->analyse_info_added)
		return;

	engine_view_add_info_view (engine_get_engine_view (ENGINE (engine)), 
			       "Analysis", GTK_WIDGET (priv->analyse_info));
	priv->analyse_info_added = TRUE;
}

static void
hide_analyse_info (EngineLocal *engine)
{
	EngineLocalPrivate *priv;
	
	priv = engine->priv;

	if (!priv->analyse_info_added)
		return;

	engine_view_remove_info_view (engine_get_engine_view (ENGINE (engine)),
				      GTK_WIDGET (priv->analyse_info));
	priv->analyse_info_added = FALSE;
	
}

static void
el_menu_set_color (EngineLocal *engine, gshort color)
{
	EngineLocalPrivate *priv;
	Board *board;
	Position *pos;
	
	priv = engine->priv;
	board = BOARD (priv->view->board);

	priv->computer = color;

	switch (priv->computer) {
	case WHITE:
		GTK_CHECK_MENU_ITEM (priv->whitecb)->active = TRUE;
		GTK_CHECK_MENU_ITEM (priv->blackcb)->active = FALSE;
		GTK_CHECK_MENU_ITEM (priv->analyse)->active = FALSE;
		break;
	case BLACK:
		GTK_CHECK_MENU_ITEM (priv->whitecb)->active = FALSE;
		GTK_CHECK_MENU_ITEM (priv->blackcb)->active = TRUE;
		GTK_CHECK_MENU_ITEM (priv->analyse)->active = FALSE;
		break;
	case NONE:
		GTK_CHECK_MENU_ITEM (priv->whitecb)->active = FALSE;
		GTK_CHECK_MENU_ITEM (priv->blackcb)->active = FALSE;
		GTK_CHECK_MENU_ITEM (priv->analyse)->active = TRUE;
		write_child (priv->write_chan, "analyze\n");
		break;
	}
	
	board_set_flip (board, (color == WHITE));

	pos = board_get_position (board);
	if (position_get_color_to_move (pos) == priv->computer)
		write_child (priv->write_chan, "go\n");
}

static void
el_menu_computer_white (GtkWidget *widget, gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	
	engine = ENGINE_LOCAL (data);
	priv = engine->priv;
	
	if (priv->computer == WHITE)
		return;
	
	el_menu_set_color (engine, WHITE);
	
	hide_analyse_info (engine);
}

static void
el_menu_computer_black (GtkWidget *widget, gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	
	engine = ENGINE_LOCAL (data);
	priv = engine->priv;

	if (priv->computer == BLACK)
		return;

	el_menu_set_color (engine, BLACK);
	
	hide_analyse_info (engine);
}

static void
el_menu_analyse_mode (GtkWidget *widget, gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;

	engine = ENGINE_LOCAL (data);
	priv = engine->priv;

	if (priv->computer == NONE)
		return;

	el_menu_set_color (engine, NONE);

	show_analyse_info (engine);
}

static void
el_menu_level (GtkWidget *widget, gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	gint moves = 60;
	gint level_time = 2;
	
	engine = ENGINE_LOCAL (data);
	priv = engine->priv;
	
	if (dialog_level (&moves, &level_time)) {
		gchar *s;
		s = g_strdup_printf ("level %d %d 0\n", moves, level_time);
		write_child (priv->write_chan, s);
		g_free (s);
	}
}

static void
el_menu_takeback (GtkWidget *widget, gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	Board *board;
	Position *pos;
	
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));
	g_return_if_fail (data != NULL);
	
	engine = ENGINE_LOCAL (data);
	priv = engine->priv;

	board = BOARD(priv->view->board);
	pos = board_get_position (board);
	
	if ((priv->computer == BLACK && position_get_color_to_move (pos) == WHITE) ||
	    (priv->computer == WHITE && position_get_color_to_move (pos) == BLACK)) {
		write_child (priv->write_chan, "remove\n");
		g_signal_emit_by_name (G_OBJECT (engine), "unmove", 2);
	} else {
		write_child (priv->write_chan, "undo\n");
		g_signal_emit_by_name (G_OBJECT (engine), "unmove", 1);
	}
}

static void
el_menu_now (GtkWidget *widget, gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	
	engine = ENGINE_LOCAL (data);
	priv = engine->priv;
	
	write_child (priv->write_chan, "?\n");
}

static void 
fill_menu (GtkMenuShell *shell, gint pos, gpointer data) 
{
	EngineLocal *engine = data;
	EngineLocalPrivate *priv;
	
	GnomeUIInfo options_mode_items[] =
	{
		{GNOME_APP_UI_ITEM,N_("Computer _White"), NULL,
		 el_menu_computer_white, engine, NULL, 
		 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
		
		{GNOME_APP_UI_ITEM, N_("Computer _Black"), NULL,
		 el_menu_computer_black, engine, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},

		{GNOME_APP_UI_ITEM, N_("_Analyze Mode"), NULL,
		 el_menu_analyse_mode, engine, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
		
		GNOMEUIINFO_END  
	};
	
	GnomeUIInfo options_mode_list [] =
	{
		
		GNOMEUIINFO_RADIOLIST((gpointer)options_mode_items),
		
		GNOMEUIINFO_END  
	};

	GnomeUIInfo el_menu[] = 
	{
		GNOMEUIINFO_SUBTREE (N_("_Mode"), &options_mode_list),

		GNOMEUIINFO_ITEM_DATA (N_("_Level..."), NULL, el_menu_level, engine, NULL),

		GNOMEUIINFO_SEPARATOR,

		{GNOME_APP_UI_ITEM, N_("_Take Back"), NULL,
		 el_menu_takeback, engine, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UNDO,
		 GDK_t, GDK_CONTROL_MASK, NULL},
		
		GNOMEUIINFO_ITEM_DATA (N_("Move _Now"), NULL, el_menu_now, engine, NULL),

		GNOMEUIINFO_END  
	};

	priv = engine->priv;
	
	gnome_app_fill_menu (shell, el_menu, NULL, TRUE, pos);
	
	priv->whitecb = options_mode_items[0].widget;
	priv->blackcb = options_mode_items[1].widget;
	priv->analyse = options_mode_items[2].widget;

	el_menu_set_color (engine, priv->computer);
}

static void 
board_move_cb (GtkWidget *widget, Square from, Square to, gpointer data)
{
	EngineLocal *engine = data;
	EngineLocalPrivate *priv;
	MoveList *move_list;
	char str[100];

	priv = engine->priv;
	
	move_list = MOVE_LIST (priv->view->movelist);
	if (move_list_currply (move_list) < move_list_maxply (move_list)) {
		move_list_clear_from (move_list, move_list_currply (move_list));
		el_write_position (engine);
	}
	
	if (priv->analyse_info_added)
		server_term_clear (priv->analyse_info);
	
	move_to_ascii (str, from, to);
	write_child (priv->write_chan, "%s\n", str);
}

static void
el_process_move (EngineLocal *engine, char *p)
{
	EngineLocalPrivate *priv;
	Position *position;
	Square from, to;

	priv = engine->priv;

	position = board_get_position (BOARD (priv->view->board));
	if (san_to_move (position, p, &from, &to))
		ascii_to_move (position, p, &from, &to);

	debug_print (DEBUG_NORMAL, "Move detected: %s %d %d\n", p+4, from, to);

	board_move (BOARD (priv->view->board), from, to);
	move_list_add (MOVE_LIST (priv->view->movelist), from, to);
}

static void 
el_write_position (EngineLocal *engine) 
{
	EngineLocalPrivate *priv;
	Board *board;
	Position *pos;
	int i;
	char s[10];
	char *p;

	priv = engine->priv;
	
	board = BOARD (priv->view->board);
	pos = board_get_position (board);
	
	write_child (priv->write_chan, "edit\n");
	write_child (priv->write_chan, "#\n");
	
	for (i=A1; i<= H8;i++) {
		p = s;
		switch (pos->square[i]) {
		case WP:  *p++='P';break;
		case WN:  *p++='N';break;
		case WB:  *p++='B';break;
		case WR:  *p++='R';break;
		case WQ:  *p++='Q';break;
		case WK:  *p++='K';break;
		default:   continue;
		}
		
		*p++   = i - i / 10 * 10 + 96;   /*  a - h       */
		*p++   = i / 10 + 47 ;              /*  1 - 8       */
		*p=0;
		debug_print (DEBUG_NORMAL, "%s\n", s);  
		write_child (priv->write_chan, "%s\n",s);  
	}
	
	write_child (priv->write_chan, "c\n");  
	
	for (i=A1; i<= H8;i++) {
		p = s;
		switch (pos->square[i]) {
		case BP:  *p++='P';break;
		case BN:  *p++='N';break;
		case BB:  *p++='B';break;
		case BR:  *p++='R';break;
		case BQ:  *p++='Q';break;
		case BK:  *p++='K';break;
		default:   continue;
		}
		
		*p++   = i - i / 10 * 10 + 96;   /*  a - h       */
		*p++   = i / 10 + 47 ;              /*  1 - 8       */
		*p=0;

		debug_print (DEBUG_NORMAL, "%s\n", s);  
		write_child (priv->write_chan, "%s\n", s);  
	}
	
	write_child (priv->write_chan, ".\n");
}

static gboolean
engine_local_cb (GIOChannel *source,
		 GIOCondition condition,
		 gpointer data)
{
	EngineLocal *engine;
	EngineLocalPrivate *priv;
	static char buf[1024];
	static char *b=buf;

	char *p,*q;
	ssize_t len;

	engine = ENGINE_LOCAL (data);
	priv = engine->priv;
	
	g_io_channel_read (priv->read_chan, b, sizeof (buf) - 1 - (b - buf), &len);

	if (len > 0) {
		b[len] = 0;
		b += len;
	}

	while (1) {
		char tmp;

		q = strchr (buf,'\n');
		if (q == NULL) break;
		tmp = *(q+1);
		*(q+1) = 0;

		debug_print (DEBUG_VERBOSE, buf);
		if (priv->analyse_info_added)
			server_term_print (priv->analyse_info, buf);

		*q=0;
		*(q+1) = tmp;

		/* parse for  NUMBER ... MOVE */
		if (isdigit (*buf))
			if ((p = strstr (buf,"...")))
				el_process_move (engine, p+4);

		/* parse for move MOVE */
		if (!strncmp ("move",buf,4))
			el_process_move (engine, buf+5);

		memmove (buf, q+1, sizeof(buf) - ( q + 1 - buf));
		b -= (q + 1 - buf);
	}
	
	return TRUE;
}

static gboolean
engine_local_err_cb (GIOChannel *source,
		     GIOCondition condition,
		     gpointer data)
{
	EngineLocal *engine;
	GtkWidget *dlg;
	
	engine = ENGINE_LOCAL(data);

	dlg = gnome_warning_dialog(_("Local Engine connection died"));
	gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
	
	return FALSE;
}

