/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-ics.h
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _ENGINE_ICS_H_
#define _ENGINE_ICS_H_

#include "engine.h"

G_BEGIN_DECLS

#define ENGINE_TYPE_ICS		   (engine_ics_get_type ())
#define ENGINE_ICS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ENGINE_TYPE_ICS, EngineIcs))
#define ENGINE_ICS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), ENGINE_TYPE_ICS, EngineIcsClass))
#define ENGINE_IS_ICS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ENGINE_TYPE_ICS))
#define ENGINE_IS_ICS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ENGINE_TYPE_ICS))


typedef struct _EngineIcs        EngineIcs;
typedef struct _EngineIcsPrivate EngineIcsPrivate;
typedef struct _EngineIcsClass   EngineIcsClass;

struct _EngineIcs {
	Engine parent;

	EngineIcsPrivate *priv;
};

struct _EngineIcsClass {
	EngineClass parent_class;
};



GtkType    engine_ics_get_type (void);
GObject *engine_ics_new      (const char *host,
			      const char *port,
			      const char *user,
			      const char *pass,
			      const char *telnetprogram);

void engine_ics_user            (EngineIcs *engine);
void engine_ics_password        (EngineIcs *engine);
void engine_ics_invaliduser     (EngineIcs *engine);
void engine_ics_invalidpassword (EngineIcs *engine);
void engine_ics_prompt          (EngineIcs *engine,
				 gchar     *str);
void engine_ics_result          (EngineIcs *engine,
				 char      *result);
int  engine_ics_input           (char      *buf,
				 int        max);
void engine_ics_output          (EngineIcs *engine,
				 char      *str,
				 int       c_index);
void engine_ics_update_board    (EngineIcs *engine,
				 char      *boardstring);


G_END_DECLS

#endif
