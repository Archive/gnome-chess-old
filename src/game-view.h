/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* game-view.h
 *
 * Copyright (C) 2001  JP Rosevear
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _GAME_VIEW_H_
#define _GAME_VIEW_H_

#include <gnome.h>

G_BEGIN_DECLS

#define TYPE_GAME_VIEW			(game_view_get_type ())
#define GAME_VIEW(obj)			(GTK_CHECK_CAST ((obj), TYPE_GAME_VIEW, GameView))
#define GAME_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), TYPE_GAME_VIEW, GameViewClass))
#define IS_GAME_VIEW(obj)		(GTK_CHECK_TYPE ((obj), TYPE_GAME_VIEW))
#define IS_GAME_VIEW_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), TYPE_GAME_VIEW))


typedef struct _GameView        GameView;
typedef struct _GameViewPrivate GameViewPrivate;
typedef struct _GameViewClass   GameViewClass;

typedef void (*GameViewFillMenu) (GtkMenuShell *, gint pos, gpointer data);

typedef enum {
	GAME_VIEW_STATUS_NONE,          /*  No game		       */
	GAME_VIEW_STATUS_PAUSED,        /*  Game is paused	       */
	GAME_VIEW_STATUS_PROGRESS,      /*  Game is in progress, player is playing    */
	GAME_VIEW_STATUS_VIEWING,       /*  Game is in progress, player is not playing    */
	GAME_VIEW_STATUS_ABORTED,       /*  Game was aborted       */
	GAME_VIEW_STATUS_WHITE,         /*  White won the game     */
	GAME_VIEW_STATUS_BLACK,         /*  Black won the game     */
	GAME_VIEW_STATUS_DRAW           /*  Draw		       */
} GameViewStatus;

struct _GameView {
	GtkHBox parent;

	GtkWidget *board;
	GtkWidget *movelist;
	GtkWidget *black_clock;
	GtkWidget *white_clock;
	
	GameViewPrivate *priv;
};

struct _GameViewClass {
	GtkHBoxClass parent_class;

	void (*title) (GameView *view, const char *title);
};



GtkType    game_view_get_type        (void);
GtkWidget *game_view_new             (GameViewFillMenu fill_menu, gpointer data);
void game_view_fill_menu (GameView *view, GtkMenuShell *shell, gint pos);

const char *game_view_get_white (GameView *view);
void game_view_set_white (GameView *view, const char *name);

const char *game_view_get_black (GameView *view);
void game_view_set_black (GameView *view, const char *name);

const char *game_view_get_description (GameView *view);
void game_view_set_description (GameView *view, const char *description);

gint game_view_get_to_move (GameView *view);
void game_view_set_to_move (GameView *view, gint to_move);

GameViewStatus game_view_get_status (GameView *view);
void game_view_set_status (GameView *view, GameViewStatus status);

G_END_DECLS

#endif
