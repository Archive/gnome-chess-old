/* 
 * Copyright (C) 1999 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef PREFS_H
#define PREFS_H

#include <glib.h>
#include <gconf/gconf-client.h>
#include <gdk/gdk.h>

typedef struct {
	gchar *name;
	gchar *location;
} PrefsProgram;

typedef struct {
	gchar *server;
	gchar *port;
	gchar *user;
	gchar *password;
	gchar *connect;
} PrefsServer;

void prefs_init(void);
void prefs_sync (void);
void prefs_rm_notification (guint id);

/* Board prefs */
guint prefs_add_notification_light_color (GConfClientNotifyFunc func, gpointer data);
void prefs_get_light_color (guint8 *r, guint8 *g, guint8 *b);
void prefs_set_light_color (guint8 r, guint8 g, guint8 b);

guint prefs_add_notification_dark_color (GConfClientNotifyFunc func, gpointer data);
void prefs_get_dark_color (guint8 *r, guint8 *g, guint8 *b);
void prefs_set_dark_color (guint8 r, guint8 g, guint8 b);

const gchar *prefs_get_piecedir (void);
void prefs_set_piecedir (const char *piecedir);

/* Game prefs */
gint prefs_get_notation (void);
void prefs_set_notation (gint notation);

gboolean prefs_get_beeponmove (void);
void prefs_set_beeponmove (gboolean beep);

gboolean prefs_get_promotetoqueen (void);
void prefs_set_promotetoqueen (gboolean beep);

gboolean prefs_get_autoflag (void);
void prefs_set_autoflag (gboolean beep);

/* Program prefs */
guint prefs_add_notification_programs (GConfClientNotifyFunc func, gpointer data);
GSList *prefs_get_programs (void);
void prefs_set_programs (GSList *programs);
GSList *prefs_copy_program_list (GSList *list);
void prefs_free_program_list (GSList *list);

/* Server prefs */
guint prefs_add_notification_servers (GConfClientNotifyFunc func, gpointer data);
GSList *prefs_get_servers (void);
void prefs_set_servers (GSList *servers);
GSList *prefs_copy_server_list (GSList *list);
void prefs_free_server_list (GSList *list);

const gchar *prefs_get_telnetprogram (void);
void prefs_set_telnetprogram (const gchar *telnet_program);

/* Palette prefs */
GdkColor *prefs_get_palette (void);
void prefs_get_palette_color (guint color, guint8 *r, guint8 *g, guint8 *b);
void prefs_set_palette_color (guint color, guint8 r, guint8 g, guint8 b);

#endif /* PREFS_H */

