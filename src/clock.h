/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* clock.h
 *
 * Copyright (C) 2001  JP Rosevear
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _CLOCK_H_
#define _CLOCK_H_

#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TYPE_CLOCK			(clock_get_type ())
#define CLOCK(obj)			(GTK_CHECK_CAST ((obj), TYPE_CLOCK, Clock))
#define CLOCK_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), TYPE_CLOCK, ClockClass))
#define IS_CLOCK(obj)		        (GTK_CHECK_TYPE ((obj), TYPE_CLOCK))
#define IS_CLOCK_CLASS(klass)	        (GTK_CHECK_CLASS_TYPE ((obj), TYPE_CLOCK))


typedef struct _Clock        Clock;
typedef struct _ClockPrivate ClockPrivate;
typedef struct _ClockClass   ClockClass;

struct _Clock {
	GtkHBox parent;

	ClockPrivate *priv;
};

struct _ClockClass {
	GtkHBoxClass parent_class;

	void (*expired) (Clock *clock);
};



GType        clock_get_type    (void);
GtkWidget *  clock_new         (void);
gboolean     clock_get_to_move (Clock       *clock);
void         clock_set_to_move (Clock       *clock,
				gboolean     tomove);
const gchar *clock_get_name    (Clock       *clock);
void         clock_set_name    (Clock       *clock,
				const gchar *name);
gint         clock_get_time    (Clock       *clock);
void         clock_set_time    (Clock       *clock,
				gint         seconds);
void         clock_start       (Clock       *clock);
void         clock_stop        (Clock       *clock);

G_END_DECLS

#endif
