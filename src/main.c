/* 
 * Copyright (C) 1999 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>
#include <gnome.h>
#include <glade/glade.h>
#ifdef USE_WM_ICONS
#include <libgnomeui/gnome-window-icon.h>
#endif

#include "debug.h"
#include "makros.h"
#include "prefs.h"
#include "engine-window-registry.h"
#include "engine-window.h"
 
/* Command line variables */
static gint *ics = 0;
static char *icshost = NULL;
static char *icsuser = NULL;
static char *icspass = NULL;
static char *icsport = NULL;
static char *icstelnetprogram = NULL;
static char *fcp = NULL;
static char **startup_files = NULL;

static poptContext ctx;

static const struct poptOption gnome_chess_popt_options [] = {
	{ "debug", '\0', POPT_ARG_INT, &debug, 0,
	  N_("Enables some debugging functions"), N_("LEVEL") },
	{ "fcp", '\0', POPT_ARG_STRING, &fcp, 0,
	  N_("First Chess Program"),   N_("PROGRAM") },
	{ "ics", '\0', POPT_ARG_NONE, &ics, 0,
	  N_("Start in ICS mode"), NULL },
	{ "icshost", '\0', POPT_ARG_STRING, &icshost, 0,
	  N_("ICS Server"), N_("SERVER") },
	{ "icsuser", '\0', POPT_ARG_STRING, &icsuser, 0,
	  N_("ICS User"), N_("USERNAME") },
	{ "icspass", '\0', POPT_ARG_STRING, &icspass, 0,
	  N_("ICS Password"), N_("PASSWORD") },
	{ "icsport", '\0', POPT_ARG_STRING, &icsport, 0,
	  N_("ICS Port"), N_("PORT") },
	{ "icstelnetprogram", '\0', POPT_ARG_STRING, &icstelnetprogram, 0,
	  N_("ICS Telnet Programm"), N_("TELNET") },

	{ NULL, '\0', 0, NULL, 0 }
};

GtkWidget *main_engine_window = NULL;

int 
main (int argc, char *argv []) 
{
	int i = 0;
	
	bindtextdomain (GETTEXT_PACKAGE, GNOME_CHESS_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

	/* Command Line Args */
	gnome_init_with_popt_table (PACKAGE, 
				    VERSION, 
				    argc, argv, 
				    gnome_chess_popt_options,
				    0, &ctx);

	gnome_window_icon_set_default_from_file (GNOME_ICONDIR 
						 "/gnome-chess.png");

	/* Initialize the preferences */
	prefs_init();

	/* Initialize glade */
	glade_gnome_init();
	
	/* A Board Window */
	main_engine_window = engine_window_new ();
	engine_window_registry_add (ENGINE_WINDOW (main_engine_window));
	gtk_window_set_default_size (GTK_WINDOW (main_engine_window), 560, 420);
	gtk_widget_show (main_engine_window);

	/* Process various command line options */
	if (fcp != NULL) {
		char *argv[2];
		
		argv[0] = fcp;
		argv[1] = NULL;
		
		engine_window_add_local_engine (ENGINE_WINDOW (main_engine_window), argv);
	}

	if (ics && icshost != NULL) {
		engine_window_add_ics_engine (ENGINE_WINDOW (main_engine_window),
					      icshost, icsport, icsuser, icspass, NULL);
	}
	
	/* open the files */
	startup_files = (char **)poptGetArgs (ctx);
	for (i = 0; startup_files && startup_files[i]; i++) {
		engine_window_add_pgn_engine (ENGINE_WINDOW (main_engine_window), startup_files[i]);
	}
	
	/* If nothing else is open, show a board */
	if (engine_window_engine_count (ENGINE_WINDOW (main_engine_window)) == 0)
		engine_window_add_null_engine (ENGINE_WINDOW (main_engine_window));

	poptFreeContext (ctx);
	
	/* Begin the event loop */
	gtk_main ();

	return 0;
}

