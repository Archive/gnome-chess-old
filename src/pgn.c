/* 
 * Copyright (C) 1999 Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <gnome.h>

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "debug.h"
#include "makros.h"
#include "position.h"
#include "notation.h"
#include "board.h"
#include "game-view.h"
#include "pgn.h"

/*#define DEBUG */

enum {
  PGN_EVENT,
  PGN_SITE,
  PGN_DATE,
  PGN_ROUND,
  PGN_WHITE,
  PGN_BLACK,
  PGN_RESULT, 
  PGN_WHITE_COUNTRY, 
  PGN_BLACK_COUNTRY, 
  PGN_WHITE_TITLE,
  PGN_BLACK_TITLE,
  PGN_WHITE_ELO,
  PGN_BLACK_ELO,
  PGN_REMARK,
  PGN_ECO,
  PGN_OPENING,
  PGN_VARIATION,
  PGN_PLYCOUNT,
  PGN_ANNOTATOR,
  PGN_TIMECONTROL,
  PGN_NIC,
  PGN_EVENTDATE
};

static struct
{
  gchar *name;
  gint token;
} symbols[] = {
  { "Event", PGN_EVENT },         
  { "Site", PGN_SITE },
  { "Date", PGN_DATE },
  { "Round", PGN_ROUND },
  { "White", PGN_WHITE },
  { "Black", PGN_BLACK },
  { "Result", PGN_RESULT },
  { "WhiteCountry", PGN_WHITE_COUNTRY },
  { "BlackCountry", PGN_BLACK_COUNTRY },
  { "WhiteTitle", PGN_WHITE_TITLE },
  { "BlackTitle", PGN_BLACK_TITLE },
  { "WhiteELO", PGN_WHITE_ELO },
  { "BlackELO", PGN_BLACK_ELO },
  { "ELOWhite", PGN_WHITE_ELO }, /* some broken programs use this */
  { "ELOBlack", PGN_BLACK_ELO }, /* and this */
  { "Remark", PGN_REMARK },
  { "ECO", PGN_ECO },
  { "NIC", PGN_NIC },
  { "Opening", PGN_OPENING },
  { "Variation", PGN_VARIATION },
  { "Plycount", PGN_PLYCOUNT },
  { "Annotator", PGN_ANNOTATOR },
  { "EventDate", PGN_EVENTDATE },
  { "TimeControl", PGN_TIMECONTROL },
};

static guint nsymbols = sizeof (symbols) / sizeof (symbols[0]);

static GScannerConfig scanner_config =
{
  (
   " \t\r"
   )			/* cset_skip_characters */,
  (
   G_CSET_a_2_z
   "_"
   G_CSET_A_2_Z
   "_0123456789"
   )			/* cset_identifier_first */,
  (
   G_CSET_a_2_z
   "_0123456789"
   G_CSET_A_2_Z
   G_CSET_LATINS
   G_CSET_LATINC
   "-+#\\='./;:,$"
   )			/* cset_identifier_nth */,
  ( ";\n" )		/* cpair_comment_single */,
  
  FALSE			/* case_sensitive */,
  
  TRUE			/* skip_comment_multi */,
  TRUE			/* skip_comment_single */,
  TRUE			/* scan_comment_multi */,
  TRUE			/* scan_identifier */,
  FALSE			/* scan_identifier_1char */,
  FALSE			/* scan_identifier_NULL */,
  TRUE			/* scan_symbols */,
  FALSE			/* scan_binary */,
  TRUE			/* scan_octal */,
  TRUE			/* scan_float */,
  TRUE			/* scan_hex */,
  FALSE			/* scan_hex_dollar */,
  TRUE			/* scan_string_sq */,
  TRUE			/* scan_string_dq */,
  TRUE			/* numbers_2_int */,
  FALSE			/* int_2_float */,
  FALSE			/* identifier_2_string */,
  TRUE			/* char_2_token */,
  FALSE			/* symbol_2_token */,
  FALSE			/* scope_0_fallback */
};

static int pgn_reg_move (char *str, int test, pgn_info *info, GameView *view);
static void pgn_header_load (GScanner *s, Pgn_Tag *);
static GScanner* pgn_scanner_init (void);
static GList * pgn_index (const char *fname);
static int pgn_load_from_fp (FILE *fp, int len, int test, pgn_info *info, GameView *view);
static int pgn_game_load (const char *newbuf, int len, int test, pgn_info *info, GameView *view);

#ifdef DEBUG
void pgn_test(void);
void pgn_test(void)
{
	int i,n;
	FILE *file;
	char *fname="ttt";
	GList *game_list;
	GList *game;

	filename = g_strdup(fname);
	game_list=pgn_index(fname);

	n = g_list_length(game_list); 
	file = fopen(fname, "r");

	game = game_list;

	for(i=0;i<n-1;i++)
	{
		printf("Game %d\n",i+1);
		fseek(file,(long)game->data,SEEK_SET);
		txt_load_from_fp(file,(long)game->next->data - (long)game->data, TRUE);
		game = game->next;
	}
}
#endif

/* HACK ALERT
   gscanner now has buffers and does no longer like external seeks.
   this hack works around the problem
   According to Tim Janik on gtk-devel (12 Nov 1998) 
   "this will hopefully change, as soon as an IO abstraction for GLib is
   implemented which can be used as an input to GScanner."          
*/


static void
g_scanner_sync_file_offset_extern (GScanner *scanner, gint offset)
{
	g_return_if_fail (scanner != NULL);

	if (scanner->input_fd >= 0)
	{
		if (lseek (scanner->input_fd, offset, SEEK_SET) >= 0)
		{
			/* we succeeded, blow our buffer's contents now */
			scanner->text = NULL;
			scanner->text_end = NULL;
		}
	}
}

/* End of HACK ALERT */

pgn_info *
pgn_open (const char *filename)
{
	pgn_info *info = g_new0 (pgn_info, 1);
	GScanner *s = pgn_scanner_init ();
	GList *l;
	int file;

	info->filename = g_strdup (filename);
	info->game_list = pgn_index (info->filename);

	file = open (info->filename, O_RDONLY);
	if (file == -1) return NULL;

	g_scanner_input_file (s, file);

	for (l = info->game_list; l != NULL && l->next != NULL; l = l->next) {
		Pgn_Tag *tag = g_new0 (Pgn_Tag, 1);
      
		g_scanner_sync_file_offset_extern (s, GPOINTER_TO_INT (l->data));
		pgn_header_load (s, tag);
		info->game_tags = g_list_append (info->game_tags, tag);		
	}

	close (file);
	g_scanner_destroy (s);

	return info;
}

static GList *
pgn_index (const char *fname)
{
	FILE *fp = fopen (fname, "r");
	GList *game_list = NULL;
	char buf[1024];
	char *p;
	long tell;

	if (!fp) 
		return FALSE;

	while (1) {
		tell = ftell (fp);
		if (!fgets (buf, sizeof (buf), fp)) 
			break;

		if ((p = strstr (buf, "[Event \"")))
			game_list = g_list_prepend (game_list, GINT_TO_POINTER (tell + (p - buf)));
	}

	game_list = g_list_prepend (game_list, GINT_TO_POINTER (tell));
	game_list = g_list_reverse (game_list);
	fclose (fp);

	return game_list;
}

static int 
pgn_load_from_fp (FILE *fp, int len, int test, pgn_info *info, GameView *view)
{
	int ret;
	char *buf = g_new (char, len);

	fread (buf, len, 1, fp);
	ret = pgn_game_load (buf, len, test, info, view);
	g_free (buf);

	return ret;
}

static GScanner* 
pgn_scanner_init ()
{
	int i;
	GScanner *s = g_scanner_new(&scanner_config);

	for (i = 0; i < nsymbols; i++)
		g_scanner_add_symbol (s, symbols[i].name, 
				      GINT_TO_POINTER (symbols[i].token));
	return s;

}

static void 
pgntag (GTokenValue value1, GTokenValue value2, Pgn_Tag *tag)
{
	switch (value1.v_int)
	{
	case PGN_EVENT:
		tag->event = g_strdup(value2.v_string);
		break;
	case PGN_SITE:
		tag->site = g_strdup(value2.v_string);
		break;
	case PGN_DATE:
		tag->date = g_strdup(value2.v_string);
		break;
	case PGN_ROUND:
		tag->round = g_strdup(value2.v_string);
		break;
	case PGN_WHITE:
		tag->white = g_strdup(value2.v_string);
		break;
	case PGN_BLACK:
		tag->black = g_strdup(value2.v_string);
		break;
	case PGN_RESULT:
		tag->result = g_strdup(value2.v_string);
		break;
	default:
	  ;
	  // do not abort on unknown Tags 
	  //    abort();
	}
}

static Pgn_Tag * 
pgn_tag_new(void)
{
	Pgn_Tag *tag= malloc(sizeof( Pgn_Tag));

	tag->event=tag->site=tag->date=tag->round=tag->white=tag->black=tag->result=0;

	return tag;
}

static void 
pgn_tag_destroy(Pgn_Tag *tag)
{
	if (tag->event) g_free(tag->event);
	if (tag->site)  g_free(tag->site);
	if (tag->date)  g_free(tag->date);
	if (tag->round) g_free(tag->round);
	if (tag->white) g_free(tag->white);
	if (tag->black) g_free(tag->black);
	if (tag->result)g_free(tag->result);

	g_free(tag);
}

static int 
pgn_game_load (const char *newbuf, int len, int test, pgn_info *info, GameView *view)
{
	Pgn_Tag *tag= pgn_tag_new();
	int ret =FALSE;
	GScanner *s = pgn_scanner_init();

	move_list_clear (MOVE_LIST (view->movelist));
	move_list_freeze (MOVE_LIST (view->movelist));

	g_scanner_input_text(s,newbuf,len);

	while (1) {
		GTokenValue value1, value2;
		GTokenType token2;
		GTokenType token = g_scanner_get_next_token (s);
		debug_print (DEBUG_VERBOSE, "Token: %d\n", token);

		if (token == G_TOKEN_EOF) 
			break;

		switch(token) {
		case  G_TOKEN_LEFT_BRACE:
			token2 = g_scanner_get_next_token (s);
			value1 = g_scanner_cur_value (s);

			if (token2 == G_TOKEN_SYMBOL) {
				g_scanner_get_next_token (s);
				value2 = g_scanner_cur_value (s);
				pgntag (value1, value2, tag);
			} else {
				g_scanner_get_next_token (s);
				value2 = g_scanner_cur_value (s);
			}
			token2 =g_scanner_get_next_token (s);
			if (token2 != G_TOKEN_RIGHT_BRACE) {
				debug_print (DEBUG_NONE, "Parse error!\n");
				ret = FALSE;
				goto finish;
			}
			break;
		case  G_TOKEN_RIGHT_BRACE:
		case  G_TOKEN_SYMBOL:  
		case  G_TOKEN_STRING:
			debug_print (DEBUG_NONE,"Parse error!\n");
			break;
		case  G_TOKEN_FLOAT:
			break;
		case  G_TOKEN_IDENTIFIER:
			value1 = g_scanner_cur_value(s);
			if ('0' <=  *value1.v_string &&  *value1.v_string <= '9')
				break;
			pgn_reg_move (value1.v_string, test, info, view);
			break;
		case G_TOKEN_LEFT_CURLY: /* comments are ignored for now */
		{
			do {
				token2 =g_scanner_get_next_token(s);
				if (token2 ==  G_TOKEN_IDENTIFIER) {
					value1 = g_scanner_cur_value(s);
				} else if (token2 ==  G_TOKEN_EOF) { 
						debug_print(DEBUG_NONE, "EOF in comment!\n");
						ret = FALSE;
						goto finish;
				}
			} while (token2 != G_TOKEN_RIGHT_CURLY);
			break;
		}
		case G_TOKEN_LEFT_PAREN: /* RAVs are ignored for now */
		{ 
			int nest=1;
			debug_print(DEBUG_VERBOSE, "Nest %d\n",nest);
			while (1) {
				token2 =g_scanner_get_next_token(s);
				debug_print(DEBUG_VERBOSE, "Token: %d\n",token2);
				if (token2 == G_TOKEN_EOF) {
					debug_print(DEBUG_NONE,"EOF in RAV!\n");
					ret = FALSE;
					goto finish;
				} else if (token2 == G_TOKEN_LEFT_PAREN) {
						nest++;
						debug_print(DEBUG_VERBOSE, "Nest %d\n",nest);
				} else if (token2 == G_TOKEN_RIGHT_PAREN) {
					nest--;
					debug_print(DEBUG_VERBOSE, "Unnest %d\n",nest);
					if (!nest) break;
				}
			}
		}
		break;
		case '$': /* NAGs are ignored for now */
		{ 
		  while (1) {
		    token2 =g_scanner_peek_next_token(s);
			debug_print(DEBUG_NONE, "NAG token: %d\n",token2);
		    if(token2 >= '0' && token2 <='9') {
			g_scanner_get_next_token(s);
			continue;
		      }
		  break;
		  }
		  break;
		}
		case '%': /* PGN escape mechanism */
		{
		    debug_print(DEBUG_NONE, "Escape token:\n");
		    while (1) {
		      token2 = g_scanner_get_next_token(s);
		      debug_print(DEBUG_VERBOSE, "Token: %d\n",token2);
		      if (token2 == G_TOKEN_EOF) 
			break;
		      if (token2 == '\n') 
			break;
		    }
		    break;
		}
		case  '\n':
		        break;
		default:
			debug_print(DEBUG_NONE, "Unknown token: %d\n",token);
			ret = FALSE;
			goto finish;
		}
	}

	ret = TRUE;

 finish:
	move_list_thaw (MOVE_LIST (view->movelist));
	pgn_tag_destroy(tag);
	g_scanner_destroy(s);
	return ret;
}

static void 
pgn_header_load(GScanner *s, Pgn_Tag * tp)
{
	while(1) {
		GTokenValue value1,value2;
		GTokenType token =g_scanner_get_next_token(s);
		debug_print(DEBUG_VERBOSE, "Token: %d\n",token);

		if (token == G_TOKEN_EOF) break;


		switch(token)
		{
		case  G_TOKEN_LEFT_BRACE:
			break;
		case  G_TOKEN_RIGHT_BRACE:
			break;
		case  G_TOKEN_SYMBOL:  
			value1 = g_scanner_cur_value(s);
			g_scanner_get_next_token(s);
			value2 = g_scanner_cur_value(s);
			pgntag(value1,value2,tp);
			break;
		case  G_TOKEN_STRING:
			break;
		case  G_TOKEN_FLOAT:
			break;
		case  G_TOKEN_IDENTIFIER:
			value1 = g_scanner_cur_value(s);
			return;
		case  '\n':
		        break;
		default:
			debug_print(DEBUG_NONE, "Unknown token: %d\n",token);
			return;
		}
	}
}

static int 
pgn_reg_move_found (Square from, Square to, int test, pgn_info *info, GameView *view)
{
	if (!test) 
		move_list_add (MOVE_LIST (view->movelist), from, to);

	return TRUE;
}

static int 
pgn_reg_move(char *str, int test, pgn_info *info, GameView *view)
{
	Position *pos;
	Square from, to;

	pos = move_list_get_position_current (MOVE_LIST (view->movelist));
	

	if (!san_to_move (pos, str, &from, &to))
	    return pgn_reg_move_found (from, to, test, info, view);

	return FALSE;
}

void
pgn_select_game (pgn_info *pgn_info, GameView *view, gint row)
{
	FILE *fp;

	fp = fopen (pgn_info->filename, "r");
	if (fp) {
		long start, end;
		
		start = GPOINTER_TO_INT (g_list_nth_data (pgn_info->game_list, row));
		end = GPOINTER_TO_INT (g_list_nth_data (pgn_info->game_list, row + 1));

		fseek (fp, GPOINTER_TO_INT (g_list_nth_data (pgn_info->game_list, row)), SEEK_SET);
		pgn_load_from_fp (fp, end - start, FALSE, pgn_info, view);
	}
}

void
pgn_save (const char *fname, GameView *view)
{
	MoveList *movelist;
	FILE *file;
	Square from, to;
	int number, linelen;
	char str[256], rstr[256];
	const char *str2;
	int i;
	GameViewStatus status;
	Position *pos;
	
	linelen = 0;

	movelist = MOVE_LIST (view->movelist);
	pos = POSITION (position_new_initial ());
	number = move_list_startply (movelist);

	g_return_if_fail(!number);
	
	file = fopen(fname, "w");

	g_return_if_fail(file);
	
	/* Write the seven tag roster */
	fprintf(file,"[Event \"?\"]\n");
	fprintf(file,"[Site \"?\"]\n");
	fprintf(file,"[Round \"?\"]\n");
	fprintf(file,"[Date \"????.??.??\"]\n");
	str2 = game_view_get_white (view);
	fprintf(file, "[White \"%s\"]\n",str2);
	str2 = game_view_get_black (view);
	fprintf(file, "[Black \"%s\"]\n",str2);

	status = game_view_get_status (view);
	switch (status) {
	case GAME_VIEW_STATUS_WHITE:
		snprintf(rstr, sizeof(rstr), "1-0");
		break;
	case GAME_VIEW_STATUS_BLACK:
		snprintf(rstr, sizeof(rstr), "0-1");
		break;
	case GAME_VIEW_STATUS_DRAW:
		snprintf(rstr, sizeof(rstr), "1/2-1/2");
		break;
	default:
		snprintf(rstr, sizeof(rstr), "*");
	}
	fprintf(file, "[Result \"%s\"]\n\n", rstr);

	/* Write the moves */
	for( i = 1;  i<= move_list_maxply(movelist); i++)  {
		char *san;
		
		if (i%2 == 1) {
			number++;
			snprintf(str, sizeof(str), "%d. ", number);
			fprintf(file, "%s", str);
			linelen += strlen(str);
		}
		move_list_get_ply(movelist, i, &from, &to);
		position_move(pos, from, to);
		san = move_to_san (pos, from, to);
		fprintf(file, "%s ", san);
		g_free (san);
		linelen += strlen(san) + 1;
		if (linelen >= 70) {
			fprintf(file, "\n");
			linelen = 0;
		}
	}
	fprintf(file, " %s\n", rstr);

	fclose(file);
}

