/* 
 * Copyright (C) 1999 Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef _CHILD_H_
#define _CHILD_H_

#include <sys/types.h>
#include <glib.h>

G_BEGIN_DECLS

int  start_child (char       **arg,
		  GIOChannel **read_chan,
		  GIOChannel **write_chan,
		  pid_t       *childpid);

void write_child (GIOChannel  *write_chan,
		  char        *format,
		  ...);

int  stop_child  (pid_t        childpid);

G_END_DECLS

#endif




