/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* movlist.c
 *
 * Copyright (C) 2001  Robert Wilhelm.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Robert Wilhelm
 */

#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "debug.h"
#include "makros.h"
#include "prefs.h"
#include "notation.h"
#include "movlist.h"

typedef struct {
	Piece piece;
	Square from;
	Square to;
	int  check : 1;
	int  mate  : 1;
	int  take  : 1;
	int  color  : 1;
	int  reserved: 4;
	char ascii[8];
	Position *pos;
} Ply;

typedef struct {
	int  number;
	Ply *p[2];
} Move;

struct _MoveListPrivate
{
	gint length;
	gint current;
	gint ply;

	GtkWidget *swindow;
	GtkWidget *clist;
};

/* Class signals */
enum {
	MOVE_SIGNAL,
	LAST_SIGNAL
};
static gint move_list_signals[LAST_SIGNAL] = { 0 };

static void class_init (MoveListClass *klass);
static void init (MoveList *List);

static void ml_select_move (GtkWidget *widget, int row, int column,
			    GdkEventButton * event, MoveList *move_list);
static void ml_clicked_start (GtkWidget *widget, MoveList *movelist);
static void ml_clicked_prev (GtkWidget *widget, MoveList *movelist);
static void ml_clicked_next (GtkWidget *widget, MoveList *movelist);
static void ml_clicked_end (GtkWidget *widget, MoveList *movelist);


static GtkVBoxClass *parent_class = NULL;


GtkType
move_list_get_type (void)
{
  static GtkType type = 0;

  if (type == 0)
    {
      static const GtkTypeInfo info =
      {
        "MoveList",
        sizeof (MoveList),
        sizeof (MoveListClass),
        (GtkClassInitFunc) class_init,
        (GtkObjectInitFunc) init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      type = gtk_type_unique (gtk_vbox_get_type (), &info);
    }

  return type;
}

static void
class_init (MoveListClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (gtk_vbox_get_type ());

	move_list_signals[MOVE_SIGNAL] = gtk_signal_new ("move",
							GTK_RUN_FIRST,
							GTK_CLASS_TYPE (object_class),
							GTK_SIGNAL_OFFSET (MoveListClass, move_selected),
							gtk_marshal_NONE__INT, 
							GTK_TYPE_NONE, 1, GTK_TYPE_INT);
}


static void
init (MoveList *movelist)
{
	MoveListPrivate *priv;
	GtkWidget *clist, *hbox;
	static char *titles[] ={ N_("Number"), N_("White"), N_("Black")};
	const gchar *fn[] = {"start.png", "prev.png", "next.png", "end.png"};
	const gpointer cb[] = {ml_clicked_start, ml_clicked_prev, ml_clicked_next, ml_clicked_end};
	

	int i;

	priv = g_new0 (MoveListPrivate, 1);

	movelist->priv = priv;

	priv->swindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->swindow),
					GTK_POLICY_AUTOMATIC, 
					GTK_POLICY_ALWAYS);

	gtk_widget_set_usize (GTK_WIDGET (priv->swindow), 200, 0);

	gtk_container_add (GTK_CONTAINER (movelist), priv->swindow);
	gtk_widget_show (priv->swindow);

	/* The list of moves */
	for (i = 0; i < 3; i++)
		titles[i] = _(titles[i]);
	clist = gtk_clist_new_with_titles (3, titles);
	gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_BROWSE);
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	gtk_signal_connect (GTK_OBJECT (clist), "select_row",
			    (GtkSignalFunc) ml_select_move, 
			    movelist);

	for (i = 0; i < 3; i++)
		gtk_clist_set_column_width (GTK_CLIST (clist), i, 50);

	gtk_widget_set_usize (clist, 150, 0 );

	gtk_container_add (GTK_CONTAINER (priv->swindow), clist);
	gtk_widget_show (clist);
	priv->clist = clist;

	/* The navigation buttons */
	hbox = gtk_hbox_new (FALSE, 0);
	for (i = 0; i < 4; i++) {
		GdkPixbuf *pb;
		gchar *full;
		
		full = g_strdup_printf ("%s/pixmaps/gnome-chess/%s", 
					GNOMEDATADIR, fn[i]);
		pb = gdk_pixbuf_new_from_file (full, NULL);
		if (pb != NULL) {
			GtkWidget *btn, *gtp;
			GdkPixmap *gdp;
			GdkBitmap *mask;

			gdk_pixbuf_render_pixmap_and_mask (pb, &gdp, &mask, 100);
			gtp = gtk_pixmap_new (gdp, mask);
			btn = gtk_button_new ();
			gtk_container_add (GTK_CONTAINER (btn), gtp);
			gtk_widget_show (gtp);
			gtk_container_add (GTK_CONTAINER (hbox), btn);
			gtk_widget_show (btn);
			gtk_signal_connect (GTK_OBJECT (btn), "clicked",
					    (GtkSignalFunc) cb[i], 
					    movelist);
		}
	}
	gtk_box_pack_start (GTK_BOX (movelist), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	
	move_list_clear (movelist);
}



GtkWidget *
move_list_new (void)
{
	return gtk_type_new (MOVE_TYPE_LIST);
}

void
move_list_freeze (MoveList *movelist)
{
	MoveListPrivate *priv;
	
	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;
	
	gtk_clist_freeze (GTK_CLIST (priv->clist));
}

void
move_list_thaw (MoveList *movelist)
{
	MoveListPrivate *priv;
	
	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;
	
	gtk_clist_thaw (GTK_CLIST (priv->clist));
}


Position*
move_list_get_position (MoveList *movelist, gint plynum)
{
	MoveListPrivate *priv;
	Move *mv;
	gint startply;

	g_return_val_if_fail (movelist != NULL, NULL);
	g_return_val_if_fail (IS_MOVE_LIST (movelist), NULL);

	priv = movelist->priv;
	
	startply = move_list_startply (movelist);
	if (startply == plynum) {
		gpointer pos;

		pos = gtk_object_get_user_data (
			GTK_OBJECT (priv->clist));

		return POSITION (pos);
	}
	
	mv = gtk_clist_get_row_data (
		GTK_CLIST (priv->clist),
		(plynum - startply + 1) / 2 - 1);

	g_return_val_if_fail (mv != NULL, NULL);

	if (plynum % 2 == 1)
		return mv->p [0]->pos;
	else
		return mv->p [1]->pos;
}

Position *
move_list_get_position_start (MoveList *movelist)
{
	gint plynum;

	g_return_val_if_fail (movelist != NULL, NULL);
	g_return_val_if_fail (IS_MOVE_LIST (movelist), NULL);

	plynum = move_list_startply (movelist);

	return move_list_get_position (movelist, plynum);
}

Position *
move_list_get_position_current (MoveList *movelist)
{
	gint plynum;

	g_return_val_if_fail (movelist != NULL, NULL);
	g_return_val_if_fail (IS_MOVE_LIST (movelist), NULL);

	plynum = move_list_currply (movelist);

	return move_list_get_position (movelist, plynum);
}

void
move_list_get_ply (MoveList *movelist, gint plynum, Square *from, Square *to)
{
	MoveListPrivate *priv;
	Move *mv;
	gint startply;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;
	
	startply = move_list_startply (movelist);
	
	mv = gtk_clist_get_row_data (GTK_CLIST (priv->clist),
				     (plynum - startply + 1) / 2 - 1);

	if (plynum % 2 == 1) {
		*from = mv->p[0]->from;
		*to = mv->p[0]->to;
	} else {
		*from = mv->p[1]->from;
		*to = mv->p[1]->to;
	}
}

static Move *
move_list_addtolist (MoveList *movelist) 
{
	MoveListPrivate *priv;
	Move *mv;
	GtkCList *clist;
	char *text[10] = { "", "","" };
	char text2[10];

	priv = movelist->priv;
	
	mv = g_new (Move, 1);
	mv->number = (priv->ply + 1) /2;
	mv->p[0] = NULL;
	mv->p[1] = NULL;

	sprintf (text2, "%d", mv->number);
	clist = GTK_CLIST (priv->clist);
	gtk_clist_append (clist, text);
	gtk_clist_set_text (clist, priv->length / 2, 0, text2);
	gtk_clist_set_row_data (clist, priv->length / 2, mv);
	gtk_clist_select_row (clist, priv->length / 2, 0);
	gtk_clist_moveto (clist, priv->length / 2, 0, 1.0, 1.0);

	return mv;
}

void
move_list_add (MoveList *movelist, Square from, Square to)
{
	MoveListPrivate *priv;
	Position *currpos;
	GtkCList *clist;
	Move *mv;
	Ply *p;
	char *str;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;
	
	p = g_new (Ply, 1);

	if (!p) 
		abort ();

	p->from = from;
	p->to   = to;
	currpos = move_list_get_position_current (movelist);
	p->pos = position_copy (currpos);
	p->piece = p->pos->square[from];
	p->color = WPIECE (p->piece) ? WHITE : BLACK;
	position_move(p->pos, from, to);

	if (prefs_get_notation () == SAN) {
		str = move_to_san (p->pos, p->from, p->to);
		strcpy (p->ascii, str);
		g_free (str);
	} else {
		piece_move_to_ascii (p->ascii, p->piece, p->from, p->to);
	}

	priv->length++;
	priv->current++;
	priv->ply++;
	clist = GTK_CLIST (priv->clist);
	if (WPIECE (p->piece)) {
		mv = move_list_addtolist (movelist);
		mv->p[0] = p;
		gtk_clist_set_text (clist, priv->length / 2, 1, p->ascii);
	} else {
		mv = gtk_clist_get_row_data (clist, (priv->length / 2) - 1);
		if (mv == NULL) {
			mv = move_list_addtolist (movelist);
			priv->length++;
			priv->current++;
		}
		mv->p[1] = p;
		gtk_clist_set_text (clist, (priv->length / 2) - 1, 2, p->ascii);
	}
}

void 
move_list_movetoply (MoveList *movelist, gint plynum) 
{
	MoveListPrivate *priv;
	GtkCList *clist;
	int row;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;

	row = (plynum - 1)/ 2;

	clist = GTK_CLIST (priv->clist);
	gtk_clist_unselect_row (clist, priv->current, 0);
	gtk_clist_select_row (clist, row, 0);
	gtk_clist_moveto (clist, row, 0, 1.0, 1.0);
	priv->current = plynum;

	gtk_signal_emit (GTK_OBJECT (movelist),
			 move_list_signals[MOVE_SIGNAL],
			 plynum);
}

void
move_list_move_forward (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;
	
	if (priv->current == priv->length)
		return;
                        
	move_list_movetoply (movelist, priv->current + 1);
}

void
move_list_move_back (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;

	if (priv->current == 0)
		return;
                 
	if (priv->current == 1)
		move_list_move_start (movelist);
	else
		move_list_movetoply (movelist, priv->current - 1);
}

void
move_list_move_start (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;

	if (priv->current == 0)
		return;
             
	move_list_movetoply (movelist, 0);
}

void
move_list_move_end (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	priv = movelist->priv;

	move_list_movetoply (movelist, priv->length);
}

gint
move_list_startply (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_val_if_fail (movelist != NULL, 0);
	g_return_val_if_fail (IS_MOVE_LIST (movelist), 0);

	priv = movelist->priv;

	return priv->ply - priv->length;
}

gint
move_list_currply (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_val_if_fail (movelist != NULL, 0);
	g_return_val_if_fail (IS_MOVE_LIST (movelist), 0);

	priv = movelist->priv;

	return priv->ply - (priv->length - priv->current);
}

gint
move_list_maxply (MoveList *movelist)
{
	MoveListPrivate *priv;

	g_return_val_if_fail (movelist != NULL, 0);
	g_return_val_if_fail (IS_MOVE_LIST (movelist), 0);

	priv = movelist->priv;

	return priv->ply;
}

void 
move_list_clear_from (MoveList *movelist, gint plynum) 
{
	MoveListPrivate *priv;
	GtkCList *clist;
	Move *mv;
	gint orig_plynum;
	gint i;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	debug_print (DEBUG_NORMAL, "move_list_clear_from %d\n", plynum);

	priv = movelist->priv;

	orig_plynum = plynum;
	
	clist = GTK_CLIST (priv->clist);
	if (plynum > 0 && (plynum % 2) == 1) {
		mv = gtk_clist_get_row_data (clist, (plynum - 1) / 2);
		gtk_clist_set_text (clist, (plynum - 1) / 2, 2, "");
		g_free (mv->p[1]);
		mv->p[1] = NULL;
		plynum++;
	}

	for (i = plynum; i <= move_list_maxply (movelist); i += 2) {
		debug_print (DEBUG_NORMAL, "clist remove %d\n",  (i - move_list_startply (movelist)) / 2);
		gtk_clist_remove (clist, (i - move_list_startply (movelist)) / 2);
	}

	priv->length = priv->current = priv->ply = orig_plynum;
	move_list_movetoply (movelist, orig_plynum);
}

void
move_list_clear_initial (MoveList *movelist, gint plynum, Position *pos)
{
	MoveListPrivate *priv;
	Position *pos2;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));
	g_return_if_fail (pos != NULL);

	priv = movelist->priv;

	priv->current = 0;
	priv->length =  0;
	priv->ply = plynum;
	pos2 = position_copy (pos);

	if (priv->clist) {
		GtkCList *clist = GTK_CLIST (priv->clist);

		gtk_clist_clear (clist);
		gtk_object_set_user_data (GTK_OBJECT (clist), pos2);
	}
}

void
move_list_clear (MoveList *movelist)
{
	Position *currpos;

	g_return_if_fail (movelist != NULL);
	g_return_if_fail (IS_MOVE_LIST (movelist));

	currpos = POSITION (position_new_initial ());
	move_list_clear_initial (movelist, 0, currpos);
}

static void
ml_select_move (GtkWidget *widget, int row, int column,
		GdkEventButton * event, MoveList *movelist)
{
	MoveListPrivate *priv;
	gint plynum;

	priv = movelist->priv;
	
	debug_print (DEBUG_NORMAL, "Select move %d %d\n", row, column);
	if (column == 1 || column == 2){
		plynum = row * 2 + column;
		priv->current = plynum;
		gtk_signal_emit (GTK_OBJECT (movelist),
				 move_list_signals[MOVE_SIGNAL],
				 plynum);
	}	
}

static void
ml_clicked_start (GtkWidget *widget, MoveList *movelist) 
{
	move_list_move_start (movelist);
}

static void
ml_clicked_prev (GtkWidget *widget, MoveList *movelist) 
{
	move_list_move_back (movelist);
}

static void
ml_clicked_next (GtkWidget *widget, MoveList *movelist) 
{
	move_list_move_forward (movelist);
}

static void
ml_clicked_end (GtkWidget *widget, MoveList *movelist) 
{
	move_list_move_end (movelist);
}
