/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine.h
 *
 * Copyright (C) 2001  Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Robert Wilhelm
 *          JP Rosevear
 */

#ifndef _ENGINE_H_
#define _ENGINE_H_

#include <gtk/gtk.h>
#include "makros.h"
#include "position.h"
#include "engine-view.h"

G_BEGIN_DECLS

#define TYPE_ENGINE	       (engine_get_type ())
#define ENGINE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_ENGINE, Engine))
#define ENGINE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_ENGINE, EngineClass))
#define IS_ENGINE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_ENGINE))
#define IS_ENGINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_ENGINE))


typedef struct _Engine        Engine;
typedef struct _EnginePrivate EnginePrivate;
typedef struct _EngineClass   EngineClass;

typedef enum {
	ENGINE_ERROR_WARNING,
	ENGINE_ERROR_CRITICAL
} EngineError;

typedef void (*EngineFillMenu) (Engine *, GtkMenuShell *, gint pos);

struct _Engine {
	GObject parent;

	EnginePrivate *priv;
};

struct _EngineClass {
	GObjectClass parent_class;

	/* Virtual functions */
	EngineFillMenu engine_fill_menu;
	
	/* Signal prototypes */
	void       (*game)   (Engine *engine, gint ply, Position *pos);
	void       (*move)   (Engine *engine, Square from, Square to);
	void       (*unmove) (Engine *engine, guint ply);
	void       (*error)  (Engine *engine, EngineError error);
};



GType engine_get_type  (void);
EngineView  *engine_get_engine_view (Engine *engine);
void  engine_fill_menu (Engine       *engine,
			GtkMenuShell *shell,
			gint          pos);

G_END_DECLS

#endif
