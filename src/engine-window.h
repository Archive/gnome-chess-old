/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-window.h
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _ENGINE_WINDOW_H_
#define _ENGINE_WINDOW_H_

#include <gnome.h>
#include "game-view.h"

G_BEGIN_DECLS

#define ENGINE_TYPE_WINDOW			(engine_window_get_type ())
#define ENGINE_WINDOW(obj)			(GTK_CHECK_CAST ((obj), ENGINE_TYPE_WINDOW, EngineWindow))
#define ENGINE_WINDOW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), ENGINE_TYPE_WINDOW, EngineWindowClass))
#define IS_ENGINE_WINDOW(obj)			(GTK_CHECK_TYPE ((obj), ENGINE_TYPE_WINDOW))
#define IS_ENGINE_WINDOW_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), ENGINE_TYPE_WINDOW))


typedef struct _EngineWindow        EngineWindow;
typedef struct _EngineWindowPrivate EngineWindowPrivate;
typedef struct _EngineWindowClass   EngineWindowClass;

struct _EngineWindow {
	GnomeApp parent;

	EngineWindowPrivate *priv;
};

struct _EngineWindowClass {
	GnomeAppClass parent_class;
};



GtkType    engine_window_get_type            (void);
GtkWidget *engine_window_new                 (void);

void engine_window_add_pgn_engine (EngineWindow *window, const char *filename);
void engine_window_add_local_engine (EngineWindow *window, char **arg);
void engine_window_add_ics_engine (EngineWindow *window,
				   const char *host,
				   const char *port,
				   const char *user,
				   const char *pass,
				   const char *telnetprogram);
void engine_window_add_null_engine (EngineWindow *window);

int engine_window_engine_count (EngineWindow *window);

G_END_DECLS

#endif

