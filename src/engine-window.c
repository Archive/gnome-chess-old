/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-window.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <config.h>
#include "common.h"
#include "pgn.h"
#include "prefs.h"
#include "dialogs.h"
#include "engine_null.h"
#include "engine-pgn.h"
#include "engine_ics.h"
#include "engine_local.h"
#include "engine-view.h"
#include "engine-window.h"

static void class_init (EngineWindowClass *klass);
static void init (EngineWindow *window);
static void destroy (GtkObject *object);

static void tree_selection_changed_cb (GtkTreeSelection *selection, gpointer data);

enum columns {
	COL_ICON,
	COL_TITLE,
	COL_ENGINE,
	COL_COUNT
};

enum rows {
	ROW_PGN,
	ROW_PROGRAMS,
	ROW_SCRATCH,
	ROW_SERVERS,
	ROW_COUNT
};

typedef struct {
	EngineWindow *window;
	gpointer data;
} EngineWindowMenuData;

struct _EngineWindowPrivate {
	GtkTreeStore *store;
	GtkTreeSelection *selection;
	GtkWidget *view;
	
	GtkTreePath *paths[ROW_COUNT];
	
	GtkNotebook *notebook;

	GSList *program_md;
	GSList *server_md;

	guint programs_id;
	guint servers_id;
	
	gint num;
};

static GnomeAppClass *parent_class = NULL;

static void 
menu_new (GtkWidget *widget, void *data) 
{
	EngineWindow *window = ENGINE_WINDOW (data);

	engine_window_add_null_engine (window);
}

static void 
menu_open (GtkWidget *widget, void *data) 
{
	EngineWindow *window = ENGINE_WINDOW (data);
	char *filename;

	filename = filesel ("Load PGN", "noname.pgn");
	if (filename)
		engine_window_add_pgn_engine (window, filename);
}

static void 
menu_save_as (GtkWidget *widget, void *data) 
{
	EngineWindow *window = ENGINE_WINDOW (data);
	EngineWindowPrivate *priv;
	char *filename;
	
	priv = window->priv;
	
	filename = filesel ("Save PGN", "noname.pgn");
	if (filename) {
		Engine *engine;
		GtkTreeModel *model;
		GtkTreeIter iter;
		GameView *game_view;
		
		if (gtk_tree_selection_get_selected (priv->selection, &model, &iter)) {
			EngineView *view;
			
			gtk_tree_model_get (model, &iter, COL_ENGINE, &engine, -1);
			view = engine_get_engine_view (engine);
		
			game_view = engine_view_get_current_game_view (view);
			pgn_save (filename, game_view);			
		}
	}
}

static void 
menu_program (GtkWidget *widget, void *data) 
{
	EngineWindowMenuData *md = data;
	PrefsProgram *pp = md->data;
	GtkWidget *window;
	char *argv[2];
	
	argv[0] = pp->location;
	argv[1] = NULL;

	window = gtk_widget_get_toplevel (widget);
	engine_window_add_local_engine (md->window, argv);
}

static void 
menu_server (GtkWidget *widget, void *data) 
{
	EngineWindowMenuData *md = data;
	PrefsServer *ps = md->data;
	GtkWidget *window;
	
	window = gtk_widget_get_toplevel (widget);
	engine_window_add_ics_engine (md->window, 
				      ps->server, ps->port, ps->user, 
				      ps->password, ps->connect);
}

static void 
menu_close (GtkWidget *widget, void *data) 
{
	EngineWindow *window = ENGINE_WINDOW (data);
	EngineWindowPrivate *priv;
        GtkTreeIter iter;
        GtkTreeModel *model;
	Engine *engine;
	
	priv = window->priv;
	
        if (gtk_tree_selection_get_selected (priv->selection, &model, &iter)) {
		EngineView *view;
		int num;
		
                gtk_tree_model_get (model, &iter, COL_ENGINE, &engine, -1);

		/* Remove the view */
		view = engine_get_engine_view (engine);		
		num = gtk_notebook_get_current_page (priv->notebook);
		gtk_notebook_remove_page (priv->notebook, num);		
		g_object_unref (engine);

		/* Remove the stuff from the model */
		gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
		
	}
}

static void 
menu_exit (GtkWidget *widget, void *data) 
{
	gtk_main_quit ();
}

static void 
menu_prefs (GtkWidget *widget, void *data) 
{
	dialog_prefs ();
}

static void 
menu_about (GtkWidget *widget, void *data) 
{
	EngineWindow *window = ENGINE_WINDOW (data);
	static GtkWidget *about = NULL;

	const gchar *authors[] = { 
		"Robert Wilhelm",
		"JP Rosevear",
		NULL
	};
	const gchar *documentors[] = { 
		"JP Rosevear",
		NULL
	};

	if (about != NULL) {
		gtk_window_present (GTK_WINDOW (about));
		return;
	}
	
	about = gnome_about_new (_("Gnome Chess"), VERSION,
				 "Copyright 1998-2001 Robert Wilhelm\n"
				 "Copyright 1999-2003 JP Rosevear",
				 "The graphical chess interface for GNOME",
				 authors,
				 documentors,
				 "Translations by the GNOME Translation Project\n",
				 NULL);

	gtk_window_set_destroy_with_parent (GTK_WINDOW (about), TRUE);
	gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (window));

	g_signal_connect (G_OBJECT (about), "destroy",
			  G_CALLBACK (gtk_widget_destroyed), &about);

	gtk_widget_show_all (about);
}

static void
set_program_menu (EngineWindow *window)
{
	EngineWindowPrivate *priv;
	GSList *programs, *l;

	priv = window->priv;
	
	gnome_app_remove_menus (GNOME_APP (window), "File/New/Programs/", 
				g_slist_length (priv->program_md));
	
	for (l = priv->program_md; l != NULL; l = l->next)
		g_free (l->data);
	g_slist_free (priv->program_md);
	priv->program_md = NULL;
	
	programs = prefs_get_programs ();
	for (l = programs; l != NULL; l = l->next) {
		EngineWindowMenuData *md = g_new0 (EngineWindowMenuData, 1);
		PrefsProgram *pp = l->data;

		md->window = window;
		md->data = pp;
		priv->program_md = g_slist_append (priv->program_md, md);
		
		{
			GnomeUIInfo info[]= {
				{GNOME_APP_UI_ITEM, 
				 pp->name, NULL, 
				 menu_program, md,
				 NULL, GNOME_APP_PIXMAP_NONE, 
				 NULL, 0, 0, NULL},
				
				GNOMEUIINFO_END};
		
			gnome_app_insert_menus (GNOME_APP (window), 
						"File/New/Programs/", info);
		}
	}
}

static void
programs_changed_cb (GConfClient* client, guint id, GConfEntry *entry, gpointer data)
{
	EngineWindow *window = data;
	
	set_program_menu (window);
}

static void
set_server_menu (EngineWindow *window)
{
	EngineWindowPrivate *priv;
	GSList *servers, *l;

	priv = window->priv;	

	gnome_app_remove_menus (GNOME_APP (window), "File/New/Servers/",
				g_slist_length (priv->server_md));

	for (l = priv->server_md; l != NULL; l = l->next)
			g_free (l->data);
	g_slist_free (priv->server_md);	

	servers = prefs_get_servers ();
	for (l = servers; l != NULL; l = l->next) {
		EngineWindowMenuData *md = g_new0 (EngineWindowMenuData, 1);
		PrefsServer *ps = l->data;
	
		md->window = window;
		md->data = ps;
		priv->server_md = g_slist_append (priv->server_md, md);
			
		
		{
			GnomeUIInfo info[]= {
				{GNOME_APP_UI_ITEM, 
				 ps->server, NULL, 
				 menu_server, md,
				 NULL, GNOME_APP_PIXMAP_NONE, 
				 NULL, 0, 0, NULL},
				
				GNOMEUIINFO_END};
		
			gnome_app_insert_menus (GNOME_APP (window), 
						"File/New/Servers/", info);
		}
	}	
}

static void
servers_changed_cb (GConfClient* client, guint id, GConfEntry *entry, gpointer data)
{
	EngineWindow *window = data;
	
	set_server_menu (window);
}

static GnomeUIInfo file_play_menu[] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo file_server_menu[] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo file_new_menu[] = {

	GNOMEUIINFO_MENU_NEW_ITEM ("_New Scratch", "New scratch board", menu_new, NULL),

	GNOMEUIINFO_SUBTREE_STOCK (N_("_Programs"), file_play_menu, GNOME_STOCK_MENU_EXEC),

	GNOMEUIINFO_SUBTREE_STOCK (N_("_Servers"),  file_server_menu, GNOME_STOCK_MENU_BOOK_RED),
	
	GNOMEUIINFO_END
};

static GnomeUIInfo file_menu[] = {

	GNOMEUIINFO_MENU_NEW_SUBTREE (file_new_menu),

	GNOMEUIINFO_MENU_OPEN_ITEM (menu_open, NULL),

	GNOMEUIINFO_MENU_SAVE_AS_ITEM (menu_save_as, NULL),

	GNOMEUIINFO_SEPARATOR, 

	GNOMEUIINFO_MENU_CLOSE_ITEM (menu_close, NULL),

	GNOMEUIINFO_MENU_EXIT_ITEM (menu_exit, NULL),
	
	GNOMEUIINFO_END
};

static GnomeUIInfo games_menu[] = {

	GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {

	GNOMEUIINFO_MENU_PREFERENCES_ITEM (menu_prefs, NULL),
	
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {

	GNOMEUIINFO_MENU_ABOUT_ITEM (menu_about, NULL),
	
	GNOMEUIINFO_END
};

GnomeUIInfo menus[] = {

	GNOMEUIINFO_MENU_FILE_TREE (file_menu),

	GNOMEUIINFO_MENU_GAME_TREE (games_menu),

	GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu),

	GNOMEUIINFO_MENU_HELP_TREE (help_menu),

	GNOMEUIINFO_END
};

static void
class_init (EngineWindowClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (gnome_app_get_type ());
	
	object_class->destroy = destroy;
}

static gboolean
tree_selection_function (GtkTreeSelection *selection,
			 GtkTreeModel *model,
			 GtkTreePath *path,
			 gboolean path_currently_selected,
			 gpointer data)
{
	EngineWindow *window = data;
	EngineWindowPrivate *priv;
	int i;
	
	priv = window->priv;
	
	for (i = 0; i < ROW_COUNT; i++) {
		if (!gtk_tree_path_compare (priv->paths[i], path))
			return FALSE;
	}	

	return TRUE;
}

static void
init (EngineWindow *window)
{
	EngineWindowPrivate *priv;
	GtkWidget *hpane;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeIter iter;
	
	gnome_app_construct (GNOME_APP (window), "gnome-chess", NULL); 
	
	priv = g_new0 (EngineWindowPrivate, 1);

	window->priv = priv;

	hpane = gtk_hpaned_new ();
	gtk_widget_show (hpane);
	
	/* The engine tree */
	priv->store = gtk_tree_store_new (COL_COUNT, G_TYPE_STRING, G_TYPE_STRING, TYPE_ENGINE);

	priv->view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (priv->store));
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->view), FALSE);
	
	column = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->view), column);
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_add_attribute (column, renderer, "stock-id", COL_ICON);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_add_attribute (column, renderer, "text", COL_TITLE);

	priv->selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->view));
	gtk_tree_selection_set_select_function (priv->selection, tree_selection_function, window, NULL);
	gtk_tree_selection_set_mode (priv->selection, GTK_SELECTION_SINGLE);

	gtk_widget_show (priv->view);

	gtk_paned_pack1 (GTK_PANED (hpane), GTK_WIDGET (priv->view), TRUE, TRUE);
	
	g_signal_connect (G_OBJECT (priv->selection), "changed",
			  G_CALLBACK (tree_selection_changed_cb),
			  window);

	/* Set up the toplevel nodes */
	gtk_tree_store_append (priv->store, &iter, NULL); 
	gtk_tree_store_set (priv->store, &iter,
			    COL_ICON, GNOME_STOCK_MULTIPLE_FILE,
			    COL_TITLE, "PGN Files", 
			    COL_ENGINE, NULL, -1);
	priv->paths[ROW_PGN] = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &iter);
	
	gtk_tree_store_append (priv->store, &iter, NULL); 
	gtk_tree_store_set (priv->store, &iter,
			    COL_ICON, GTK_STOCK_EXECUTE,
			    COL_TITLE, "Programs",
			    COL_ENGINE, NULL, -1);
	priv->paths[ROW_PROGRAMS] = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &iter);

	gtk_tree_store_append (priv->store, &iter, NULL); 
	gtk_tree_store_set (priv->store, &iter,
			    COL_ICON, GNOME_STOCK_TRASH_FULL,
			    COL_TITLE, "Scratch",
			    COL_ENGINE, NULL, -1);	
	priv->paths[ROW_SCRATCH] = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &iter);

	gtk_tree_store_append (priv->store, &iter, NULL); 
	gtk_tree_store_set (priv->store, &iter,
			    COL_ICON, GNOME_STOCK_MENU_BOOK_RED,
			    COL_TITLE, "Servers",
			    COL_ENGINE, NULL, -1);
	priv->paths[ROW_SERVERS] = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &iter);
                    
	/* The game and info views */
	priv->notebook = GTK_NOTEBOOK (gtk_notebook_new ());
	gtk_notebook_set_show_tabs (priv->notebook, FALSE);
	gtk_widget_show (GTK_WIDGET (priv->notebook));

	gtk_paned_pack2 (GTK_PANED (hpane), GTK_WIDGET (priv->notebook), TRUE, TRUE);

	gnome_app_create_menus_with_data (GNOME_APP (window), menus, window);

	set_program_menu (window);
	set_server_menu (window);
	
	priv->programs_id = prefs_add_notification_programs (programs_changed_cb, window);
	priv->servers_id = prefs_add_notification_servers (servers_changed_cb , window);
	
	gnome_app_set_contents (GNOME_APP (window), GTK_WIDGET (hpane));
}

static void 
destroy (GtkObject *object)
{
	EngineWindow *window;
	EngineWindowPrivate *priv;

	window = ENGINE_WINDOW (object);
	priv = window->priv;

	if (priv != NULL) {
		GSList *l;
		int i;
		
		g_object_unref (priv->store);
		
		for (i = 0; i < ROW_COUNT; i++)
			gtk_tree_path_free (priv->paths[i]);
		
		for (l = priv->program_md; l != NULL; l = l->next)
			g_free (l->data);
		g_slist_free (priv->program_md);

		for (l = priv->server_md; l != NULL; l = l->next)
			g_free (l->data);
		g_slist_free (priv->server_md);

		prefs_rm_notification (priv->programs_id);
		prefs_rm_notification (priv->servers_id);

		g_free (priv);
		window->priv = NULL;
	}
	
	GNOME_CALL_PARENT (GTK_OBJECT_CLASS, destroy, (object));
}


GtkType
engine_window_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static const GtkTypeInfo info = {
				"EngineWindow",
				sizeof (EngineWindow),
				sizeof (EngineWindowClass),
				(GtkClassInitFunc) class_init,
				(GtkObjectInitFunc) init,
				/* reserved_1 */ NULL,
				/* reserved_2 */ NULL,
				(GtkClassInitFunc) NULL,
			};

		type = gtk_type_unique (gnome_app_get_type (), &info);
	}

	return type;
}



GtkWidget *
engine_window_new (void)
{
	GtkWidget *widget;
	
	widget = gtk_type_new (ENGINE_TYPE_WINDOW);

	return widget;
}

void 
engine_window_add_pgn_engine (EngineWindow *window, const char *filename)
{
	EngineWindowPrivate *priv;
	EngineView *view;
	GObject *engine;
	GtkTreeIter parent, child;
	GtkTreePath *path;
	
	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_ENGINE_WINDOW (window));

	priv = window->priv;

	if (!gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->store), &parent, priv->paths[ROW_PGN]))
		return;

	engine = engine_pgn_new (filename);
	view = engine_get_engine_view (ENGINE (engine));
	gtk_widget_show (GTK_WIDGET (view));
	
	gtk_tree_store_append (priv->store, &child, &parent); 
	gtk_tree_store_set (priv->store, &child,
			    COL_ICON, GNOME_STOCK_BLANK,
			    COL_TITLE, g_basename (filename), 
			    COL_ENGINE, engine, -1);

	g_object_unref (engine);
	
	gtk_notebook_append_page (priv->notebook, GTK_WIDGET (view), NULL);
	gtk_notebook_set_current_page (priv->notebook, -1);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &child);
	gtk_tree_view_expand_to_path (GTK_TREE_VIEW (priv->view), path);
	gtk_tree_selection_select_path (priv->selection, path);
	gtk_tree_path_free (path);
}

void 
engine_window_add_local_engine (EngineWindow *window, char **arg)
{
	EngineWindowPrivate *priv;
	EngineView *view;
	GObject *engine;
	GtkTreeIter parent, child;
	GtkTreePath *path;
	
	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_ENGINE_WINDOW (window));

	priv = window->priv;

	if (!gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->store), &parent, priv->paths[ROW_PROGRAMS]))
		return;

	engine = engine_local_new (arg);
	view = engine_get_engine_view (ENGINE (engine));
	gtk_widget_show (GTK_WIDGET (view));
	
	gtk_tree_store_append (priv->store, &child, &parent); 
	gtk_tree_store_set (priv->store, &child,
			    COL_ICON, GNOME_STOCK_BLANK,
			    COL_TITLE, g_basename (arg[0]), 
			    COL_ENGINE, engine, -1);

	g_object_unref (engine);
	
	gtk_notebook_append_page (priv->notebook, GTK_WIDGET (view), NULL);
	gtk_notebook_set_current_page (priv->notebook, -1);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &child);
	gtk_tree_view_expand_to_path (GTK_TREE_VIEW (priv->view), path);
	gtk_tree_selection_select_path (priv->selection, path);
	gtk_tree_path_free (path);
}

void 
engine_window_add_ics_engine (EngineWindow *window,
			      const char *host,
			      const char *port,
			      const char *user,
			      const char *pass,
			      const char *telnetprogram)
{
	EngineWindowPrivate *priv;
	EngineView *view;
	GObject *engine;
	GtkTreeIter parent, child;
	GtkTreePath *path;
	
	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_ENGINE_WINDOW (window));

	priv = window->priv;

	if (!gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->store), &parent, priv->paths[ROW_SERVERS]))
		return;

	engine = engine_ics_new (host, port, user, pass, telnetprogram);

	if (!engine)
        {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new ( GTK_WINDOW(window),
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  "Error connecting server '%s'",
                                  host);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy ( GTK_WIDGET(dialog));
		return;
	}

	view = engine_get_engine_view (ENGINE (engine));
	gtk_widget_show (GTK_WIDGET (view));
	
	gtk_tree_store_append (priv->store, &child, &parent); 
	gtk_tree_store_set (priv->store, &child,
			    COL_ICON, GNOME_STOCK_BLANK,
			    COL_TITLE, host, 
			    COL_ENGINE, engine, -1);
	
	g_object_unref (engine);

	gtk_notebook_append_page (priv->notebook, GTK_WIDGET (view), NULL);
	gtk_notebook_set_current_page (priv->notebook, -1);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &child);
	gtk_tree_view_expand_to_path (GTK_TREE_VIEW (priv->view), path);
	gtk_tree_selection_select_path (priv->selection, path);
	gtk_tree_path_free (path);
}

void 
engine_window_add_null_engine (EngineWindow *window)
{
	EngineWindowPrivate *priv;
	EngineView *view;
	GObject *engine;
	GtkTreeIter parent, child;
	GtkTreePath *path;
	
	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_ENGINE_WINDOW (window));

	priv = window->priv;

	if (!gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->store), &parent, priv->paths[ROW_SCRATCH]))
		return;

	engine = engine_null_new ();
	view = engine_get_engine_view (ENGINE (engine));
	gtk_widget_show (GTK_WIDGET (view));
	
	gtk_tree_store_append (priv->store, &child, &parent); 
	gtk_tree_store_set (priv->store, &child,
			    COL_ICON, GNOME_STOCK_BLANK,
			    COL_TITLE, "Scratch", 
			    COL_ENGINE, engine, -1);

	g_object_unref (engine);
	
	gtk_notebook_append_page (priv->notebook, GTK_WIDGET (view), NULL);
	gtk_notebook_set_current_page (priv->notebook, -1);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->store), &child);
	gtk_tree_view_expand_to_path (GTK_TREE_VIEW (priv->view), path);
	gtk_tree_selection_select_path (priv->selection, path);
	gtk_tree_path_free (path);
}

int 
engine_window_engine_count (EngineWindow *window)
{
	EngineWindowPrivate *priv;

	g_return_val_if_fail (window != NULL, 0);
	g_return_val_if_fail (IS_ENGINE_WINDOW (window), 0);

	priv = window->priv;
	
	return 0;
}

static void
tree_selection_changed_cb (GtkTreeSelection *selection, gpointer data)
{
	EngineWindow *window = ENGINE_WINDOW (data);
	EngineWindowPrivate *priv;
        GtkTreeIter iter;
        GtkTreeModel *model;
	Engine *engine;

	priv = window->priv;
        if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		EngineView *view;
		int num;
		
                gtk_tree_model_get (model, &iter, COL_ENGINE, &engine, -1);
		
		view = engine_get_engine_view (engine);
		num =  gtk_notebook_page_num (priv->notebook, GTK_WIDGET (view));
		gtk_notebook_set_current_page (priv->notebook, num);

		g_object_unref (engine);
	}
}
