/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* board-window.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <config.h>
#include "board_window_menu.h"
#include "board-window.h"

static void class_init (BoardWindowClass *klass);
static void init (BoardWindow *window);
static void destroy (GtkObject *object);

static void title_cb (GtkWidget *widget, const char *title, gpointer data);
static void bw_switch_page_cb (GtkNotebook *nb, GtkNotebook *page, gint page_num, gpointer user_data);

enum columns {
	COL_ICON,
	COL_TITLE,
	COL_COUNT
};

static GnomeAppClass *parent_class = NULL;

struct _BoardWindowPrivate {
	GtkTreeStore *store;
	GtkWidget *view;
	
	GtkNotebook *view_nb;
	GtkNotebook *info_nb;

	gint num;
};

GtkType
board_window_get_type (void)
{
  static GtkType type = 0;

  if (type == 0)
    {
      static const GtkTypeInfo info =
      {
        "BoardWindow",
        sizeof (BoardWindow),
        sizeof (BoardWindowClass),
        (GtkClassInitFunc) class_init,
        (GtkObjectInitFunc) init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      type = gtk_type_unique (gnome_app_get_type (), &info);
    }

  return type;
}

static void
class_init (BoardWindowClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (gnome_app_get_type ());
	
	object_class->destroy = destroy;
}

static void
init (BoardWindow *window)
{
	BoardWindowPrivate *priv;
	GtkWidget *vpane, *hpane;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeIter iter1;
	
	gnome_app_construct (GNOME_APP (window), "gnome-chess", NULL); 
	
	priv = g_new0 (BoardWindowPrivate, 1);

	window->priv = priv;

	hpane = gtk_hpaned_new ();
	gtk_widget_show (hpane);
	
	/* The engine tree */
	priv->store = gtk_tree_store_new (COL_COUNT, G_TYPE_STRING, G_TYPE_STRING);

	priv->view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (priv->store));
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->view), FALSE);
	
	column = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->view), column);
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_add_attribute (column, renderer, "stock-id", COL_ICON);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_add_attribute (column, renderer, "text", COL_TITLE);

	gtk_widget_show (priv->view);

	gtk_paned_pack1 (GTK_PANED (hpane), GTK_WIDGET (priv->view), TRUE, TRUE);

	gtk_tree_store_append (priv->store, &iter1, NULL); 
	gtk_tree_store_set (priv->store, &iter1,
			    COL_ICON, GNOME_STOCK_MULTIPLE_FILE,
			    COL_TITLE, "PGN Files", -1);

	gtk_tree_store_append (priv->store, &iter1, NULL); 
	gtk_tree_store_set (priv->store, &iter1,
			    COL_ICON, GTK_STOCK_EXECUTE,
			    COL_TITLE, "Programs", -1);

	gtk_tree_store_append (priv->store, &iter1, NULL); 
	gtk_tree_store_set (priv->store, &iter1,
			    COL_ICON, GNOME_STOCK_TRASH_FULL,
			    COL_TITLE, "Scratch", -1);	

	gtk_tree_store_append (priv->store, &iter1, NULL); 
	gtk_tree_store_set (priv->store, &iter1,
			    COL_ICON, GNOME_STOCK_MENU_BOOK_RED,
			    COL_TITLE, "Servers", -1);
	
                    
	/* The game and info views */
	vpane = gtk_vpaned_new ();
	gtk_paned_pack2 (GTK_PANED (hpane), GTK_WIDGET (vpane), TRUE, TRUE);
	gtk_widget_show (vpane);
	
	priv->view_nb = GTK_NOTEBOOK (gtk_notebook_new ());
	gtk_widget_show (GTK_WIDGET (priv->view_nb));
	gtk_paned_pack1 (GTK_PANED (vpane), GTK_WIDGET (priv->view_nb), TRUE, TRUE);
	
	priv->info_nb = GTK_NOTEBOOK (gtk_notebook_new ());
	gtk_widget_show (GTK_WIDGET (priv->info_nb));
	gtk_paned_pack2 (GTK_PANED (vpane), GTK_WIDGET (priv->info_nb), TRUE, TRUE);

	gnome_app_create_menus_with_data (GNOME_APP (window),
					  board_window_menu,
					 (gpointer) window);

	board_window_set_program_menu (window);
	board_window_set_server_menu (window);
	gtk_notebook_set_page(GTK_NOTEBOOK(priv->view_nb),0);

	gtk_signal_connect_after (GTK_OBJECT (priv->view_nb), "switch_page",
				  (GtkSignalFunc) bw_switch_page_cb, window);
	
	gnome_app_set_contents (GNOME_APP (window), 
				GTK_WIDGET (hpane));
}



GtkWidget *
board_window_new (void)
{
	GtkWidget *widget;
	
	widget = gtk_type_new (board_window_get_type ());

	return widget;
}

void 
board_window_add_view (BoardWindow *window, GameView *view)
{
	BoardWindowPrivate *priv;
	GtkWidget *lbl;
	char *txt;

	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_BOARD_WINDOW (window));
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	priv = window->priv;

	gtk_signal_connect (GTK_OBJECT (view), "title",
			    (GtkSignalFunc) title_cb, window);

	priv->num++;
	txt = g_strdup_printf (_("Board %d"), priv->num);
	lbl = gtk_label_new (txt);
	g_free (txt);
	gtk_widget_show (lbl);

	gtk_widget_show (GTK_WIDGET (view));

	gtk_notebook_append_page (priv->view_nb, GTK_WIDGET (view), lbl);
	gtk_notebook_set_page (GTK_NOTEBOOK (priv->view_nb), -1);
	board_window_set_game_menu (window);
}

static void
board_window_real_remove (BoardWindow *window, gint page_num)
{
	BoardWindowPrivate *priv;

	priv = window->priv;
	
	gtk_notebook_remove_page (priv->view_nb, page_num);
	if (g_list_length(gtk_container_children(GTK_CONTAINER(priv->view_nb))) == 0)
		gtk_main_quit();
}

void 
board_window_remove_view (BoardWindow *window, GameView *view)
{
	BoardWindowPrivate *priv;
	gint page_num;

	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_BOARD_WINDOW (window));
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	priv = window->priv;
	
	page_num = gtk_notebook_page_num (priv->view_nb, GTK_WIDGET (view));
	board_window_real_remove (window, page_num);
}

void
board_window_remove_current_view (BoardWindow *window)
{
	BoardWindowPrivate *priv;
	gint page_num;

	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_BOARD_WINDOW (window));
	
	priv = window->priv;
	
	page_num = gtk_notebook_get_current_page (priv->view_nb);
	if (page_num < 0) 
		return;
	
	board_window_real_remove (window, page_num);
}

void
board_window_remove_all_views (BoardWindow *window)
{
	BoardWindowPrivate *priv;
	gint page_num;

	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_BOARD_WINDOW (window));

	priv = window->priv;
	
	while ((page_num = gtk_notebook_get_current_page (priv->view_nb)) >= 0)
	       board_window_real_remove (window, page_num);
}

GameView *
board_window_get_current_view (BoardWindow *window)
{
	BoardWindowPrivate *priv;
	GtkWidget *view = NULL;
	gint page_num;

	g_return_val_if_fail (window != NULL, NULL);
	g_return_val_if_fail (IS_BOARD_WINDOW (window), NULL);
	
	priv = window->priv;

	page_num = gtk_notebook_get_current_page (priv->view_nb);
	if (page_num >= 0) {
		view =  gtk_notebook_get_nth_page (GTK_NOTEBOOK (priv->view_nb), page_num);
		return GAME_VIEW (view);
	}
	
	return NULL;
}

void 
board_window_add_info (BoardWindow *window, const char *text, GtkWidget *child)
{
	BoardWindowPrivate *priv;
	GtkWidget *lbl;

	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_BOARD_WINDOW (window));
	g_return_if_fail (text != NULL);
	g_return_if_fail (child != NULL);
	g_return_if_fail (GTK_IS_WIDGET (child));

	priv = window->priv;

	lbl = gtk_label_new (text);
	gtk_widget_show (lbl);

	gtk_widget_show (child);

	gtk_notebook_append_page (priv->info_nb, child, lbl);
}

void 
board_window_remove_info (BoardWindow *window, GtkWidget *child)
{
	BoardWindowPrivate *priv;
	gint page_num;

	g_return_if_fail (window != NULL);
	g_return_if_fail (IS_BOARD_WINDOW (window));
	g_return_if_fail (child != NULL);
	g_return_if_fail (GTK_IS_WIDGET (child));

	priv = window->priv;
	
	page_num = gtk_notebook_page_num (priv->info_nb, child);
	if (page_num < 0)
		return;
	
	gtk_notebook_remove_page (priv->info_nb, page_num);
}

static void 
destroy (GtkObject *object)
{
	BoardWindow *window;
	BoardWindowPrivate *priv;

	window = BOARD_WINDOW (object);
	priv = window->priv;

	g_free (priv);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);

	gtk_main_quit ();
}

static void
title_cb (GtkWidget *view, const char *title, gpointer data)
{
	BoardWindow *window = BOARD_WINDOW (data);
	BoardWindowPrivate *priv;
	GtkWidget *lbl;
		
	priv = window->priv;
	
	lbl = gtk_notebook_get_tab_label (priv->view_nb, view);
	
	gtk_label_set_text (GTK_LABEL (lbl), title);
}

static void
bw_switch_page_cb (GtkNotebook *nb, GtkNotebook *page,
		   gint page_num, gpointer user_data)
{
	BoardWindow *window = user_data;
	
	board_window_set_game_menu (window);
}

