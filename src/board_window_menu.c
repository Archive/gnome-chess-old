/* 
 * Copyright (C) 1999 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>
#include <glade/glade.h>
#include "prefs.h"
#include "common.h"
#include "pgn.h"
#include "engine-registry.h"
#include "engine_null.h"
#include "engine_local.h"
#include "engine_ics.h"
#include "engine-pgn.h"
#include "board-window.h"
#include "game-view.h"
#include "dialogs.h"
#include "board_window_menu.h"

extern GtkWidget *main_board_window;

static GnomeUIInfo file_play_menu[] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo file_server_menu[] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo file_menu[] = {

	GNOMEUIINFO_MENU_OPEN_ITEM (board_window_menu_open, NULL),

	GNOMEUIINFO_MENU_SAVE_AS_ITEM (board_window_menu_save_as, NULL),

	GNOMEUIINFO_SEPARATOR, 

	GNOMEUIINFO_SUBTREE_STOCK (N_("Programs"), file_play_menu, GNOME_STOCK_MENU_EXEC),

	GNOMEUIINFO_SUBTREE_STOCK (N_("_Servers"),  file_server_menu, GNOME_STOCK_MENU_BOOK_RED),

	GNOMEUIINFO_SEPARATOR, 

	GNOMEUIINFO_MENU_CLOSE_ITEM (board_window_menu_close, NULL),

	GNOMEUIINFO_MENU_EXIT_ITEM (board_window_menu_exit, NULL),
	
	GNOMEUIINFO_END
};

static GnomeUIInfo games_menu[] = {

	GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {

	GNOMEUIINFO_MENU_PREFERENCES_ITEM (board_window_menu_prefs, NULL),
	
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {

	GNOMEUIINFO_MENU_ABOUT_ITEM (board_window_menu_about, NULL),
	
	GNOMEUIINFO_END
};

GnomeUIInfo board_window_menu[] = {

	GNOMEUIINFO_MENU_FILE_TREE (file_menu),

	GNOMEUIINFO_MENU_GAME_TREE (games_menu),

	GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu),

	GNOMEUIINFO_MENU_HELP_TREE (help_menu),

	GNOMEUIINFO_END
};

void
board_window_set_program_menu (BoardWindow *window)
{
	GList *programs, *l;

	/* Hopefully removing 20 children will be quite enough */
	gnome_app_remove_menus (GNOME_APP (window), "File/Programs/", 20);

	programs = prefs_get_programs ();
	for (l = programs; l != NULL; l = l->next) {
		PrefsProgram *pp = l->data;
		
		{
			GnomeUIInfo info[]= {
				{GNOME_APP_UI_ITEM, 
				 pp->name, NULL, 
				 board_window_menu_program, pp,
				 NULL, GNOME_APP_PIXMAP_NONE, 
				 NULL, 0, 0, NULL},
				
				GNOMEUIINFO_END};
		
			gnome_app_insert_menus (GNOME_APP (window), 
						"File/Programs/", info);
		}
	}
}

void
board_window_set_server_menu (BoardWindow *window)
{
	GList *servers, *l;

	/* Hopefully removing 20 children will be quite enough */
	gnome_app_remove_menus (GNOME_APP (window), "File/Servers/", 20);
	
	servers = prefs_get_servers ();
	for (l = servers; l != NULL; l = l->next) {
		PrefsServer *ps = l->data;
		
		{
			GnomeUIInfo info[]= {
				{GNOME_APP_UI_ITEM, 
				 ps->server, NULL, 
				 board_window_menu_server, ps,
				 NULL, GNOME_APP_PIXMAP_NONE, 
				 NULL, 0, 0, NULL},
				
				GNOMEUIINFO_END};
		
			gnome_app_insert_menus (GNOME_APP (window), 
						"File/Servers/", info);
		}
	}	
}

void
board_window_set_game_menu (BoardWindow *window)
{
	GameView *view;
	BonoboDockItem *item;
	GtkWidget *shell;
	gint pos;
	
	/* Hopefully removing 20 children will be quite enough */
	gnome_app_remove_menus (GNOME_APP (window), "_Game/", 20);

	item = gnome_app_get_dock_item_by_name (GNOME_APP (window),
						GNOME_APP_MENUBAR_NAME);
	shell = bonobo_dock_item_get_child (item);
	shell = gnome_app_find_menu_pos (shell, "Game/", &pos);

	view = board_window_get_current_view (window);
	game_view_fill_menu (view, GTK_MENU_SHELL (shell), 0);
}

void 
board_window_menu_new (GtkWidget *widget, void *data) 
{
	BoardWindow *window = BOARD_WINDOW (data);

	engine_null_new (window);
}

void 
board_window_menu_open (GtkWidget *widget, void *data) 
{
	BoardWindow *window = BOARD_WINDOW (data);
	char *filename;
	
	filename = filesel ("Load PGN", "noname.pgn");
	if (filename)
		engine_pgn_new (window, filename);
}

