/* 
 * Copyright (C) 2000 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>
#include "dialogs.h"

#define GLADE_FILE "level.glade"

enum { BUTTON_OK = 0, BUTTON_CANCEL };

static gboolean
dialog_level_impl (GladeXML *gui, gint *moves, gint *time)
{
	GtkWidget *dialog, *move_spin, *time_spin;
	gint bval = BUTTON_OK;

	dialog = glade_xml_get_widget (gui, "dialog");
	move_spin = glade_xml_get_widget (gui, "move_spin");
	time_spin = glade_xml_get_widget (gui, "time_spin");

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (move_spin), *moves);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (time_spin), *time);
	
	gnome_dialog_set_default (GNOME_DIALOG (dialog), BUTTON_CANCEL);
	gtk_widget_show_all (GNOME_DIALOG (dialog)->vbox);

	bval = gnome_dialog_run (GNOME_DIALOG (dialog));
	switch (bval) {
		
	case BUTTON_OK:
		*moves = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (move_spin));
		*time = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (time_spin));
		break;
		
	case BUTTON_CANCEL:
		gnome_dialog_close (GNOME_DIALOG (dialog));

	case -1:
		return FALSE;
		
	default:
		g_assert_not_reached();
		break;
	}

	/* If the user canceled we have already returned */
	gnome_dialog_close (GNOME_DIALOG (dialog));

	return TRUE;
}

/*
 * Wrapper around dialog_search_replace_impl
 * To libglade'ify it
 */
gboolean
dialog_level (gint *moves, gint *level_time)
{
	GladeXML *gui;
	gboolean result;
	
	gui = glade_xml_new (GNOMECHESS_GLADEDIR "/" GLADE_FILE ,
			     NULL, GETTEXT_PACKAGE);
	if (!gui) {
		printf ("Could not find " GLADE_FILE "\n");
		return FALSE;

	}

	result = dialog_level_impl (gui, moves, level_time);
	g_object_unref (gui);

	return result;
}












