/* 
 * Copyright (C) 2000 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>
#include "dialogs.h"

#define GLADE_FILE "login.glade"

enum { BUTTON_OK = 0, BUTTON_CANCEL };

static gboolean
dialog_login_impl (GladeXML *gui, gchar **user, gchar **pass)
{
	GtkWidget *dialog, *user_entry, *pass_entry;
	gint bval = BUTTON_OK;

	dialog = glade_xml_get_widget (gui, "dialog");
	user_entry = glade_xml_get_widget (gui, "user_entry");
	pass_entry = glade_xml_get_widget (gui, "pass_entry");

	if (*user)
		gtk_entry_set_text(GTK_ENTRY(user_entry), *user);
	if (*pass)
		gtk_entry_set_text(GTK_ENTRY(pass_entry), *pass);
	
	gnome_dialog_set_default (GNOME_DIALOG (dialog), BUTTON_CANCEL);
	gtk_widget_show_all (GNOME_DIALOG (dialog)->vbox);

	bval = gnome_dialog_run (GNOME_DIALOG (dialog));
	switch (bval) {
		
	case BUTTON_OK:
		if (*user)
			g_free (*user);
		if (*pass)
			g_free (*pass);
		*user = gtk_editable_get_chars (GTK_EDITABLE (user_entry), 0, -1);
		*pass = gtk_editable_get_chars (GTK_EDITABLE (pass_entry), 0, -1);
		break;

	case BUTTON_CANCEL:
		gnome_dialog_close (GNOME_DIALOG (dialog));

	case -1: /* close window */
		return FALSE;
		
	default:
		g_assert_not_reached();
		break;
	}

	/* If the user canceled we have already returned */
	gnome_dialog_close (GNOME_DIALOG (dialog));

	return TRUE;
}

/*
 * Wrapper around dialog_search_replace_impl
 * To libglade'ify it
 */
gboolean
dialog_login (gchar **user, gchar **pass)
{
	GladeXML *gui;
	gboolean result;
	
	gui = glade_xml_new (GNOMECHESS_GLADEDIR "/" GLADE_FILE ,
			     NULL, GETTEXT_PACKAGE);
	if (!gui) {
		printf ("Could not find " GLADE_FILE "\n");
		return FALSE;
	}

	result = dialog_login_impl (gui, user, pass);
	g_object_unref (gui);

	return result;
}

