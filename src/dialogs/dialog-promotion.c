/* 
 * Copyright (C) 2000 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>
#include "dialogs.h"

#define GLADE_FILE "promotion.glade"

enum { BUTTON_QUEEN = 0, BUTTON_ROOK, BUTTON_BISHOP, BUTTON_KNIGHT };

static gint
dialog_promotion_impl (GladeXML *gui)
{
	GtkWidget *dialog;
	gint btn = BUTTON_QUEEN;

	dialog = glade_xml_get_widget (gui, "dialog");
		
	gnome_dialog_set_default (GNOME_DIALOG (dialog), btn);
	gtk_window_set_policy (GTK_WINDOW (dialog), FALSE, TRUE, FALSE);

	gtk_widget_show_all (GNOME_DIALOG (dialog)->vbox);

	btn = gnome_dialog_run_and_close (GNOME_DIALOG (dialog));

	return btn;
}

gint
dialog_promotion( void )
{
	gint btn;
	GladeXML *gui;
	
	gui = glade_xml_new (GNOMECHESS_GLADEDIR "/" GLADE_FILE ,
			     NULL, GETTEXT_PACKAGE);
	if (!gui) {
		printf ("Could not find " GLADE_FILE "\n");
		return -1;
	}

	btn = dialog_promotion_impl (gui);
	g_object_unref (gui);

	return btn;
}

