#ifndef GNOMECHESS_DIALOGS_H
#define GNOMECHESS_DIALOGS_H

#include <glib.h>
#include "../pgn.h"

gint     dialog_promotion (void);
gboolean dialog_login (gchar **user, gchar **pass);
gboolean dialog_level (int *moves, int *level_time);
void     dialog_prefs (void);
Pgn_Tag *dialog_pgn( Pgn_Tag *tag );

#endif /* GNOMECHESS_DIALOGS_H */
