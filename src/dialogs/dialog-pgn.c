/* 
 * Copyright (C) 2000 JP Rosevear
 * Copyright (C) 2001 Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>
#include "dialogs.h"
#include "../pgn.h"

#define GLADE_FILE "pgn.glade"

static Pgn_Tag *
dialog_pgn_impl (GladeXML *gui, Pgn_Tag *tag)
{
        gint btn;
	GtkWidget *dialog;
	GtkWidget *event,*site,*date,*round;
	GtkWidget *white,*black,*result;

	dialog = glade_xml_get_widget (gui, "dialog");
		
	event = glade_xml_get_widget (gui, "event_entry");
	site = glade_xml_get_widget (gui, "site_entry");
	date = glade_xml_get_widget (gui, "date_entry");
	round = glade_xml_get_widget (gui, "round_entry");
	white = glade_xml_get_widget (gui, "white_entry");
	black = glade_xml_get_widget (gui, "black_entry");
	result = glade_xml_get_widget (gui, "result_entry");

	if (tag)
	  {	
	    gtk_entry_set_text(GTK_ENTRY(event), tag->event);
	    gtk_entry_set_text(GTK_ENTRY(site), tag->site);
	    gtk_entry_set_text(GTK_ENTRY(date), tag->date);
	    gtk_entry_set_text(GTK_ENTRY(round), tag->round);
	    gtk_entry_set_text(GTK_ENTRY(white), tag->white);
	    gtk_entry_set_text(GTK_ENTRY(black), tag->black);
	    gtk_entry_set_text(GTK_ENTRY(result), tag->result);
	  }

	gtk_window_set_policy (GTK_WINDOW (dialog), FALSE, TRUE, FALSE);

	gtk_widget_show_all (GNOME_DIALOG (dialog)->vbox);

	btn = gnome_dialog_run (GNOME_DIALOG (dialog));

	if (btn == 0) {
       	  if (tag == 0)
	    tag = g_new0(Pgn_Tag, 1);
	  tag->event  = gtk_editable_get_chars(GTK_EDITABLE(event),0,-1);
	  tag->site   = gtk_editable_get_chars(GTK_EDITABLE(site),0,-1);
	  tag->date   = gtk_editable_get_chars(GTK_EDITABLE(date),0,-1);
	  tag->round  = gtk_editable_get_chars(GTK_EDITABLE(round),0,-1);
	  tag->white  = gtk_editable_get_chars(GTK_EDITABLE(white),0,-1);
	  tag->black  = gtk_editable_get_chars(GTK_EDITABLE(black),0,-1);
	  tag->result = gtk_editable_get_chars(GTK_EDITABLE(result),0,-1);
	}

	gnome_dialog_close(GNOME_DIALOG (dialog));

	return tag;
}

Pgn_Tag * dialog_pgn( Pgn_Tag *tag  )
{
	GladeXML *gui;

	gui = glade_xml_new (GNOMECHESS_GLADEDIR "/" GLADE_FILE ,
			     NULL, GETTEXT_PACKAGE);

	if (!gui) {
		printf ("Could not find " GLADE_FILE "\n");
		return NULL;
	}

	tag = dialog_pgn_impl (gui, tag);
	g_object_unref (gui);

	return tag;
}

