/* 
 * Copyright (C) 2000 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <config.h>

#include <gnome.h>
#include <glade/glade.h>
#include "makros.h"
#include "prefs.h"
#include "dialogs.h"

#define GLADE_FILE "prefs.glade"

enum { BUTTON_OK = 0, BUTTON_APPLY, BUTTON_CLOSE, BUTTON_HELP };

enum { USER_COL, SERVER_COL, PORT_COL, SERVER_PTR_COL, LAST_SERVER_COL };
enum { NAME_COL, PATH_COL, PROGRAM_PTR_COL, LAST_PROGRAM_COL };

typedef struct _PrefsState
{
	GladeXML *gui;
	
	GtkWidget *pb;

	GtkWidget *dark_color;
	GtkWidget *light_color;
	GtkWidget *piece_dir;

	GtkWidget *beep;
	GtkWidget *promote_to_queen;
	GtkWidget *autoflag;
	GtkWidget *sa_notation;
	GtkWidget *co_notation;

	GSList *servers;
	GtkListStore *server_store;
	GtkWidget *server_dlg;
	GtkWidget *server_list;
	GtkWidget *server_name;
	GtkWidget *server_port;
	GtkWidget *server_username;
	GtkWidget *server_password;
	GtkWidget *server_connect;

	GtkWidget *telnet_program;
	
	GSList *programs;
	GtkListStore *program_store;
	GtkWidget *program_dlg;
	GtkWidget *program_list;
	GtkWidget *program_name;
	GtkWidget *program_loc;

	GtkWidget *colors[18];
} PrefsState;

static PrefsState *state = NULL;

static void
cb_color_changed (GnomeColorPicker *gcp, guint r, guint g, 
		  guint b, guint a, gpointer data) 
{
	void (*func) (guint8, guint8, guint8) = data;
	guint8 r8, g8, b8, a8;
	
	gnome_color_picker_get_i8 (gcp, &r8, &g8, &b8, &a8);
	func (r8, g8, b8);
}

static void
cb_palette_color_changed (GnomeColorPicker *gcp, guint r, guint g, 
			  guint b, guint a, gpointer data) 
{
	guint color = GPOINTER_TO_INT (data);
	guint8 r8, g8, b8, a8;
	
	gnome_color_picker_get_i8 (gcp, &r8, &g8, &b8, &a8);
	prefs_set_palette_color (color, r8, g8, b8);
}

static void
cb_toggle_changed (GtkToggleButton *toggle, gpointer data) 
{
	void (*func) (gboolean) = data;

	func (gtk_toggle_button_get_active (toggle));
}

static void
cb_sa_notation_changed (GtkToggleButton *toggle, gpointer data) 
{
	if (gtk_toggle_button_get_active (toggle))
		prefs_set_notation (SAN);
}

static void
cb_co_notation_changed (GtkToggleButton *toggle, gpointer data) 
{
	if (gtk_toggle_button_get_active (toggle))
		prefs_set_notation (COORD);
}

static void
cb_entry_changed (GtkWidget *widget, gpointer user_data)
{
	void (*func) (const char *) = user_data;

	func (gtk_entry_get_text (GTK_ENTRY (widget)));
}

static gboolean
foreach_program (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	PrefsProgram *pp = NULL;
	GSList **programs = data;

	gtk_tree_model_get (model, iter, PROGRAM_PTR_COL, &pp, -1);
	*programs = g_slist_append (*programs, pp);
	
	return FALSE;
}

static void
run_program_dialog (PrefsState *dlg, GtkTreeIter *iter) 
{
	PrefsProgram *pp = NULL;
	GSList *programs = NULL;
	gint btn;

	if (iter != NULL) {
		gtk_tree_model_get (GTK_TREE_MODEL (dlg->program_store), iter, PROGRAM_PTR_COL, &pp, -1);

		gtk_entry_set_text (GTK_ENTRY (dlg->program_name), pp->name);
		gtk_entry_set_text (GTK_ENTRY (dlg->program_loc), pp->location);
	} else {
		gtk_entry_set_text (GTK_ENTRY (dlg->program_name), "Program");
		gtk_entry_set_text (GTK_ENTRY (dlg->program_loc), "/usr/bin/chess");
		
		pp = g_new0 (PrefsProgram, 1);
	}

	
	btn = gtk_dialog_run (GTK_DIALOG (dlg->program_dlg));
	if (btn == GTK_RESPONSE_OK) {
		pp->name = gtk_editable_get_chars (GTK_EDITABLE (dlg->program_name), 0, -1);
  		pp->location = gtk_editable_get_chars (GTK_EDITABLE (dlg->program_loc), 0, -1);
		if (iter != NULL) {
			gtk_list_store_set (dlg->program_store, iter, 
					    NAME_COL, pp->name,
					    PATH_COL, pp->location, -1);
		} else {
			GtkTreeIter iter;

			gtk_list_store_append (dlg->program_store, &iter);
			gtk_list_store_set (dlg->program_store, &iter, 
					    NAME_COL, pp->name,
					    PATH_COL, pp->location,
					    PROGRAM_PTR_COL, pp, -1);
		}

		gtk_tree_model_foreach (GTK_TREE_MODEL (dlg->program_store), foreach_program, &programs);
		prefs_set_programs (programs);
	}
	gtk_widget_hide (dlg->program_dlg);
}

static void
cb_add_program (GtkWidget *widget, gpointer user_data)
{
	PrefsState *dlg = user_data;

	run_program_dialog (dlg, NULL);
}

static void
cb_mod_program (GtkWidget *widget, gpointer user_data)
{
	PrefsState *dlg = user_data;
	GtkTreeSelection *sel;
	GtkTreeIter iter;

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->program_list));
	if (!gtk_tree_selection_get_selected (sel, NULL, &iter))
		return;
	
	run_program_dialog (dlg, &iter);
}

static void
cb_del_program (GtkWidget *widget, gpointer user_data)
{
	PrefsState *dlg = user_data;
	GSList *programs = NULL;
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	
	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->program_list));
	if (!gtk_tree_selection_get_selected (sel, NULL, &iter))
		return;

	gtk_list_store_remove (dlg->program_store, &iter);

	gtk_tree_model_foreach (GTK_TREE_MODEL (dlg->program_store), foreach_program, &programs);
	prefs_set_programs (programs);
}

static gboolean
foreach_server (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	PrefsServer *ps = NULL;
	GSList **servers = data;

	gtk_tree_model_get (model, iter, SERVER_PTR_COL, &ps, -1);
	*servers = g_slist_append (*servers, ps);
	
	return FALSE;
}

static void
run_server_dialog (PrefsState *dlg, GtkTreeIter *iter) 
{
	PrefsServer *ps = NULL;
	GSList *servers = NULL;
	gint btn;

	if (iter != NULL) {
		gtk_tree_model_get (GTK_TREE_MODEL (dlg->server_store), iter, SERVER_PTR_COL, &ps, -1);
		gtk_entry_set_text (GTK_ENTRY (dlg->server_name), ps->server);
		gtk_entry_set_text (GTK_ENTRY (dlg->server_port), ps->port);
		gtk_entry_set_text (GTK_ENTRY (dlg->server_username), ps->user);
		gtk_entry_set_text (GTK_ENTRY (dlg->server_password), ps->password);
		gtk_entry_set_text (GTK_ENTRY (dlg->server_connect), ps->connect);
	} else {
		gtk_entry_set_text (GTK_ENTRY (dlg->server_name), "chess.server.com");
		gtk_entry_set_text (GTK_ENTRY (dlg->server_port), "5000");
		gtk_entry_set_text (GTK_ENTRY (dlg->server_username), g_get_user_name ());
		gtk_entry_set_text (GTK_ENTRY (dlg->server_password), "");
		gtk_entry_set_text (GTK_ENTRY (dlg->server_connect), prefs_get_telnetprogram ());
		
		ps = g_new0 (PrefsServer, 1);
	}

	
	btn = gtk_dialog_run (GTK_DIALOG (dlg->server_dlg));
	if (btn == GTK_RESPONSE_OK) {
		ps->server = gtk_editable_get_chars (GTK_EDITABLE (dlg->server_name), 0, -1);
  		ps->port = gtk_editable_get_chars (GTK_EDITABLE (dlg->server_port), 0, -1);
  		ps->user = gtk_editable_get_chars (GTK_EDITABLE (dlg->server_username), 0, -1);
  		ps->password = gtk_editable_get_chars (GTK_EDITABLE (dlg->server_password), 0, -1);
  		ps->connect = gtk_editable_get_chars (GTK_EDITABLE (dlg->server_connect), 0, -1);
		if (iter != NULL) {
			gtk_list_store_set (dlg->server_store, iter, 
					    USER_COL, ps->user,
					    SERVER_COL, ps->server,
					    PORT_COL, ps->port, -1);
		} else {
			GtkTreeIter iter;
			
			gtk_list_store_append (dlg->server_store, &iter);
			gtk_list_store_set (dlg->server_store, &iter, 
					    USER_COL, ps->user,
					    SERVER_COL, ps->server,
					    PORT_COL, ps->port, 
					    SERVER_PTR_COL, ps, -1);
		}

		gtk_tree_model_foreach (GTK_TREE_MODEL (dlg->server_store), foreach_server, &servers);
		prefs_set_servers (servers);
	}
	gtk_widget_hide (dlg->server_dlg);
}

static void
cb_add_server (GtkWidget *widget, gpointer user_data)
{
	PrefsState *dlg = user_data;

	run_server_dialog (dlg, NULL);
}

static void
cb_mod_server (GtkWidget *widget, gpointer user_data)
{
	PrefsState *dlg = user_data;
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	
	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->server_list));
	if (!gtk_tree_selection_get_selected (sel, NULL, &iter))
		return;
	
	run_server_dialog (dlg, &iter);
}

static void
cb_del_server (GtkWidget *widget, gpointer user_data)
{
	PrefsState *dlg = user_data;
	GSList *servers = NULL;
	GtkTreeSelection *sel;
	GtkTreeIter iter;

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->server_list));
	if (!gtk_tree_selection_get_selected (sel, NULL, &iter))
		return;

	gtk_list_store_remove (dlg->server_store, &iter);

	gtk_tree_model_foreach (GTK_TREE_MODEL (dlg->server_store), foreach_server, &servers);
 	prefs_set_servers (servers);
}

static void
dialog_prefs_destroy (GtkObject *object, gpointer user_data)
{
	prefs_sync ();
	
	prefs_free_server_list (state->servers);
	prefs_free_program_list (state->programs);
	
	g_object_unref (state->gui);

	g_free (state);
	state = NULL;
}

static void
dialog_prefs_impl (PrefsState *dlg)
{
	GtkWidget *btn;
	GSList *programs, *servers, *l;
	GtkCellRenderer *renderer;
	guint8 r, g, b;
	gint not, i;

	renderer = gtk_cell_renderer_text_new ();

	dlg->pb = glade_xml_get_widget (dlg->gui, "dialog");

	gtk_signal_connect (GTK_OBJECT (dlg->pb), "destroy",
			    GTK_SIGNAL_FUNC (dialog_prefs_destroy), dlg);

	/* Board and Pieces Page */
	dlg->light_color = glade_xml_get_widget (dlg->gui, "light_color");
	dlg->dark_color = glade_xml_get_widget (dlg->gui, "dark_color");
	dlg->piece_dir = glade_xml_get_widget (dlg->gui, "piece_dir");

	prefs_get_light_color (&r, &g, &b);
	gnome_color_picker_set_i8 (GNOME_COLOR_PICKER (dlg->light_color), r, g, b, 0);
	prefs_get_dark_color (&r, &g, &b);
	gnome_color_picker_set_i8 (GNOME_COLOR_PICKER (dlg->dark_color), r, g, b, 0);
	
	gtk_entry_set_text (GTK_ENTRY (dlg->piece_dir),
			    prefs_get_piecedir ());

	gtk_signal_connect (GTK_OBJECT (dlg->light_color), "color-set",
			    GTK_SIGNAL_FUNC (cb_color_changed), prefs_set_light_color);
	gtk_signal_connect (GTK_OBJECT (dlg->dark_color), "color-set",
			    GTK_SIGNAL_FUNC (cb_color_changed), prefs_set_dark_color);
	gtk_signal_connect (GTK_OBJECT (dlg->piece_dir), "changed",
			    GTK_SIGNAL_FUNC (cb_entry_changed), prefs_set_piecedir);

	/* Playing Page */
	dlg->beep = glade_xml_get_widget (dlg->gui, "beep");
	dlg->promote_to_queen = glade_xml_get_widget (dlg->gui, "promote_to_queen");
	dlg->autoflag = glade_xml_get_widget (dlg->gui, "autoflag");

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->beep),
				      prefs_get_beeponmove ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->promote_to_queen),
				      prefs_get_promotetoqueen ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->autoflag),
				      prefs_get_autoflag ());
	

	gtk_signal_connect (GTK_OBJECT (dlg->beep), "toggled",
			    GTK_SIGNAL_FUNC (cb_toggle_changed), prefs_set_beeponmove);
	gtk_signal_connect (GTK_OBJECT (dlg->promote_to_queen), "toggled",
			    GTK_SIGNAL_FUNC (cb_toggle_changed), prefs_set_promotetoqueen);
	gtk_signal_connect (GTK_OBJECT (dlg->autoflag), "toggled",
			    GTK_SIGNAL_FUNC (cb_toggle_changed), prefs_set_autoflag);

	dlg->sa_notation = glade_xml_get_widget (dlg->gui, "sa_notation");
	dlg->co_notation = glade_xml_get_widget (dlg->gui, "co_notation");
	not = prefs_get_notation ();
	if (not == SAN)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->sa_notation),
					      TRUE);
	else
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->co_notation),
					      TRUE);

	gtk_signal_connect (GTK_OBJECT (dlg->sa_notation), "toggled",
			    GTK_SIGNAL_FUNC (cb_sa_notation_changed), NULL);
	gtk_signal_connect (GTK_OBJECT (dlg->co_notation), "toggled",
			    GTK_SIGNAL_FUNC (cb_co_notation_changed), NULL);

	/* Chess Servers Page */
	dlg->server_store = gtk_list_store_new (LAST_SERVER_COL, 
						G_TYPE_STRING, 
						G_TYPE_STRING,
						G_TYPE_STRING,
						G_TYPE_POINTER);
	dlg->server_dlg = glade_xml_get_widget (dlg->gui, "dialog_server");
	dlg->server_list = glade_xml_get_widget (dlg->gui, "server_list");
	dlg->server_name = glade_xml_get_widget (dlg->gui, "server_name");
	dlg->server_port = glade_xml_get_widget (dlg->gui, "server_port");
	dlg->server_username = glade_xml_get_widget (dlg->gui, "server_username");
	dlg->server_password = glade_xml_get_widget (dlg->gui, "server_password");
	dlg->server_connect = glade_xml_get_widget (dlg->gui, "server_connect");


	gtk_tree_view_set_model (GTK_TREE_VIEW (dlg->server_list), GTK_TREE_MODEL (dlg->server_store));
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dlg->server_list), -1,
						     "User", renderer,
						     "text", USER_COL,
						     NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dlg->server_list), -1,
						     "Server", renderer,
						     "text", SERVER_COL,
						     NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dlg->server_list), -1,
						     "Port", renderer,
						     "text", PORT_COL,
						     NULL);

	servers = prefs_copy_server_list (prefs_get_servers ());
	for (l = servers; l != NULL; l = l->next) {
		PrefsServer *ps = l->data;		
		GtkTreeIter iter;
		
		gtk_list_store_append (dlg->server_store, &iter);
		gtk_list_store_set (dlg->server_store, &iter, 
				    USER_COL, ps->user,
				    SERVER_COL, ps->server,
				    PORT_COL, ps->port, 
				    SERVER_PTR_COL, ps, -1);
	}
	dlg->servers = servers;
	
	btn = glade_xml_get_widget (dlg->gui, "add_server");
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (cb_add_server), dlg);

	btn = glade_xml_get_widget (dlg->gui, "modify_server");
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (cb_mod_server), dlg);

	btn = glade_xml_get_widget (dlg->gui, "delete_server");
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (cb_del_server), dlg);

	/* Telnet Program */
	dlg->telnet_program = glade_xml_get_widget (dlg->gui, "telnet_program");
	gtk_entry_set_text (GTK_ENTRY (dlg->telnet_program), 
			    prefs_get_telnetprogram ()); 

	gtk_signal_connect (GTK_OBJECT (dlg->telnet_program), "changed",
			    GTK_SIGNAL_FUNC (cb_entry_changed), prefs_set_telnetprogram);

	/* Chess Programs Page */
	dlg->program_store = gtk_list_store_new (LAST_PROGRAM_COL,
						 G_TYPE_STRING,
						 G_TYPE_STRING,
						 G_TYPE_POINTER);
	dlg->program_dlg = glade_xml_get_widget (dlg->gui, "dialog_program");
	dlg->program_list = glade_xml_get_widget (dlg->gui, "program_list");
	dlg->program_name = glade_xml_get_widget (dlg->gui, "program_name");
	dlg->program_loc = glade_xml_get_widget (dlg->gui, "program_location");

	gtk_tree_view_set_model (GTK_TREE_VIEW (dlg->program_list), GTK_TREE_MODEL (dlg->program_store));
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dlg->program_list), -1,
						     "Name", renderer,
						     "text", NAME_COL,
						     NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dlg->program_list), -1,
						     "Path", renderer,
						     "text", PATH_COL,
						     NULL);
	
	programs = prefs_copy_program_list (prefs_get_programs ());
	for (l = programs; l != NULL; l = l->next) {
		PrefsProgram *pp = l->data;		
		GtkTreeIter iter;

		gtk_list_store_append (dlg->program_store, &iter);
		gtk_list_store_set (dlg->program_store, &iter, 
				    NAME_COL, pp->name,
				    PATH_COL, pp->location,
				    PROGRAM_PTR_COL, pp, -1);
	}
	dlg->programs = programs;
	
	btn = glade_xml_get_widget (dlg->gui, "add_program");
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (cb_add_program), dlg);

	btn = glade_xml_get_widget (dlg->gui, "modify_program");
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (cb_mod_program), dlg);

	btn = glade_xml_get_widget (dlg->gui, "delete_program");
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (cb_del_program), dlg);

	dlg->colors[16] = glade_xml_get_widget (dlg->gui, "foreground-colorpicker");
	dlg->colors[17] = glade_xml_get_widget (dlg->gui, "background-colorpicker");

	/* Color page */
	for (i = 0; i < 18; i++) {
		if (i < 16) {
			gchar *widget_name;

			widget_name = g_strdup_printf ("palette-%d-colorpicker", i);
			dlg->colors[i] = glade_xml_get_widget (dlg->gui, widget_name);
			g_free (widget_name);
		}

		prefs_get_palette_color (i, &r, &g, &b);
		gnome_color_picker_set_i8 (GNOME_COLOR_PICKER (dlg->colors[i]), r, g, b, 0);

		gtk_signal_connect (GTK_OBJECT (dlg->colors[i]), "color_set",
				    GTK_SIGNAL_FUNC (cb_palette_color_changed), GINT_TO_POINTER (i));
	}

	/* Dialog stuff */
	gtk_dialog_run (GTK_DIALOG (dlg->pb));
	gtk_widget_destroy (dlg->pb);
}

/*
 * Wrapper around dialog_prefs_impl
 * To libglade'ify it
 */
void
dialog_prefs (void)
{
	if (state != NULL) {
		gtk_widget_show_all (state->pb);
		return;
	}
	
	state = g_new0 (PrefsState, 1);

	state->gui = glade_xml_new (GNOMECHESS_GLADEDIR "/" GLADE_FILE ,
				    NULL, GETTEXT_PACKAGE);
	if (!state->gui) {
		printf ("Could not find " GLADE_FILE "\n");
		return;
	}

	dialog_prefs_impl (state);
}
