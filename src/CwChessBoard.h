// cwchessboard -- A GTK+ chess board widget
//
// Copyright (C) 2006 Carlo Wood
//
// Carlo Wood, Run on IRC <carlo@alinoe.com>
// RSA-1024 0x624ACAD5 1997-01-26                    Sign & Encrypt
// Fingerprint16 = 32 EC A7 B6 AC DB 65 A6  F6 F6 55 DD 1C DC FF 61
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

//! @file CwChessBoard.h
//! @brief This file contains the declaration of the widget #CwChessBoard.
//! @image html arrowsandmarkers.png

#ifndef CWCHESSBOARD_H
#define CWCHESSBOARD_H

#include <math.h>
#include <glib.h>
#include <gtk/gtkdrawingarea.h>

#ifdef __cplusplus
#define CWCHESSBOARD_INLINE inline
#define CWCHESSBOARD_DEFINE_INLINE 1
#else
#define CWCHESSBOARD_INLINE
#ifndef CWCHESSBOARD_DEFINE_INLINE
#define CWCHESSBOARD_DEFINE_INLINE 0
#endif
#endif

G_BEGIN_DECLS

GType cw_chess_board_get_type(void) G_GNUC_CONST;

#define CW_TYPE_CHESS_BOARD             (cw_chess_board_get_type())
#define CW_CHESS_BOARD(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), CW_TYPE_CHESS_BOARD, CwChessBoard))
#define CW_CHESS_BOARD_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), CW_TYPE_CHESS_BOARD,  CwChessBoardClass))
#define CW_IS_CHESS_BOARD(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CW_TYPE_CHESS_BOARD))
#define CW_IS_CHESS_BOARD_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), CW_TYPE_CHESS_BOARD))
#define CW_CHESS_BOARD_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), CW_TYPE_CHESS_BOARD, CwChessBoardClass))

typedef struct _CwChessBoard CwChessBoard;
typedef struct _CwChessBoardClass CwChessBoardClass;
typedef struct _CwChessBoardPrivate CwChessBoardPrivate;

/** @typedef CwChessBoardColorHandle
 *  @brief A color handle used for background markers.
 *
 *  The five least significant bits determine the color
 *  from a user defined color palet, used by the background squares
 *  and markers. A value of zero meaning the default background value
 *  for that square, or no marker - respectively.
 *
 *  @sa cw_chess_board_allocate_color_handle_rgb, cw_chess_board_allocate_color_handle, cw_chess_board_free_color_handle,
 *      cw_chess_board_set_background_color, cw_chess_board_get_background_color,
 *      cw_chess_board_set_background_colors, cw_chess_board_get_background_colors,
 *      cw_chess_board_set_marker_color, cw_chess_board_get_marker_color
 */
typedef unsigned char CwChessBoardColorHandle;

/** @name Creation */
//@{

/**
 * Create a new chess board widget.
 *
 * @returns newly created CwChessBoard.
 */
GtkWidget* cw_chess_board_new(void);

//@} Creation

/** @name Chess board codes */
//@{

/** @typedef CwChessBoardCode
 *  @brief A code to specify a chess piece.
 *
 * One of the following constants:
 * #empty_square, #black_pawn, #white_pawn, #black_rook, #white_rook, #black_knight, #white_knight,
 * #black_bishop, #white_bishop, #black_queen, #white_queen, #black_king or #white_king.
 *
 * @sa cw_chess_board_set_square, cw_chess_board_add_floating_piece, cw_chess_board_get_floating_piece
 */
typedef guint16 CwChessBoardCode;

// Doxygen refuses to document static variables, so fool it into thinking these are non-static.
// The comments for each constant are needed to get doxygen to show them at all.
#ifndef DOXYGEN_STATIC
#define DOXYGEN_STATIC static
#endif

/** An empty square. The value <code>1</code> will also result in an empty square. */
DOXYGEN_STATIC CwChessBoardCode const empty_square = 0;

/** A black pawn. */
DOXYGEN_STATIC CwChessBoardCode const black_pawn = 2;

/** A white pawn. */
DOXYGEN_STATIC CwChessBoardCode const white_pawn = 3;

/** A black rook. */
DOXYGEN_STATIC CwChessBoardCode const black_rook = 4;

/** A white rook. */
DOXYGEN_STATIC CwChessBoardCode const white_rook = 5;

/** A black knight. */
DOXYGEN_STATIC CwChessBoardCode const black_knight = 6;

/** A white knight. */
DOXYGEN_STATIC CwChessBoardCode const white_knight = 7;

/** A black bishop. */
DOXYGEN_STATIC CwChessBoardCode const black_bishop = 8;

/** A white bishop. */
DOXYGEN_STATIC CwChessBoardCode const white_bishop = 9;

/** A black queen. */
DOXYGEN_STATIC CwChessBoardCode const black_queen = 10;

/** A white queen. */
DOXYGEN_STATIC CwChessBoardCode const white_queen = 11;

/**
 * A black king.
 * Since the CwChessBoard widget does not do any checking,
 * it is possible to display more than one king.
 */
DOXYGEN_STATIC CwChessBoardCode const black_king = 12;

/**
 * A white king.
 * Since the CwChessBoard widget does not do any checking,
 * it is possible to display more than one king.
 */
DOXYGEN_STATIC CwChessBoardCode const white_king = 13;

//@}

#ifdef DOXYGEN
/** @struct CwChessBoard
 *  @brief A chess board widget.
 *
 * @sa cw_chess_board_new
 */
struct CwChessBoard
#else
struct _CwChessBoard
#endif
{
  GtkDrawingArea parent;
  CwChessBoardPrivate* priv;

  gint const sside;		//!< Square side in pixels (read only).
  gint const top_left_a1_x;	//!< The x coordinate of the top-left pixel of square a1 (read-only).
  gint const top_left_a1_y;	//!< The y coordinate of the top-left pixel of square a1 (read-only).
};

#ifdef DOXYGEN
/** @struct CwChessBoardClass
 *  @brief The Class structure of %CwChessBoard
 */
struct CwChessBoardClass
#else
struct _CwChessBoardClass
#endif
{
  GtkDrawingAreaClass parent_class;

  /** @name Virtual functions of CwChessBoard */
  //@{

  /**
   * Calculate the border width of the chess board
   * as function of the side of a square, in pixels.
   *
   * @param sside	The size of of one side of a square in pixels.
   *
   * @returns the width of the border in pixels.
   *
   * Default value: #default_calc_board_border_width
   */
  gint (*calc_board_border_width)(gint sside);

  /**
   * An array with function pointers for drawing chess pieces.
   *
   * The functions draw one of the six chess pieces, both white and black.
   * The index of the array refers to the piece being drawn:
   * pawn = 0, rook = 1, bishop = 2, knight = 3, queen = 4, king = 5.
   *
   * The default values are given below.
   * They can be overridden by assigning other values to this array.
   *
   * \sa cw_chess_board_draw_pawn,
   *     cw_chess_board_draw_rook,
   *     cw_chess_board_draw_bishop,
   *     cw_chess_board_draw_knight,
   *     cw_chess_board_draw_queen,
   *     cw_chess_board_draw_king
   */
  void (*draw_piece[6])(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

  /**
   * Draw the HUD layer. This is a layer in between the background
   * (existing of the 64 squares with a single color) and the layer
   * with the pieces. It can be used to add some texture to the
   * background.
   * 
   * If the HUD layer is active, then this function is called
   * whenever the widget is resized.
   *
   * @param chess_board		A #CwChessBoard.
   * @param cr			A cairo drawing context.
   * @param sside		The size of one side of a square, in pixels.
   * @param hud			The HUD index number, see the detailed documentation on the \ref index "main page" for more info.
   * 
   * Default value: #cw_chess_board_default_draw_hud_layer
   *
   * @sa cw_chess_board_enable_hud_layer,
   *     cw_chess_board_disable_hud_layer,
   *     cw_chess_board_default_draw_hud_layer
   */
  void (*draw_hud_layer)(CwChessBoard* chess_board, cairo_t* cr, gint sside, guint hud);

  /**
   * Draw a single HUD square at \a col, \a row.
   * This function is called by cw_chess_board_default_draw_hud_layer
   * for each square. You can use it if you don't override draw_hud_layer.
   *
   * @param chess_board		A #CwChessBoard.
   * @param cr			A cairo drawing context.
   * @param col			The column of the square.
   * @param row			The row of the square.
   * @param sside		The size of one side of the square, in pixels.
   * @param hud			The HUD index number, see the detailed documentation of \ref index "main page" for more info.
   *
   * @returns TRUE if anything was drawn at all, FALSE if the HUD square is fully transparent.
   */
  gboolean (*draw_hud_square)(CwChessBoard* chess_board, cairo_t* cr, gint col, gint row, gint sside, guint hud);

  //@}

};

/**
 * Convert a (\a col, \a row) pair to the top-left coordinates of the corresponding square,
 * relative to the top-left of the widget.
 *
 * @param chess_board	A #CwChessBoard.
 * @param col		A column, in the range [0, 7].
 * @param row		A row, in the range [0, 7].
 * @param x		A pointer to where the x-coordinate of the result will be written to.
 * @param y		A pointer to where the y-coordinate of the result will be written to.
 */
CWCHESSBOARD_INLINE
void cw_chess_board_colrow2xy(CwChessBoard* chess_board, gint col, gint row, gint* x, gint* y)
#if CWCHESSBOARD_DEFINE_INLINE
{
  *x = chess_board->top_left_a1_x + col * chess_board->sside;
  *y = chess_board->top_left_a1_y - row * chess_board->sside;
}
#else
;
#endif

/**
 * Convert an x-coordinate to the column number that it matches.
 * If the x coordinate falls outside the board, then the returned value
 * will be outside the range [0, 7].
 *
 * @param chess_board	A #CwChessBoard.
 * @param x		An x coordinate, relative to the left-side of the widget.
 *
 * @returns a column number.
 */
CWCHESSBOARD_INLINE
gint cw_chess_board_x2col(CwChessBoard* chess_board, gdouble x)
#if CWCHESSBOARD_DEFINE_INLINE
{
  return (gint)floor((x - chess_board->top_left_a1_x) / chess_board->sside);
}
#else
;
#endif

/**
 * Convert a y-coordinate to the row number that it matches.
 * If the y coordinate falls outside the board, then the returned value
 * will be outside the range [0, 7].
 *
 * @param chess_board	A #CwChessBoard.
 * @param y		A y coordinate, relative to the top-side of the widget.
 *
 * @returns a row number.
 */
CWCHESSBOARD_INLINE
gint cw_chess_board_y2row(CwChessBoard* chess_board, gdouble y)
#if CWCHESSBOARD_DEFINE_INLINE
{
  return (gint)floor((chess_board->top_left_a1_y + chess_board->sside - 1 - y) / chess_board->sside);
}
#else
;
#endif

/** @name Chess Position */
//@{

/**
 * Change or remove the piece on the square (\a col, \a row),
 * by replacing the contents of the square with \a code.
 * This does not change any other attribute of the square,
 * like it's background color or marker.
 *
 * @param chess_board	A #CwChessBoard.
 * @param col		A column [0..7]
 * @param row		A row [0..7]
 * @param code		A #CwChessBoardCode.
 */
void cw_chess_board_set_square(CwChessBoard* chess_board, gint col, gint row, CwChessBoardCode code);

/**
 * Get the chess piece code for the square at (\a col, \a row).
 */
CwChessBoardCode cw_chess_board_get_square(CwChessBoard* chess_board, gint col, gint row);

//@} Chess Position

/** @name Border */
//@{

/**
 * Set the boolean which determines whether or not the chess board widget
 * draws a border around the chess board. Default: TRUE (draw border).
 *
 * @param chess_board	A #CwChessBoard.
 * @param draw		Boolean, determining if the border should be drawn.
 *
 * @sa CwChessBoardClass::calc_board_border_width
 */
void cw_chess_board_set_draw_border(CwChessBoard* chess_board, gboolean draw);

/**
 * Set the color of the border around the chess board.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new color of the border.
 *
 * @sa cw_chess_board_get_border_color, cw_chess_board_set_draw_border
 */
void cw_chess_board_set_border_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Get the boolean that determines whether or not the chess board widget draws a border around the chess board.
 *
 * @param chess_board	A #CwChessBoard.
 *
 * @returns <code>TRUE</code> if the border is being drawn.
 *
 * @sa cw_chess_board_set_draw_border
 */
gboolean cw_chess_board_get_draw_border(CwChessBoard* chess_board);

/**
 * This is the default value of CwChessBoardClass::calc_board_border_width.
 * This function uses the formula <code>(gint)MAX(1.0, round(1.0 + (sside - 12) / 25.0))</code>
 *
 * @param sside		The size of one side of a square, in pixels.
 *
 * @returns the border width in pixels.
 */
gint default_calc_board_border_width(gint sside);

//@} The Border

/** @name Colors */
//@{

/**
 * Set the background color of the dark squares (a1, c1 etc).
 * Default: light green.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new color of the dark squares.
 */
void cw_chess_board_set_dark_square_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Set the background color of the light squares (b1, d1 etc).
 * Default: yellow/white.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new color of the light squares.
 */
void cw_chess_board_set_light_square_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Set the fill color of the white chess pieces.
 * Default: white
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new fill color of the white pieces.
 */
void cw_chess_board_set_white_fill_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Set the line color of the white chess pieces.
 * Default: black
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new line color of the white pieces.
 */
void cw_chess_board_set_white_line_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Set the fill color of the black chess pieces.
 * Default: black
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new fill color of the black pieces.
 */
void cw_chess_board_set_black_fill_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Set the line color of the black chess pieces.
 * Default: white
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The new line color of the black pieces.
 */
void cw_chess_board_set_black_line_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Retrieve the current background color of the dark squares.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_dark_square_color
 */
void cw_chess_board_get_dark_square_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Retrieve the current background color of the light squares.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_light_square_color
 */
void cw_chess_board_get_light_square_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Retrieve the current color of the border around the chess board.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_border_color
 */
void cw_chess_board_get_border_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Retrieve the current fill color of the white chess pieces.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_white_fill_color
 */
void cw_chess_board_get_white_fill_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Retrieve the current line color of the white chess pieces.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_white_line_color
 */
void cw_chess_board_get_white_line_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Retrieve the current fill color of the black chess pieces.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_black_fill_color
 */
void cw_chess_board_get_black_fill_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Retrieve the current line color of the black chess pieces.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 *
 * @sa cw_chess_board_set_black_line_color
 */
void cw_chess_board_get_black_line_color(CwChessBoard* chess_board, GdkColor* color);

/**
 * Allocate a new CwChessBoardColorHandle.
 * Simultaneous, there can be at most 31 different colors.
 * It is the responsibility of the user to free the colors if they are no longer used.
 *
 * @param chess_board	A #CwChessBoard.
 * @param red		The red component of the color in the range [0...1].
 * @param green		The green component of the color in the range [0...1].
 * @param blue		The blue component of the color in the range [0...1].
 *
 * @returns a color handle that can be used with #cw_chess_board_set_background_color and #cw_chess_board_set_marker_color.
 *
 * @sa cw_chess_board_allocate_color_handle, cw_chess_board_free_color_handle
 */
CwChessBoardColorHandle cw_chess_board_allocate_color_handle_rgb(CwChessBoard* chess_board,
    gdouble red, gdouble green, gdouble blue);

/**
 * Allocate a new CwChessBoardColorHandle.
 * From more information, see #cw_chess_board_allocate_color_handle_rgb.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The color to allocate.
 *
 * @returns a color handle that can be used with #cw_chess_board_set_background_color and #cw_chess_board_set_marker_color.
 *
 * @sa cw_chess_board_free_color_handle
 */
CWCHESSBOARD_INLINE
CwChessBoardColorHandle cw_chess_board_allocate_color_handle(CwChessBoard* chess_board, GdkColor const* color)
#if CWCHESSBOARD_DEFINE_INLINE
{
  return cw_chess_board_allocate_color_handle_rgb(chess_board, color->red / 65535.0,
      color->green / 65535.0, color->blue / 65535.0);
}
#else
;
#endif

/**
 * Free up the color handle \a handle, so it can be reused.
 *
 * @param chess_board	A #CwChessBoard.
 * @param handle	A color handle as returned by cw_chess_board_allocate_color_handle_rgb or cw_chess_board_allocate_color_handle.
 *
 * @sa cw_chess_board_allocate_color_handle_rgb, cw_chess_board_allocate_color_handle
 */
void cw_chess_board_free_color_handle(CwChessBoard* chess_board, CwChessBoardColorHandle handle);

/**
 * Set the background color of the square at \a col, \a row.
 *
 * @param chess_board	A #CwChessBoard.
 * @param col		The column of the square.
 * @param row		The row of the square.
 * @param handle	A color handle as returned by #cw_chess_board_allocate_color_handle_rgb or
 * 			#cw_chess_board_allocate_color_handle. A handle with a value of 0 means the
 * 			default background color.
 */
void cw_chess_board_set_background_color(CwChessBoard* chess_board, gint col, gint row, CwChessBoardColorHandle handle);

/**
 * Convenience function.
 *
 * @param chess_board	A #CwChessBoard.
 * @param col		The column of the square.
 * @param row		The row of the square.
 *
 * @returns the handle that was passed to #cw_chess_board_set_background_color
 *          for this square, or 0 if the square is not associated with a color handle.
 */
CwChessBoardColorHandle cw_chess_board_get_background_color(CwChessBoard* chess_board, gint col, gint row);

/**
 * Set new background colors of any number of squares.
 *
 * @param chess_board	A #CwChessBoard.
 * @param handles	Array of 64 CwChessBoardColorHandles.
 *                      A handle with a value of 0 means the default background color.
 */
void cw_chess_board_set_background_colors(CwChessBoard* chess_board, CwChessBoardColorHandle const* handles);

/**
 * Fill the array \a handles with the current color handles.
 *
 * @param chess_board	A #CwChessBoard.
 * @param handles	The output array. Should be an array of 64 CwChessBoardColorHandles.
 */
void cw_chess_board_get_background_colors(CwChessBoard* chess_board, CwChessBoardColorHandle* handles);

//@} Colors

/** @name Floating Pieces */
//@{

/**
 * This function displays a chess piece with code \a code at widget coordinates (\a x, \a y).
 * Half the side of a square will be subtracted from the coordinates passed, and
 * the result truncated, in order to determine where to draw the top-left corner
 * of the piece. The result is that (\a x, \a y) is more or less the center of the piece.
 *
 * Setting \a pointer_device will cause gdk_window_get_pointer to be called
 * after the next redraw has finished. This is needed to receive the next motion
 * notify event with GDK_POINTER_MOTION_HINT_MASK being used.
 *
 * There may only be one floating piece related the pointer device at a time.
 * If there is already another floating piece related to the pointer device
 * then the value of \a pointer_device is ignored.
 *
 * @param chess_board	A #CwChessBoard.
 * @param code		The code of the chess piece to be drawn.
 * @param x		The center x-coordinate of the piece.
 * @param y		The center y-coordinate of the piece.
 * @param pointer_device Whether this piece is under the pointer device or not.
 *
 * @returns a handle that can be passed to each of the functions below.
 *
 * @sa cw_chess_board_get_floating_piece,
 *     cw_chess_board_remove_floating_piece,
 *     cw_chess_board_move_floating_piece
 */
gint cw_chess_board_add_floating_piece(CwChessBoard* chess_board,
    CwChessBoardCode code, gdouble x, gdouble y, gboolean pointer_device);

/**
 * Move a floating piece with handle \a handle to the new widget
 * coordinates at (\a x, \a y). \a handle must be a handle as
 * returned by #cw_chess_board_add_floating_piece.
 *
 * @param chess_board	A #CwChessBoard.
 * @param handle	The floating piece handle.
 * @param x		The new x coordinate.
 * @param y		The new y coordinate.
 *
 * @sa cw_chess_board_add_floating_piece
 */
void cw_chess_board_move_floating_piece(CwChessBoard* chess_board, gint handle, gdouble x, gdouble y);

/**
 * Delete the floating piece with handle \a handle.
 * \a handle must be a handle as returned by #cw_chess_board_add_floating_piece.
 *
 * @param chess_board	A #CwChessBoard.
 * @param handle	The floating piece handle.
 */
void cw_chess_board_remove_floating_piece(CwChessBoard* chess_board, gint handle);

/**
 * Get the CwChessBoardCode of the floating piece represented by \a handle.
 * \a handle must be a handle as returned by #cw_chess_board_add_floating_piece.
 *
 * @param chess_board	A #CwChessBoard.
 * @param handle	The floating piece handle.
 */
CwChessBoardCode cw_chess_board_get_floating_piece(CwChessBoard* chess_board, gint handle);

//@} Floating Pieces

/** @name Default Drawing Functions */
//@{

/**
 * This is the default function used by CwChessBoard to draw pawns.
 *
 * If \a white is set, a white pawn will be drawn. Otherwise a black pawn.
 *
 * <code>cw_chess_board_draw_pawn</code> is the default function called
 * via a call to CwChessBoardClass::draw_piece.
 * It is called every time the chess board is resized.
 *
 * The function uses vector graphics (by doing direct calls to cairo),
 * and is therefore capable of drawing the piece in any arbitrary size
 * and uses anti-aliasing.
 *
 * @image html pawn.png
 *
 * @param chess_board	A #CwChessBoard.
 * @param cr		The cairo drawing context.
 * @param x		The (widget) x-coordinate of the center of the piece.
 * @param y		The (widget) y-coordinate of the center of the piece.
 * @param sside		The assumed side of a square, in pixels.
 * 			<code>chess_board->sside</code> is ignored, so that
 * 			this function can be used to draw pieces elsewhere with a different
 * 			size than what is used on the chess board.
 * @param white		A boolean that determines if a black or white piece is drawn.
 */
void cw_chess_board_draw_pawn(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

/**
 * This is the default function used by CwChessBoard to draw a rook.
 *
 * @image html rook.png
 *
 * See #cw_chess_board_draw_pawn for more details.
 */
void cw_chess_board_draw_rook(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

/**
 * This is the default function used by CwChessBoard to draw a knight.
 *
 * @image html knight.png
 *
 * See #cw_chess_board_draw_pawn for more details.
 */
void cw_chess_board_draw_knight(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

/**
 * This is the default function used by CwChessBoard to draw a bishop.
 *
 * @image html bishop.png
 *
 * See #cw_chess_board_draw_pawn for more details.
 */
void cw_chess_board_draw_bishop(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

/**
 * This is the default function used by CwChessBoard to draw a queen.
 *
 * @image html queen.png
 *
 * See #cw_chess_board_draw_pawn for more details.
 */
void cw_chess_board_draw_queen(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

/**
 * This is the default function used by CwChessBoard to draw a king.
 *
 * @image html king.png
 *
 * See #cw_chess_board_draw_pawn for more details.
 */
void cw_chess_board_draw_king(CwChessBoard* chess_board, cairo_t* cr, gdouble x, gdouble y, gdouble sside, gboolean white);

/**
 * The default CwChessBoardClass::draw_hud_layer function.
 * You can restore the default behaviour with:
 * \code
 * CW_CHESS_BOARD_GET_CLASS(chess_board)->draw_hud_layer = cw_chess_board_default_draw_hud_layer;
 * \endcode
 *
 * @param chess_board	A #CwChessBoard.
 * @param cr		The cairo drawing context.
 * @param sside		The side of one square in pixels.
 * @param hud		The HUD layer (0 or 1).
 *
 * This function calls CwChessBoardClass::draw_hud_square for every square.
 */
void cw_chess_board_default_draw_hud_layer(CwChessBoard* chess_board, cairo_t* cr, gint sside, guint hud);

/**
 * The default CwChessBoardClass::draw_hud_square function.
 * This function is only used by cw_chess_board_default_draw_hud_layer.
 * You can restore the default behaviour of cw_chess_board_default_draw_hud_layer with:
 * \code
 * CW_CHESS_BOARD_GET_CLASS(chess_board)->draw_hud_square = cw_chess_board_default_draw_hud_square;
 * \endcode
 *
 * This function hatches the dark squares with fine, diagonal lines.
 *
 * @param chess_board	A #CwChessBoard.
 * @param cr		The cairo drawing context.
 * @param col		The column of the square.
 * @param row		The row of the square.
 * @param sside		The side of one square in pixels.
 * @param hud		The HUD layer (0 or 1).
 *
 * @returns <code>TRUE</code> if anything was drawn. <code>FALSE</code> otherwise.
 */
gboolean cw_chess_board_default_draw_hud_square(CwChessBoard* chess_board,
    cairo_t* cr, gint col, gint row, gint sside, guint hud);

//@} Default Virtual Functions

/** @name HUD Layers */
//@{

/**
 * Active a HUD layer.
 * HUD 0 lays between the background and the pieces.
 * HUD 1 lays above the pieces.
 * A custom HUD layer can be created by setting CwChessBoardClass::draw_hud_layer.
 *
 * @param chess_board	A #CwChessBoard.
 * @param hud		The HUD layer (0 or 1).
 *
 * @sa CwChessBoardClass::draw_hud_layer, cw_chess_board_disable_hud_layer
 */
void cw_chess_board_enable_hud_layer(CwChessBoard* chess_board, guint hud);

/**
 * Disable the HUD layer again. Used resources are returned to the system.
 *
 * @param chess_board	A #CwChessBoard.
 * @param hud		The HUD layer (0 or 1).
 *
 * @sa cw_chess_board_enable_hud_layer
 */
void cw_chess_board_disable_hud_layer(CwChessBoard* chess_board, guint hud);

//@} // HUD Layers

/** @name Markers */
//@{

/**
 * Add or remove a marker to the square at \a col, \a row.
 *
 * @param chess_board	A #CwChessBoard.
 * @param col		The column of the square.
 * @param row		The row of the square.
 * @param mahandle	A color handle as returned by #cw_chess_board_allocate_color_handle_rgb or
 * 			#cw_chess_board_allocate_color_handle. A handle with a value of 0 means the
 * 			default background color.
 */
void cw_chess_board_set_marker_color(CwChessBoard* chess_board,
    gint col, gint row, CwChessBoardColorHandle mahandle);

/**
 * Convenience function.
 *
 * @param chess_board	A #CwChessBoard.
 * @param col		The column of the square.
 * @param row		The row of the square.
 *
 * @returns the handle that was passed to #cw_chess_board_set_marker_color
 *          for this square, or 0 if the square doesn't have a marker.
 */
CwChessBoardColorHandle cw_chess_board_get_marker_color(CwChessBoard* chess_board, gint col, gint row);

/**
 * Set the marker thickness. This is a value between 0 and 0.5.
 *
 * @param chess_board	A #CwChessBoard.
 * @param thickness	The thickness of the marker as fraction of sside. Range [0...0.5]
 *
 * @sa cw_chess_board_get_marker_thickness
 */
void cw_chess_board_set_marker_thickness(CwChessBoard* chess_board, gdouble thickness);

/**
 * Get the current marker thickness as fraction of sside.
 *
 * @param chess_board	A #CwChessBoard.
 *
 * @sa cw_chess_board_set_marker_thickness
 */
gdouble cw_chess_board_get_marker_thickness(CwChessBoard* chess_board);

/**
 * Choose whether markers should be drawn below or above HUD layer 0.
 *
 * Markers can be drawn directly below or directly above HUD layer 0.
 *
 * @param chess_board	A #CwChessBoard.
 * @param below		TRUE when markers should be drawn below HUD layer 0.
 */
void cw_chess_board_set_marker_level(CwChessBoard* chess_board, gboolean below);

//@} Markers

/** @name Cursor */
//@{

/**
 * Show the cursor.
 *
 * This high-lights the square under the mouse by drawing a
 * square with a configurable thickness and color.
 *
 * @param chess_board	A #CwChessBoard.
 *
 * @sa cw_chess_board_set_cursor_thickness, cw_chess_board_get_cursor_thickness
 */
void cw_chess_board_show_cursor(CwChessBoard* chess_board);

/**
 * Hide the cursor.
 *
 * @param chess_board	A #CwChessBoard.
 *
 * @sa cw_chess_board_show_cursor
 */
void cw_chess_board_hide_cursor(CwChessBoard* chess_board);

/**
 * Set the cursor thickness. This is a value between 0 and 0.5.
 *
 * @param chess_board	A #CwChessBoard.
 * @param thickness	The thickness of the cursor as fraction of sside. Range [0...0.5]
 *
 * @sa cw_chess_board_get_cursor_thickness
 */
void cw_chess_board_set_cursor_thickness(CwChessBoard* chess_board, gdouble thickness);

/**
 * Get the current cursor thickness as fraction of sside.
 *
 * @param chess_board	A #CwChessBoard.
 *
 * @sa cw_chess_board_set_cursor_thickness
 */
gdouble cw_chess_board_get_cursor_thickness(CwChessBoard* chess_board);

/**
 * Set the color of the cursor.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		The color to be used for the cursor.
 */
void cw_chess_board_set_cursor_color(CwChessBoard* chess_board, GdkColor const* color);

/**
 * Get the current cursor color.
 *
 * @param chess_board	A #CwChessBoard.
 * @param color		Pointer to the output variable.
 */
void cw_chess_board_get_cursor_color(CwChessBoard* chess_board, GdkColor* color);

//@} Cursor

/** @name Arrows */
//@{

/**
 * Draw an arrow on the board.
 *
 * @param chess_board	A #CwChessBoard.
 * @param begin_col	The column of the starting square.
 * @param begin_row	The row of the starting square.
 * @param end_col	The column of the ending square.
 * @param end_row	The row of the ending square.
 * @param color		The color to draw the arrow in.
 *
 * @returns a handle that can be used to remove the arrow again.
 *
 * @sa cw_chess_board_remove_arrow
 */
gpointer cw_chess_board_add_arrow(CwChessBoard* chess_board,
    gint begin_col, gint begin_row, gint end_col, gint end_row, GdkColor* color);

/**
 * Remove a previously added arrow.
 *
 * @param chess_board	A #CwChessBoard.
 * @param ptr		The arrow handle as returned by #cw_chess_board_add_arrow.
 */
void cw_chess_board_remove_arrow(CwChessBoard* chess_board, gpointer ptr);

//@} Arrows

G_END_DECLS

#endif // CWCHESSBOARD_H
