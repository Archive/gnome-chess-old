/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* board-window.h
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _BOARD_WINDOW_H_
#define _BOARD_WINDOW_H_

#include <gnome.h>
#include "game-view.h"

G_BEGIN_DECLS

#define BOARD_TYPE_WINDOW			(board_window_get_type ())
#define BOARD_WINDOW(obj)			(GTK_CHECK_CAST ((obj), BOARD_TYPE_WINDOW, BoardWindow))
#define BOARD_WINDOW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), BOARD_TYPE_WINDOW, BoardWindowClass))
#define IS_BOARD_WINDOW(obj)			(GTK_CHECK_TYPE ((obj), BOARD_TYPE_WINDOW))
#define IS_BOARD_WINDOW_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), BOARD_TYPE_WINDOW))


typedef struct _BoardWindow        BoardWindow;
typedef struct _BoardWindowPrivate BoardWindowPrivate;
typedef struct _BoardWindowClass   BoardWindowClass;

struct _BoardWindow {
	GnomeApp parent;

	BoardWindowPrivate *priv;
};

struct _BoardWindowClass {
	GnomeAppClass parent_class;
};



GtkType    board_window_get_type            (void);
GtkWidget *board_window_new                 (void);

void       board_window_add_view            (BoardWindow *window,
					     GameView    *view);
void       board_window_remove_view         (BoardWindow *window,
					     GameView    *view);
void       board_window_remove_current_view (BoardWindow *window);
void       board_window_remove_all_views    (BoardWindow *window);
GameView * board_window_get_current_view    (BoardWindow *window);

void       board_window_add_info            (BoardWindow *window,
					     const char  *text,
					     GtkWidget   *child);
void       board_window_remove_info         (BoardWindow *window,
					     GtkWidget   *child);

G_END_DECLS

#endif

