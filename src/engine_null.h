/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-null.h
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _ENGINE_NULL_H_
#define _ENGINE_NULL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "board-window.h"
#include "engine.h"

G_BEGIN_DECLS

#define ENGINE_TYPE_NULL	    (engine_null_get_type ())
#define ENGINE_NULL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ENGINE_TYPE_NULL, EngineNull))
#define ENGINE_NULL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), ENGINE_TYPE_NULL, EngineNullClass))
#define ENGINE_IS_NULL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ENGINE_TYPE_NULL))
#define ENGINE_IS_NULL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ENGINE_TYPE_NULL))


typedef struct _EngineNull        EngineNull;
typedef struct _EngineNullPrivate EngineNullPrivate;
typedef struct _EngineNullClass   EngineNullClass;

struct _EngineNull {
	Engine parent;

	EngineNullPrivate *priv;
};

struct _EngineNullClass {
	EngineClass parent_class;
};


GType    engine_null_get_type (void);
GObject *engine_null_new      (void);


G_BEGIN_DECLS

#endif
