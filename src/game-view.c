/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* game-view.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <gnome.h>
#include "debug.h"
#include "makros.h"
#include "prefs.h"
#include "sound.h"
#include "board.h"
#include "movlist.h"
#include "clock.h"
#include "engine.h"
#include "game-view.h"

enum {
	TITLE,
	LAST_SIGNAL
};
static gint signals [LAST_SIGNAL] = { 0 };

static void class_init (GameViewClass *klass);
static void init (GameView *view);
static void destroy (GtkObject *object);

static void light_color_changed_cb (GConfClient* client, guint id, GConfEntry *entry, gpointer data);
static void dark_color_changed_cb (GConfClient* client, guint id, GConfEntry *entry, gpointer data);

static void gv_engine_game_cb (GtkObject *obj, gint ply, Position *pos, gpointer data);
static void gv_engine_move_cb (GtkObject *obj, Square from, Square to, gpointer data);
static void gv_engine_unmove_cb (GtkObject *obj, gint ply, gpointer data);
static void gv_engine_error_cb (GtkObject *obj, EngineError erro, gpointer data);
static void board_move_cb (GtkWidget *widget, Square from, Square to, gpointer data);
static void board_flip_cb (GtkWidget *widget, gboolean flip, gpointer data);
static void move_list_move_cb (GtkWidget *widget, gint plynum, gpointer data);

static GtkHBoxClass *parent_class = NULL;

struct _GameViewPrivate {
	GtkWidget *vbox;
	GtkWidget *players;
	GtkWidget *description;
	GtkWidget *status_label;

	GameViewStatus status;
	
	GameViewFillMenu fill_menu;
	gpointer data;

	guint light_id;
	guint dark_id;
};


GtkType
game_view_get_type (void)
{
  static GtkType type = 0;

  if (type == 0)
    {
      static const GtkTypeInfo info =
      {
        "GameView",
        sizeof (GameView),
        sizeof (GameViewClass),
        (GtkClassInitFunc) class_init,
        (GtkObjectInitFunc) init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      type = gtk_type_unique (gtk_hbox_get_type (), &info);
    }

  return type;
}

static void
class_init (GameViewClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	signals [TITLE] =
		gtk_signal_new ("title",
				GTK_RUN_FIRST,
				GTK_CLASS_TYPE (object_class),
				GTK_SIGNAL_OFFSET (GameViewClass, title),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE,
				1, GTK_TYPE_POINTER);
	

	object_class->destroy = destroy;
}


static void
init (GameView *view)
{
	GameViewPrivate *priv;
	GtkWidget *vbox;
	guint8 r, g, b;

	priv = g_new0 (GameViewPrivate, 1);

	view->priv = priv;

	/* Left side */
  	priv->vbox = gtk_vbox_new (FALSE, 4);
	gtk_box_pack_start (GTK_BOX (view), priv->vbox, TRUE, TRUE, 0);
	gtk_widget_show (priv->vbox);

	/* The top clock */
	view->black_clock = clock_new ();
	gtk_box_pack_start (GTK_BOX (priv->vbox), view->black_clock, FALSE, FALSE, 0);
	gtk_widget_show (view->black_clock);
	clock_set_name (CLOCK (view->black_clock), _("Black"));

	/* The Board */
	view->board = board_new ();
	prefs_get_light_color (&r, &g, &b);
	board_set_light_color (BOARD (view->board), GNOME_CANVAS_COLOR (r, g, b));

	prefs_get_dark_color (&r, &g, &b);
	board_set_dark_color (BOARD (view->board), GNOME_CANVAS_COLOR (r, g, b));

	gtk_box_pack_start (GTK_BOX (priv->vbox), view->board, TRUE, TRUE, 0);
	gtk_widget_show (GTK_WIDGET (view->board));  

	priv->light_id = prefs_add_notification_light_color (light_color_changed_cb, view);
	priv->dark_id = prefs_add_notification_dark_color (dark_color_changed_cb, view);

	g_signal_connect (G_OBJECT (view->board), "flip",
			  G_CALLBACK (board_flip_cb), view);
	g_signal_connect (G_OBJECT (view->board), "move",
			  G_CALLBACK (board_move_cb), view);

	/* The bottom clock */
	view->white_clock = clock_new ();
	gtk_box_pack_start (GTK_BOX (priv->vbox), view->white_clock, FALSE, FALSE, 0);
	gtk_widget_show (view->white_clock);
	clock_set_name (CLOCK (view->white_clock), _("White"));

	/* Right side */
	vbox = gtk_vbox_new (FALSE, 4);
	gtk_box_pack_start (GTK_BOX (view), vbox, FALSE, TRUE, 0);
	gtk_widget_show (vbox);
	
	/* Game Information */
	priv->players = gtk_label_new (_("White v. Black"));
	gtk_box_pack_start (GTK_BOX (vbox), priv->players, FALSE, FALSE, 0);
	gtk_widget_show (priv->players);
	
	priv->description = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (vbox), priv->description, FALSE, FALSE, 0);
	gtk_widget_show (priv->description);

	priv->status_label = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (vbox), priv->status_label, FALSE, FALSE, 0);
	gtk_widget_show (priv->status_label);

	priv->status = GAME_VIEW_STATUS_NONE;
	
	/* Move List */
	view->movelist = move_list_new ();
	gtk_box_pack_start (GTK_BOX (vbox), view->movelist, TRUE, TRUE, 0);
	gtk_widget_show (view->movelist);

	g_signal_connect (G_OBJECT (view->movelist), "move",
			  G_CALLBACK (move_list_move_cb), view);
}

static void 
destroy (GtkObject *object)
{
	GameView *view;
	GameViewPrivate *priv;

	view = GAME_VIEW (object);
	priv = view->priv;

	if (priv != NULL) {		
		prefs_rm_notification (priv->light_id);
		prefs_rm_notification (priv->dark_id);
		
		g_free (priv);
		view->priv = NULL;
	}

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}



GtkWidget *
game_view_new (GameViewFillMenu fill_menu, gpointer data)
{
	GtkWidget *view;
	
	view = gtk_type_new (TYPE_GAME_VIEW);
	
	GAME_VIEW (view)->priv->fill_menu = fill_menu;
	GAME_VIEW (view)->priv->data = data;

	return view;
}

#if 0
GtkWidget *
game_view_new_with_engine (Engine *engine)
{
	GameView *view;
	GameViewPrivate *priv;
	
	view = gtk_type_new (TYPE_GAME_VIEW);
	priv = view->priv;
	
	/* Connect to our callbacks */
	gtk_signal_connect (GTK_OBJECT (priv->engine), "unmove",
			    (GtkSignalFunc) gv_engine_unmove_cb,
			    view);
	gtk_signal_connect (GTK_OBJECT (priv->engine), "error",
			    (GtkSignalFunc) gv_engine_error_cb,
			    view);

	return GTK_WIDGET (view);
}
#endif

void 
game_view_fill_menu (GameView *view, GtkMenuShell *shell, gint pos)
{
	GameViewPrivate *priv;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	priv = view->priv;
	
	if (priv->fill_menu)
		priv->fill_menu (shell, pos, priv->data);
}

static void
update_players (GameView *view) 
{
	GameViewPrivate *priv;
	char *text;
	
	priv = view->priv;

	text = g_strdup_printf (_("%s v. %s"), clock_get_name (CLOCK (view->white_clock)), 
				clock_get_name (CLOCK (view->black_clock)));
	gtk_label_set_text (GTK_LABEL (priv->players), text);
	gtk_signal_emit (GTK_OBJECT (view), signals[TITLE], text);
	g_free (text);
}

const char *
game_view_get_white (GameView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (IS_GAME_VIEW (view), NULL);

	return clock_get_name (CLOCK (view->white_clock));
}

void game_view_set_white (GameView *view, const char *name)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	clock_set_name (CLOCK (view->white_clock), name);
	update_players (view);
}

const char *
game_view_get_black (GameView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (IS_GAME_VIEW (view), NULL);

	return clock_get_name (CLOCK (view->black_clock));
}

void 
game_view_set_black (GameView *view, const char *name)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	clock_set_name (CLOCK (view->black_clock), name);
	update_players (view);
}

const char *
game_view_get_description (GameView *view)
{
	GameViewPrivate *priv;
	
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (IS_GAME_VIEW (view), NULL);

	priv = view->priv;
	
	return gtk_label_get_text (GTK_LABEL (priv->description));
}

void 
game_view_set_description (GameView *view, const char *description)
{
	GameViewPrivate *priv;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	priv = view->priv;
	
	gtk_label_set_text (GTK_LABEL (priv->description), description);
}

gint 
game_view_get_to_move (GameView *view)
{
	g_return_val_if_fail (view != NULL, NONE);
	g_return_val_if_fail (IS_GAME_VIEW (view), NONE);

	return NONE;
}

void 
game_view_set_to_move (GameView *view, gint to_move)
{
	GameViewPrivate *priv;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	priv = view->priv;
	
	switch (to_move) {
	case WHITE:
		if (priv->status == GAME_VIEW_STATUS_PROGRESS) {
			clock_start (CLOCK (view->white_clock));
			clock_stop (CLOCK (view->black_clock));
		}		
		clock_set_to_move (CLOCK (view->white_clock), TRUE);
		clock_set_to_move (CLOCK (view->black_clock), FALSE);
		break;
	case BLACK:
		if (priv->status == GAME_VIEW_STATUS_PROGRESS) {
			clock_start (CLOCK (view->black_clock));
			clock_stop (CLOCK (view->white_clock));
		}		
		clock_set_to_move (CLOCK (view->black_clock), TRUE);
		clock_set_to_move (CLOCK (view->white_clock), FALSE);
		break;	
	default:
		clock_stop (CLOCK (view->black_clock));
		clock_stop (CLOCK (view->white_clock));
		clock_set_to_move (CLOCK (view->black_clock), FALSE);
		clock_set_to_move (CLOCK (view->white_clock), FALSE);
		break;	
	}	
}

GameViewStatus
game_view_get_status (GameView *view)
{
	GameViewPrivate *priv;
	
	g_return_val_if_fail (view != NULL, GAME_VIEW_STATUS_NONE);
	g_return_val_if_fail (IS_GAME_VIEW (view), GAME_VIEW_STATUS_NONE);

	priv = view->priv;

	return priv->status;
}

void 
game_view_set_status (GameView *view, GameViewStatus status)
{
	GameViewPrivate *priv;
	char *text;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (IS_GAME_VIEW (view));

	priv = view->priv;
	
	priv->status = status;
	
	switch (status) {
	case GAME_VIEW_STATUS_PROGRESS:
		text = _("Playing");
		break;
	case GAME_VIEW_STATUS_VIEWING:
		text = _("Viewing");
		break;
	case GAME_VIEW_STATUS_PAUSED:
		text = _("Paused");
		break;
	case GAME_VIEW_STATUS_ABORTED:
		text = _("Aborted");
		break;
	case GAME_VIEW_STATUS_WHITE:
		text = _("1-0");
		break;
	case GAME_VIEW_STATUS_BLACK:
		text = _("0-1");
		break;
	case GAME_VIEW_STATUS_DRAW:
		text = _("1/2-1/2");
		break;
	default:
		text = "";
	}
	gtk_label_set_text (GTK_LABEL (priv->status_label), text);

	if (priv->status != GAME_VIEW_STATUS_PROGRESS) {
		clock_stop (CLOCK (view->black_clock));
		clock_stop (CLOCK (view->white_clock));
	}
}

static void
light_color_changed_cb (GConfClient* client, guint id, GConfEntry *entry, gpointer data)
{
	GameView *view = data;
	guint8 r, g, b;
	
	prefs_get_light_color (&r, &g, &b);	
	board_set_light_color (BOARD (view->board), GNOME_CANVAS_COLOR (r, g, b));
}

static void
dark_color_changed_cb (GConfClient* client, guint id, GConfEntry *entry, gpointer data)
{
	GameView *view = data;
	guint8 r, g, b;
	
	prefs_get_dark_color (&r, &g, &b);	
	board_set_dark_color (BOARD (view->board), GNOME_CANVAS_COLOR (r, g, b));
}

static void
gv_engine_game_cb (GtkObject *obj, gint ply, Position *pos, gpointer data)
{
	GameView *view;
	GameViewPrivate *priv;

	view = GAME_VIEW (data);
	priv = view->priv;

	board_set_position (BOARD (view->board), pos);

	move_list_clear_initial (MOVE_LIST (view->movelist), ply, pos);
}

static void
gv_engine_move_cb (GtkObject *obj, Square from, Square to, gpointer data)
{
	GameView *view;
	GameViewPrivate *priv;

	view = GAME_VIEW (data);
	priv = view->priv;

	board_move (BOARD (view->board), from, to);

	move_list_add (MOVE_LIST (view->movelist), from, to);
	sound_enginemove ();
}

static void
gv_engine_unmove_cb (GtkObject *obj, gint ply, gpointer data)
{
	GameView *view;
	GameViewPrivate *priv;
	gint curr;	

	view = GAME_VIEW (data);
	priv = view->priv;

	curr = move_list_currply (MOVE_LIST (view->movelist));
	if (curr < ply)
		return;
	
	move_list_clear_from (MOVE_LIST (view->movelist), curr - ply);
}

static void
gv_engine_error_cb (GtkObject *obj, EngineError error, gpointer data)
{
	GameView *view;
	GameViewPrivate *priv;

	view = GAME_VIEW (data);
	priv = view->priv;

	g_print ("Engine Error");
}

static void 
board_move_cb (GtkWidget *widget, Square from, Square to, gpointer data) 
{
	GameView *view;
	GameViewPrivate *priv;

	view = GAME_VIEW (data);
	priv = view->priv;

	move_list_add (MOVE_LIST (view->movelist), from, to);
	sound_enginemove ();
}

static void 
board_flip_cb (GtkWidget *widget, gboolean flip, gpointer data) 
{
	GameView *view;
	GameViewPrivate *priv;

	view = GAME_VIEW (data);
	priv = view->priv;

	gtk_box_reorder_child (GTK_BOX (priv->vbox), view->black_clock, flip ? 2 : 0);
	gtk_box_reorder_child (GTK_BOX (priv->vbox), view->white_clock, flip ? 0 : 2);
}

static void 
move_list_move_cb (GtkWidget *widget, gint plynum, gpointer data) 
{
	GameView *view;
	GameViewPrivate *priv;
	Position *pos;

	view = GAME_VIEW (data);
	priv = view->priv;

	debug_print (DEBUG_NORMAL,"MoveList Move To Ply: %d\n", plynum);

	pos = move_list_get_position (MOVE_LIST (view->movelist), plynum);
	board_set_position (BOARD (view->board), pos);
}
