/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* board_info.c
 *
 * Copyright (C) 2001  JP Rosevear
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>
#include "makros.h"
#include "clock.h"

struct _ClockPrivate 
{
	GtkWidget *clock_info;
	GtkWidget *left_arrow;
	GtkWidget *right_arrow;

	char *name;
	
	gint seconds;
	gint handlerid;
	
	gboolean tomove;
};

static gboolean timeout_cb (gpointer data);

enum {
	EXPIRED,
	LAST_SIGNAL
};
static gint signals [LAST_SIGNAL] = { 0 };

static GtkHBoxClass *parent_class = NULL;

static void class_init (ClockClass *klass);
static void init (Clock *clock);

static void
set_clock_info (Clock *clock)
{
	ClockPrivate *priv;
	char *text;
	
	priv = clock->priv;
	
	text = g_strdup_printf ("<span size=\"xx-large\">%s %c%d:%.2d</span>",
				priv->name != NULL ? priv->name : _("Unknown"),
				priv->seconds < 0 ? '-' : ' ',
				abs (priv->seconds / 60), abs (priv->seconds % 60));
	gtk_label_set_markup (GTK_LABEL (priv->clock_info), text);
	g_free (text);
}

GType
clock_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ClockClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof (Clock),
			0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (GTK_TYPE_HBOX, "Clock", &info, 0);
	}

	return type;
}

static void
class_init (ClockClass *klass)
{
	GtkObjectClass *object_class;
	
	object_class = GTK_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	signals [EXPIRED] =     
		g_signal_new ("expired",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (ClockClass, expired),
			      NULL, NULL,
			      gtk_marshal_NONE__NONE,
			      G_TYPE_NONE, 0);
}

static void
init (Clock *clock)
{
	ClockPrivate *priv;

	priv = g_new0 (ClockPrivate, 1);

	clock->priv = priv;

	/* Set defaults */
	gtk_box_set_homogeneous (GTK_BOX (clock), FALSE);
	gtk_box_set_spacing (GTK_BOX (clock), 2);

	/* Player */
	priv->clock_info = gtk_label_new (NULL);
	gtk_label_set_use_markup (GTK_LABEL (priv->clock_info), TRUE);
	gtk_widget_show (priv->clock_info);

	/* The arrows */
	priv->left_arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_ETCHED_OUT);
	priv->right_arrow = gtk_arrow_new (GTK_ARROW_LEFT, GTK_SHADOW_ETCHED_OUT);
	
	/* Pack the widgets */
	gtk_box_pack_start (GTK_BOX (clock), priv->left_arrow, 
			    TRUE, TRUE, 0);	
	gtk_box_pack_start (GTK_BOX (clock), priv->clock_info, 
			    TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (clock), priv->right_arrow, 
			    TRUE, TRUE, 0);	

	priv->name = g_strdup (_("White"));
	priv->seconds = 0;
	priv->handlerid = -1;
	priv->tomove = FALSE;

	set_clock_info (clock);
}



GtkWidget *
clock_new (void)
{
	Clock *clock;
	
	clock = g_object_new (TYPE_CLOCK, NULL);

	return GTK_WIDGET (clock);
}

gboolean
clock_get_to_move (Clock *clock)
{
	ClockPrivate *priv;
	
	priv = clock->priv;

	return priv->tomove;
}

void
clock_set_to_move (Clock *clock, gboolean tomove)
{
	ClockPrivate *priv;
	
	priv = clock->priv;

	if (priv->tomove == tomove)
		return;
	
	tomove ? gtk_widget_show (priv->left_arrow) : gtk_widget_hide (priv->left_arrow);
	tomove ? gtk_widget_show (priv->right_arrow) : gtk_widget_hide (priv->right_arrow);
	
	priv->tomove = tomove;
}

const gchar *
clock_get_name (Clock *clock) 
{
	ClockPrivate *priv;

	priv = clock->priv;

	return priv->name;
}

void
clock_set_name (Clock *clock, const gchar *name)
{
	ClockPrivate *priv;

	priv = clock->priv;

	if (priv->name)
		g_free (priv->name);
	
	priv->name = g_strdup (name);
	
	set_clock_info (clock);
}

gint
clock_get_time (Clock *clock)
{
	ClockPrivate *priv;
	
	priv = clock->priv;

	return priv->seconds;
}

void
clock_set_time (Clock *clock, gint seconds)
{
	ClockPrivate *priv;
	
	priv = clock->priv;

	priv->seconds = seconds;
	
	set_clock_info (clock);
}

void
clock_start (Clock *clock)
{
	ClockPrivate *priv;
	
	priv = clock->priv;

	if (priv->handlerid != -1)
		return;

	priv->handlerid = gtk_timeout_add (1000, timeout_cb, clock);
}

void
clock_stop (Clock *clock)
{
	ClockPrivate *priv;
	
	priv = clock->priv;

	if (priv->handlerid == -1)
		return;

	gtk_timeout_remove (priv->handlerid);
	priv->handlerid = -1;
}

static gboolean
timeout_cb (gpointer data) 
{
	Clock *clock = CLOCK (data);
	ClockPrivate *priv;
	
	priv = clock->priv;

	priv->seconds--;
	clock_set_time (clock, priv->seconds);

	if (priv->seconds <= 0)
		gtk_signal_emit (GTK_OBJECT (clock), signals [EXPIRED]);

	return TRUE;
}
