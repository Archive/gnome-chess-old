/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* server-term.h
 *
 * Copyright (C) 2001  JP Rosevear
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _SERVER_TERM_H_
#define _SERVER_TERM_H_

#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define SERVER_TYPE_TERM			(server_term_get_type ())
#define SERVER_TERM(obj)			(GTK_CHECK_CAST ((obj), SERVER_TYPE_TERM, ServerTerm))
#define SERVER_TERM_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), SERVER_TYPE_TERM, ServerTermClass))
#define IS_SERVER_TERM(obj)			(GTK_CHECK_TYPE ((obj), SERVER_TYPE_TERM))
#define IS_SERVER_TERM_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), SERVER_TYPE_TERM))


typedef struct _ServerTerm        ServerTerm;
typedef struct _ServerTermPrivate ServerTermPrivate;
typedef struct _ServerTermClass   ServerTermClass;

struct _ServerTerm {
	GtkVBox parent;

	ServerTermPrivate *priv;
};

struct _ServerTermClass {
	GtkVBoxClass parent_class;

	void (* command) (ServerTerm *term, gchar *command);
};


GtkType    server_term_get_type (void);
GtkWidget *server_term_new      (void);

void server_term_get_size    (ServerTerm *term,
			      guint *width,
			      guint *height);
void server_term_hide_prompt (ServerTerm *term);
void server_term_show_prompt (ServerTerm *term);
void server_term_set_prompt  (ServerTerm *term,
			      gchar      *prompt);

void server_term_print       (ServerTerm *term,
			      gchar      *text);
void server_term_clear       (ServerTerm *term);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SERVER_TERM_H_ */






