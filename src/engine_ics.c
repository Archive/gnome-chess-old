/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-ics.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <config.h>
#include <netdb.h>
#include <gnome.h>
#include <glade/glade.h>
#include "debug.h"
#include "makros.h"
#include "prefs.h"
#include "child.h"
#include "notation.h"
#include "board.h"
#include "clock.h"
#include "dialogs.h"
#include "server-term.h"
#include "engine_null.h"
#include "engine_ics.h"

/* Structs for game info */
typedef struct {
	Engine *engine;
	int game;
	int rel;	
	int ply;
	gshort color;
	gboolean flag;
} ICSEngineInfo;

struct _EngineIcsPrivate 
{
	/* Server terminal */
	ServerTerm *term;
	int last_column_size;
	int last_row_size;
	
	/* User info & status */
	char *user;
	char *pass;
	gboolean loggedin;

	/* Stuff for reading from server */
	GIOChannel *read_chan;
	GIOChannel *write_chan;
	
	pid_t childpid;

	gint read_cb;
	gint err_cb;

	/* List of board windows used by engine */
	GList *list;
};

/* Defines for the board string tokens */
#define BOARD_START 1
#define BOARD_END   8
#define TO_MOVE     9
#define DOUBLE_PUSH 10
#define WHITE_OO    11
#define WHITE_OOO   12
#define BLACK_OO    13
#define BLACK_OOO   14
#define IRREV       15
#define GAME        16
#define WHITE_NAME  17
#define BLACK_NAME  18
#define RELATION    19
#define START_TIME  20
#define START_INC   21
#define WHITE_STR   22
#define BLACK_STR   23
#define WHITE_TIME  24
#define BLACK_TIME  25
#define MOVE_NUM    26
#define VERBOSE     27
#define MOVE_TIME   28
#define NOTATION    29
#define FLIP        30

static GIOChannel *chan = NULL;
static gboolean have_read = FALSE;
G_LOCK_DEFINE (ICS_INPUT);

static GObject *parent_class = NULL;

static void class_init (EngineIcsClass *klass);
static void init (EngineIcs *ics);
static void dispose (GObject *obj);
static void finalize (GObject *obj);

static GameView *ei_add_view (EngineIcs *engine);
static void ei_view_cb (GtkWidget *widget, gpointer data);
static gboolean ei_input_cb (GIOChannel *source, GIOCondition condition, gpointer data);
static gboolean ei_err_cb (GIOChannel *source, GIOCondition condition, gpointer data);
static void ei_resize_cb (GtkWidget *widget, GtkRequisition *requisition, gpointer data);
static void ei_command_cb (GtkWidget *widget, gchar *command, gpointer data);

/* Lex declarations */
extern int yylex (EngineIcs *engine);
void yyerror (char *s);

GType
engine_ics_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (EngineIcsClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof (EngineIcs),
			0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (TYPE_ENGINE, "EngineIcs", &info, 0);
	}

	return type;
}

static void
class_init (EngineIcsClass *klass)
{
	GObjectClass *object_class;
	
	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class->dispose = dispose;
	object_class->finalize = finalize;
}


static void
init (EngineIcs *engine)
{
	EngineIcsPrivate *priv;
	
	priv = g_new0 (EngineIcsPrivate, 1);

	engine->priv = priv;

	priv->term = SERVER_TERM (server_term_new ());
	gtk_signal_connect_after (GTK_OBJECT (priv->term), "size_allocate",
				  (GtkSignalFunc) ei_resize_cb,
				  engine);
	gtk_signal_connect (GTK_OBJECT (priv->term), "command",
			    (GtkSignalFunc) ei_command_cb,
			    engine);
	
	/* Set defaults */
	priv->user = NULL;
	priv->pass = NULL;
	priv->loggedin = FALSE;
	priv->list = NULL;
}

static void
dispose (GObject *obj)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;

	engine = ENGINE_ICS (obj);
	priv = engine->priv;

	g_source_remove (priv->read_cb);
	g_source_remove (priv->err_cb);

	g_io_channel_close (priv->read_chan);
	g_io_channel_unref (priv->read_chan);

	g_io_channel_close (priv->write_chan);
	g_io_channel_unref (priv->write_chan);

	stop_child (priv->childpid);

	engine_view_remove_all_game_views (engine_get_engine_view (ENGINE (engine)));
	
	G_OBJECT_CLASS (parent_class)->dispose (obj);
}

static void
finalize (GObject *obj)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	
	engine = ENGINE_ICS (obj);
	priv = engine->priv;

	g_free (priv);
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}



GObject *
engine_ics_new (const char *host, 
		const char *port, 
		const char *user,
		const char *pass, 
		const char *telnetprogram)
{		  
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	GameView *view;
	struct hostent *he;
	char *host_addr;
	char *arg[4];
	
	engine = g_object_new (ENGINE_TYPE_ICS, NULL);
	priv = engine->priv;
	
	/* Set up the user/pass stuff */
	if (user)  
		priv->user =  g_strdup (user);

	if (pass)
		priv->pass = g_strdup (pass);

	/* Figure out the args to use */
	if (telnetprogram == NULL)
		arg[0] = g_strdup (prefs_get_telnetprogram ());
	else
		arg[0] = g_strdup (telnetprogram);
	
	he = gethostbyname (host);

	if (he == NULL) { /* name lookup failed, bail out */
		herror(NULL); /* FIXME Dialog Box needed */
		return NULL;
	}

	if (he->h_addr_list[0] != NULL) {
		host_addr = g_strdup_printf ("%d.%d.%d.%d", 
					     (guchar)he->h_addr_list[0][0],
					     (guchar)he->h_addr_list[0][1],
  					     (guchar)he->h_addr_list[0][2],
					     (guchar)he->h_addr_list[0][3]);
		arg[1] = host_addr;
	} else {
		arg[1] = g_strdup (host);
	}
	
	
	arg[2] = g_strdup (port);
	arg[3] = NULL;
		
	/* Start the child process */
	start_child (arg,
		     &priv->read_chan,
		     &priv->write_chan,
		     &priv->childpid);

	priv->read_cb = g_io_add_watch (priv->read_chan, G_IO_IN, 
					ei_input_cb, engine);
	priv->err_cb = g_io_add_watch (priv->read_chan, G_IO_HUP | G_IO_ERR, 
				       ei_err_cb, engine);

	g_free (arg[0]);
	g_free (arg[1]);
	g_free (arg[2]);
	
	view = ei_add_view (engine);
	engine_view_add_info_view (engine_get_engine_view (ENGINE (engine)), 
				   host, GTK_WIDGET (priv->term));
	
	return G_OBJECT (engine);
}

static void
ei_resize (EngineIcs *engine)
{
	EngineIcsPrivate *priv;
	guint width, height;
	
	priv = engine->priv;

	if (priv->write_chan == NULL || !priv->loggedin)
		return;
	
	server_term_get_size (priv->term, &width, &height);
	if (width != priv->last_column_size) {
		write_child (priv->write_chan, "set width %d\n", width);
		priv->last_column_size = width;
	}

	if (height != priv->last_row_size) {
		write_child (priv->write_chan, "set height %d\n", height);
		priv->last_row_size = height;
	}
}

static void
ei_menu_draw (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	
	engine = ENGINE_ICS (data);
	priv = engine->priv;

	write_child (priv->write_chan, "draw\n");
}

static void
ei_menu_flag (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	
	engine = ENGINE_ICS (data);
	priv = engine->priv;
	
	write_child (priv->write_chan, "flag\n");
}

static void
ei_menu_resign (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	
	engine = ENGINE_ICS (data);
	priv = engine->priv;

	write_child (priv->write_chan, "resign\n");
}

static void
ei_menu_rematch (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	
	engine = ENGINE_ICS (data);
	priv = engine->priv;

	write_child (priv->write_chan, "rematch\n");
}

static void 
fill_menu (GtkMenuShell *shell, gint pos, gpointer data) 
{
	ICSEngineInfo *icsinfo = data;
	Engine *engine = icsinfo->engine;

	GnomeUIInfo ei_menu[] = {

		{GNOME_APP_UI_ITEM, N_("_Draw"), NULL,
		 ei_menu_draw, engine, NULL, 
		 GNOME_APP_PIXMAP_NONE, NULL, GDK_d, GDK_CONTROL_MASK, NULL},
 
		{GNOME_APP_UI_ITEM, N_("_Flag"), NULL,
		 ei_menu_flag, engine, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_TIMER_STOP, 
		 GDK_f, GDK_CONTROL_MASK, NULL},

		{GNOME_APP_UI_ITEM, N_("_Resign"), NULL,
		 ei_menu_resign, engine, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_STOP, 0, 0, NULL},

		GNOMEUIINFO_SEPARATOR, 

		{GNOME_APP_UI_ITEM, N_("Re_match"), NULL,
		 ei_menu_rematch, engine, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},

		GNOMEUIINFO_END
	};

	gnome_app_fill_menu (shell, ei_menu, NULL, TRUE, pos);
}

static ICSEngineInfo *
ei_get_info (GameView *view)
{
	return gtk_object_get_data (GTK_OBJECT (view), "gameinfo");
}

static void
ei_init_info (ICSEngineInfo *info) 
{
	info->game = 0;
	info->rel = 0;	
	info->ply  = 0;
	info->color = NONE;
	info->flag = FALSE;
}

static void 
board_move_cb (GtkWidget *widget, Square from, Square to, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	ICSEngineInfo *icsinfo = data;
	char str[100];

	engine = ENGINE_ICS (icsinfo->engine);
	priv = engine->priv;

	move_to_ascii (str, from, to);
	write_child (priv->write_chan, "%s\n", str);
}

static void
white_expired_cb (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	ICSEngineInfo *icsinfo = data;

	engine = ENGINE_ICS (icsinfo->engine);
	priv = engine->priv;

	if (icsinfo->flag && icsinfo->color != WHITE && prefs_get_autoflag ()) {
		debug_print (DEBUG_NORMAL, "Autoflagging\n");
		write_child (priv->write_chan, "flag\n");
	}
}

static void
black_expired_cb (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	ICSEngineInfo *icsinfo = data;

	engine = ENGINE_ICS (icsinfo->engine);
	priv = engine->priv;

	if (icsinfo->flag && icsinfo->color != BLACK && prefs_get_autoflag ()) {
		debug_print (DEBUG_NORMAL, "Autoflagging\n");
		write_child (priv->write_chan, "flag\n");
	}
}
	
static GameView *
ei_add_view (EngineIcs *engine)
{
	EngineIcsPrivate *priv;
	GtkWidget *view;
	ICSEngineInfo *info;

	priv = engine->priv;
	
	info = g_new0 (ICSEngineInfo, 1);
	ei_init_info (info);
	info->engine = ENGINE (engine);
	
	/* Build a new board window */
	view = game_view_new (fill_menu, info);

	board_set_white_lock (BOARD (GAME_VIEW (view)->board), BOARD_LOCK_COMPLETE);
	board_set_black_lock (BOARD (GAME_VIEW (view)->board), BOARD_LOCK_COMPLETE);

	g_signal_connect (G_OBJECT (GAME_VIEW (view)->board), "move",
			  G_CALLBACK (board_move_cb), info);
	g_signal_connect (G_OBJECT (GAME_VIEW (view)->white_clock), "expired",
			  G_CALLBACK (white_expired_cb), info);
	g_signal_connect (G_OBJECT (GAME_VIEW (view)->black_clock), "expired",
			  G_CALLBACK (black_expired_cb), info);
	
	engine_view_add_game_view (engine_get_engine_view (ENGINE (engine)), GAME_VIEW (view));

	gtk_object_set_data (GTK_OBJECT (view), "gameinfo", info);

	/* Add it to our list */
	priv->list = g_list_prepend (priv->list, view);
	gtk_signal_connect (GTK_OBJECT (view), "destroy",
			    (GtkSignalFunc) ei_view_cb,
			    engine);
	
	return GAME_VIEW (view);
}

static GameView *
ei_get_view (EngineIcs *engine, int game, gboolean create_view)
{
	EngineIcsPrivate *priv;
	GameView *view, *alt_view = NULL;
	ICSEngineInfo *info;
	GList *l;
	
	priv = engine->priv;
	
	for (l = priv->list; l != NULL; l = l->next) {		
		view = l->data;
		info = ei_get_info (view);

		if (info->game == game)
			return view;
		else if (info->game == 0)
			alt_view = view;
	}

	if (alt_view != NULL) {
		ICSEngineInfo *info;
		
		info = ei_get_info (alt_view);
		ei_init_info (info);

		return alt_view;
	}

	if (create_view)
		return ei_add_view (engine);

	return NULL;	
}

void
engine_ics_user (EngineIcs *engine)
{
	EngineIcsPrivate *priv;
	
	priv = engine->priv;

	if (!priv->user || !priv->pass)
		dialog_login (&(priv->user), &(priv->pass));

	write_child (priv->write_chan, "%s\n", priv->user);
}

void 
engine_ics_password (EngineIcs *engine)
{
	EngineIcsPrivate *priv;

	priv = engine->priv;

	write_child (priv->write_chan, "%s\n", priv->pass);
}

void 
engine_ics_invaliduser (EngineIcs *engine) 
{
	EngineIcsPrivate *priv;

	priv = engine->priv;
	
	if (!priv->loggedin) {
		priv->user = NULL;
		priv->pass = NULL;
	}
}

void 
engine_ics_invalidpassword (EngineIcs *engine) 
{
	EngineIcsPrivate *priv;

	priv = engine->priv;

	if (!priv->loggedin)
		priv->pass = NULL;
}

void 
engine_ics_prompt (EngineIcs *engine, gchar *str) 
{
	EngineIcsPrivate *priv;
	
	priv = engine->priv;

	if (!priv->loggedin) {
		priv->loggedin = TRUE;
		server_term_set_prompt (priv->term, str);
		write_child (priv->write_chan, "set style 12\n");
		ei_resize (engine);
	}
}

void
engine_ics_result (EngineIcs *engine, char *result)
{
	GameView *view;
	ICSEngineInfo *icsinfo;
	char *rstr;
	GameViewStatus status;
	gint game;

	/* Figure out what game */
	rstr = strchr (result, ' ');
	rstr++;
	game = atoi (rstr); 

	/* Figure out what happened */
	rstr = strrchr(result, ' ');
	rstr++;
	if (!strcmp (rstr, "1-0"))
		status = GAME_VIEW_STATUS_WHITE;
	else if (!strcmp (rstr, "0-1"))
		status = GAME_VIEW_STATUS_BLACK;
	else if (!strcmp (rstr, "1/2-1/2"))
		status = GAME_VIEW_STATUS_DRAW;
	else
		status = GAME_VIEW_STATUS_ABORTED;

	/* Update the info struct */
	view = ei_get_view (engine, game, FALSE);

	if (view == NULL)
		return;
	
	icsinfo = ei_get_info (view);
	icsinfo->game = 0;

	game_view_set_status (view, status);
}

int 
engine_ics_input (char *buf, int max) 
{
	guint len;

	if (have_read) {
		have_read = FALSE;
		return 0;
	}
		
	g_io_channel_read (chan, buf, max, &len);
	if (len < max)
		have_read = TRUE;
	
	return len;
}

void 
engine_ics_output (EngineIcs *engine, char *str, int c_index) 
{
	EngineIcsPrivate *priv;
	
	priv = engine->priv;

	debug_print (DEBUG_VERBOSE, "Engine Input: %s", str);
	if (c_index >= 0) {
		char *c_str = NULL;
		
		if (c_index <= 7)
			c_str = g_strdup_printf ("\033[3%dm", c_index);
		server_term_print (priv->term, c_str);
	}
	server_term_print (priv->term, str);
	server_term_print (priv->term, "\033[39m");
}

static Position *
engine_ics_board_to_pos (char *b)
{
	int i, j;
	Position *pos;

	pos = POSITION (position_new ());
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			Square n = A8 - i * 10 + j;                                                                             
			switch (b[(8*i)+j]) {
			case 'R':
				pos->square[n] = WR;
				break;
			case 'N':
				pos->square[n] = WN;
				break;
			case 'B':
				pos->square[n] = WB;
				break;
			case 'Q':
				pos->square[n] = WQ;
				break;
			case 'K':
				pos->square[n] = WK;
				position_set_white_king (pos, n);
				break;
			case 'P':
				pos->square[n] = WP;
				break;
			case 'r':
				pos->square[n] = BR;
				break;
			case 'n':
				pos->square[n] = BN;
				break;
			case 'b':
				pos->square[n] = BB;
				break;
			case 'q':
				pos->square[n] = BQ;
				break;
			case 'k':
				pos->square[n] = BK;
				position_set_black_king (pos, n);
				break;
			case 'p':
				pos->square[n] = BP;
				break;
			case '-':
				pos->square[n] = EMPTY;
				break;
			}
		}
	}	
	return pos;
}

void
engine_ics_update_board (EngineIcs *engine, char *boardstring)
{
	EngineIcsPrivate *priv;
	GameView *view;
	Position *pos;
	ICSEngineInfo *icsinfo;
	gchar **tokens, *description;
	char b[65], m[10];
	int game, colour, ply, rel;
	Square from, to;
	int i;

	priv = engine->priv;

	/* Debug info */
	debug_print (DEBUG_NORMAL, "Board String: %s\n", boardstring);
	
	/* Go through the board string */
	tokens = g_strsplit (boardstring, " ", 0);

	/* Game number */
	game = atoi (tokens[GAME]);

	/* Get the board assigned to this engine */
	view = ei_get_view (engine, game, TRUE);
	icsinfo = ei_get_info (view);

	/* Find out who is to move */ 
	if (!strcmp (tokens[TO_MOVE], "W"))
		colour = WHITE;
	else
		colour = BLACK;
	
	/* Find relation to the game and lock the board accordingly*/
	rel = atoi (tokens[RELATION]);
	switch (rel) {
	case -3:
	case -2:
	case 0:
		board_set_white_lock (BOARD (view->board), BOARD_LOCK_COMPLETE);
		board_set_black_lock (BOARD (view->board), BOARD_LOCK_COMPLETE);
		break;
	case -1:
		board_set_white_lock (BOARD (view->board), 
				      colour == WHITE ? BOARD_LOCK_COMPLETE : BOARD_LOCK_SELECT);
		board_set_black_lock (BOARD (view->board), 
				      colour == BLACK ? BOARD_LOCK_COMPLETE : BOARD_LOCK_SELECT);
		break;
	case 1:
		board_set_white_lock (BOARD (view->board), 
				      colour == WHITE ? BOARD_LOCK_NONE : BOARD_LOCK_COMPLETE);
		board_set_black_lock (BOARD (view->board), 
				      colour == BLACK ? BOARD_LOCK_NONE : BOARD_LOCK_COMPLETE);
		break;
	case 2:
		board_set_white_lock (BOARD (view->board), 
				      colour == WHITE ? BOARD_LOCK_NONE : BOARD_LOCK_COMPLETE);
		board_set_black_lock (BOARD (view->board), 
				      colour == BLACK ? BOARD_LOCK_NONE : BOARD_LOCK_COMPLETE);
		break;
	}
	
	/* Flip the board if necessary */ 
	board_set_flip (BOARD (view->board), atoi (tokens[FLIP]));

	/* Extract the board string */
	b[0] = '\0';
	for (i = BOARD_START; i <= BOARD_END; i++) {
		strcat (b, tokens[i]);
	}

	/* Get the last move */
	if (!strcmp (tokens[VERBOSE], "o-o") ||
	    !strcmp (tokens[VERBOSE], "o-o-o")) {
		strncpy (m, tokens[VERBOSE], strlen (tokens[VERBOSE])+1);
	} else if (strcmp (tokens[VERBOSE], "none")) {
		m[0] = tokens[VERBOSE][2];
		m[1] = tokens[VERBOSE][3];
		m[2] = tokens[VERBOSE][5];
		m[3] = tokens[VERBOSE][6];
		if (tokens[VERBOSE][7] == '=') {
			m[4] = tokens[VERBOSE][8];
			m[5] = '\0';
		} else {
			m[4] = '\0';
		}
	} else {
		strcpy (m, "none");
	}	
		
	/* Figure out what ply this is */
	if (!strcmp (m, "none"))
		ply =  0;
	else if (colour == WHITE)
		ply = atoi (tokens [MOVE_NUM])*2-2;
	else
		ply = atoi (tokens [MOVE_NUM])*2-1;

	/* Update the game view */
	/* FIXME Only set once */
	game_view_set_white (view, tokens [WHITE_NAME]);
	game_view_set_black (view, tokens [BLACK_NAME]);

	description = g_strdup_printf ("%s %s", tokens[START_TIME], tokens[START_INC]);
	game_view_set_description (view, description);
	g_free (description);
	
	clock_set_time (CLOCK (view->white_clock), atoi (tokens [WHITE_TIME]));
	clock_set_time (CLOCK (view->black_clock), atoi (tokens [BLACK_TIME]));

	game_view_set_to_move (view, colour);

	if (rel >= -1 && rel <= 1)
		game_view_set_status (view, GAME_VIEW_STATUS_PROGRESS);
	else
		game_view_set_status (view, GAME_VIEW_STATUS_NONE);
	
	icsinfo->flag = (rel == 1 || rel == -1) ? TRUE : FALSE;
	if (rel == 1)
		icsinfo->color = colour;
	else if (rel == -1)
		icsinfo->color = (colour == WHITE) ? BLACK : WHITE;

	g_strfreev (tokens);

	/* Setup the board and figure out the move if there */
	/* was no dnd update or this is a different game */
	pos = board_get_position (BOARD (view->board));
	if (strcmp (m, "none") && ply - icsinfo->ply == 1) {
		icsinfo->rel = rel;
		icsinfo->ply = ply;
		ascii_to_move (pos, m, &from, &to);
		if (position_get_color_to_move (pos) != colour) {
				board_move (BOARD (view->board), from, to);
				move_list_add (MOVE_LIST (view->movelist), from, to);
		}
	} else {
		pos = engine_ics_board_to_pos (b);
		position_set_color_to_move (pos, colour);
		icsinfo->game = game;
		icsinfo->rel = rel;		
		icsinfo->ply = ply;
		
		board_set_position (BOARD (view->board), pos);
		move_list_clear_initial (MOVE_LIST (view->movelist), ply, pos);
	}
}

static gboolean
ei_input_cb (GIOChannel *source,
	     GIOCondition condition,
	     gpointer data)
{
	EngineIcs *engine = data;
	EngineIcsPrivate *priv;	
	int token;

	priv = engine->priv;
	
	G_LOCK (ICS_INPUT);
	chan = source;
	token = yylex (engine);
	G_UNLOCK (ICS_INPUT);
	
	return TRUE;
}

static gboolean
ei_err_cb (GIOChannel *source,
	   GIOCondition condition,
	   gpointer data)
{
	g_signal_emit_by_name (G_OBJECT (data), "error", ENGINE_ERROR_CRITICAL);

	return FALSE;
}

static void
ei_resize_cb (GtkWidget *widget, GtkRequisition *requisition, gpointer data)
{
	ei_resize (data);
}

static void
ei_command_cb (GtkWidget *widget, gchar *command, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;

	engine = ENGINE_ICS (data);
	priv = engine->priv;

	write_child (priv->write_chan, "%s\n", command);
}

static void
ei_view_cb (GtkWidget *widget, gpointer data)
{
	EngineIcs *engine;
	EngineIcsPrivate *priv;
	GList *elem;
	ICSEngineInfo *info;

	engine = ENGINE_ICS (data);
	priv = engine->priv;

	elem = g_list_find (priv->list, widget);	
	if (elem != NULL) {
		info = ei_get_info (GAME_VIEW (widget));
		g_free (info);
		
		priv->list = g_list_remove (priv->list, elem->data);
	}
}

