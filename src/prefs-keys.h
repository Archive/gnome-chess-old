/* 
 * Copyright (C) 2003 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef PREFS_KEYS_H
#define PREFS_KEYS_H

#define BOARD_LIGHT_COLOR "/apps/gnome-chess/board/light_color"
#define BOARD_DARK_COLOR "/apps/gnome-chess/board/dark_color"
#define BOARD_PIECE_DIR "/apps/gnome-chess/board/piece_dir"

#define GAME_NOTATION "/apps/gnome-chess/game/notation"
#define GAME_BEEP_ON_MOVE "/apps/gnome-chess/game/beep_on_move"
#define GAME_PROMOTE_TO_QUEEN "/apps/gnome-chess/game/promote_to_queen"
#define GAME_AUTOFLAG "/apps/gnome-chess/game/autoflag"

#define PROGRAMS_INFO "/apps/gnome-chess/programs"

#define SERVERS_TELNET_PROGRAM "/apps/gnome-chess/servers/telnet_program"
#define SERVER_INFO "/apps/gnome-chess/servers"

#define PALETTE_COLORS "/apps/gnome-chess/colors/palette"

#endif 

