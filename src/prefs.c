/* 
 * Copyright (C) 1999 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#include <string.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include "prefs-keys.h"
#include "prefs.h"

typedef struct {
	guint8 dark_r;
	guint8 dark_g;
	guint8 dark_b;

	guint8 light_r;
	guint8 light_g;
	guint8 light_b;
	
	char *piecedir;
	
	gint notation;
	gboolean beeponmove;
	gboolean promotetoqueen;
	gboolean autoflag;

	GSList *programs;
	GSList *servers;

	gchar *telnet_program;

	GdkColor palette[18];
	GSList *palette_list;
} Prefs;

GConfClient *client;
static Prefs prefs;

static char *
xml_get_prop (xmlNodePtr node, const char *name) 
{
	char *buf, *new_val;
	
	buf = xmlGetProp (node, name);
	new_val = g_strdup (buf);
	xmlFree (buf);
	
	return new_val;
}

static PrefsProgram *
get_program_from_xml (const char *xml)
{
	xmlNodePtr node;
	xmlDocPtr doc;
	PrefsProgram *pp;
	
	if (!(doc = xmlParseDoc ((char *)xml)))
		return NULL;

	node = doc->children;
	if (strcmp (node->name, "program") != 0) {
		xmlFreeDoc (doc);
		return NULL;
	}

	pp = g_new0 (PrefsProgram, 1);

	pp->name = xml_get_prop (node, "name");
	pp->location = xml_get_prop (node, "location");

	xmlFreeDoc (doc);

	return pp;
}

static char *
get_xml_from_program (PrefsProgram *pp)
{
	xmlNodePtr root;
	char *tmp;
	xmlChar *xmlbuf;
	xmlDocPtr doc;
	int n;

	doc = xmlNewDoc ("1.0");

	root = xmlNewDocNode (doc, NULL, "program", NULL);
	xmlDocSetRootElement (doc, root);

	xmlSetProp (root, "name", pp->name);
	xmlSetProp (root, "location", pp->location);

	xmlDocDumpMemory (doc, &xmlbuf, &n);
	xmlFreeDoc (doc);

	/* remap to glib memory */
	tmp = g_malloc (n + 1);
	memcpy (tmp, xmlbuf, n);
	tmp[n] = '\0';
	xmlFree (xmlbuf);

	return tmp;
}


static PrefsServer *
get_server_from_xml (const char *xml)
{
	xmlNodePtr node;
	xmlDocPtr doc;
	PrefsServer *ps;
	
	if (!(doc = xmlParseDoc ((char *)xml)))
		return NULL;

	node = doc->children;
	if (strcmp (node->name, "program") != 0) {
		xmlFreeDoc (doc);
		return NULL;
	}

	ps = g_new0 (PrefsServer, 1);

	ps->server = xml_get_prop (node, "server");
	ps->port = xml_get_prop (node, "port");
	ps->user = xml_get_prop (node, "user");
	ps->password = xml_get_prop (node, "password");
	ps->connect = xml_get_prop (node, "connect");

	if (ps->connect == NULL)
		ps->connect = g_strdup (prefs_get_telnetprogram ());

	xmlFreeDoc (doc);

	return ps;
}

static char *
get_xml_from_server (PrefsServer *ps)
{
	xmlNodePtr root;
	char *tmp;
	xmlChar *xmlbuf;
	xmlDocPtr doc;
	int n;

	doc = xmlNewDoc ("1.0");

	root = xmlNewDocNode (doc, NULL, "program", NULL);
	xmlDocSetRootElement (doc, root);

	xmlSetProp (root, "server", ps->server);
	xmlSetProp (root, "port", ps->port);
	xmlSetProp (root, "user", ps->user);
	xmlSetProp (root, "password", ps->password);
	xmlSetProp (root, "connect", ps->connect);

	xmlDocDumpMemory (doc, &xmlbuf, &n);
	xmlFreeDoc (doc);

	/* remap to glib memory */
	tmp = g_malloc (n + 1);
	memcpy (tmp, xmlbuf, n);
	tmp[n] = '\0';
	xmlFree (xmlbuf);

	return tmp;
}

void 
prefs_init (void)
{
	GSList *xml, *l;
	char *color = NULL;
	GdkColor gc;
	int i;

	client = gconf_client_get_default ();

	gconf_client_add_dir (client, "/apps/gnome-chess", GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
	
	color = gconf_client_get_string (client, BOARD_LIGHT_COLOR, NULL);
	gdk_color_parse (color, &gc);
	prefs.light_r = gc.red;
	prefs.light_g = gc.green;
	prefs.light_b = gc.blue;

	color = gconf_client_get_string (client, BOARD_DARK_COLOR, NULL);
	gdk_color_parse (color, &gc);
	prefs.dark_r = gc.red;
	prefs.dark_g = gc.green;
	prefs.dark_b = gc.blue;

	prefs.piecedir = gconf_client_get_string (client, BOARD_PIECE_DIR, NULL);

	prefs.notation = gconf_client_get_int (client, GAME_NOTATION, NULL);
	prefs.beeponmove = gconf_client_get_bool (client, GAME_BEEP_ON_MOVE, NULL);
	prefs.promotetoqueen = gconf_client_get_bool (client, GAME_PROMOTE_TO_QUEEN, NULL);
	prefs.autoflag = gconf_client_get_bool (client, GAME_AUTOFLAG, NULL);

	xml = gconf_client_get_list (client, PROGRAMS_INFO, GCONF_VALUE_STRING, NULL);
	for (l = xml; l != NULL; l = l->next) {
		PrefsProgram *pp;
		
		pp = get_program_from_xml (l->data);
		prefs.programs = g_slist_append (prefs.programs, pp);
		g_free (l->data);
	}	
	g_slist_free (xml);

	prefs.telnet_program = gconf_client_get_string (client, SERVERS_TELNET_PROGRAM, NULL);

	xml = gconf_client_get_list (client, SERVER_INFO, GCONF_VALUE_STRING, NULL);
	for (l = xml; l != NULL; l = l->next) {
		PrefsServer *ps;
		
		ps = get_server_from_xml (l->data);
		prefs.servers = g_slist_append (prefs.servers, ps);
		g_free (l->data);
	}	
	g_slist_free (xml);

	prefs.palette_list = gconf_client_get_list (client, PALETTE_COLORS, GCONF_VALUE_STRING, NULL);
	for (i = 0; i < 18; i++) {
		const char *color_str;
		
		color_str = g_slist_nth_data (prefs.palette_list, i);
		gdk_color_parse (color_str, &prefs.palette[i]);
	}
}

void
prefs_sync (void) 
{
	gconf_client_suggest_sync (client, NULL);	
}

void 
prefs_rm_notification (guint id)
{
	gconf_client_notify_remove (client, id);
}

static const char *
get_spec (guint8 r, guint8 g, guint8 b)
{
	static char spec[8];

	g_snprintf (spec, sizeof (spec), "#%02x%02x%02x", r, g, b);

	return spec;
}

guint 
prefs_add_notification_light_color (GConfClientNotifyFunc func, gpointer data)
{
	return gconf_client_notify_add (client, BOARD_LIGHT_COLOR, func, data, NULL, NULL);
}

void 
prefs_get_light_color (guint8 *r, guint8 *g, guint8 *b)
{
	*r = prefs.light_r;
	*g = prefs.light_g;
	*b = prefs.light_b;
}

void 
prefs_set_light_color (guint8 r, guint8 g, guint8 b)
{
 	prefs.light_r = r;
	prefs.light_g = g;
	prefs.light_b = b;

	gconf_client_set_string (client, BOARD_LIGHT_COLOR, get_spec (r, g, b), NULL);
}

guint 
prefs_add_notification_dark_color (GConfClientNotifyFunc func, gpointer data)
{
	return gconf_client_notify_add (client, BOARD_DARK_COLOR, func, data, NULL, NULL);
}

void 
prefs_get_dark_color (guint8 *r, guint8 *g, guint8 *b)
{
	*r = prefs.dark_r;
	*g = prefs.dark_g;
	*b = prefs.dark_b;
}

void 
prefs_set_dark_color (guint8 r, guint8 g, guint8 b)
{
	prefs.dark_r = r;
	prefs.dark_g = g;
	prefs.dark_b = b;

	gconf_client_set_string (client, BOARD_DARK_COLOR, get_spec (r, g, b), NULL);
}


const char *
prefs_get_piecedir (void)
{
	return prefs.piecedir;
}

void 
prefs_set_piecedir (const char *piecedir)
{
	if (prefs.piecedir)
		g_free (prefs.piecedir);
	
	prefs.piecedir = g_strdup (piecedir);

	gconf_client_set_string (client, BOARD_PIECE_DIR, piecedir, NULL);
}

gint 
prefs_get_notation (void)
{
	return prefs.notation;
}

void
prefs_set_notation (gint notation)
{
	prefs.notation = notation;

	gconf_client_set_int (client, GAME_NOTATION, notation, NULL);
}

gboolean 
prefs_get_beeponmove (void)
{
	return prefs.beeponmove;
}

void
prefs_set_beeponmove (gboolean beep)
{
	prefs.beeponmove = beep;

	gconf_client_set_bool (client, GAME_BEEP_ON_MOVE, beep, NULL);
}

gboolean 
prefs_get_promotetoqueen (void)
{
	return prefs.promotetoqueen;
}

void
prefs_set_promotetoqueen (gboolean queen)
{
	prefs.promotetoqueen = queen;

	gconf_client_set_bool (client, GAME_PROMOTE_TO_QUEEN, queen, NULL);
}

gboolean
prefs_get_autoflag (void)
{
	return prefs.autoflag;
}

void
prefs_set_autoflag (gboolean autoflag)
{
	prefs.autoflag = autoflag;

	gconf_client_set_bool (client, GAME_AUTOFLAG, autoflag, NULL);
}

guint 
prefs_add_notification_programs (GConfClientNotifyFunc func, gpointer data)
{
	return gconf_client_notify_add (client, PROGRAMS_INFO, func, data, NULL, NULL);
}

GSList *
prefs_get_programs (void)
{
	return prefs.programs;
}

void
prefs_set_programs (GSList *programs)
{
	GSList *xml = NULL, *l;
	
	prefs_free_program_list (prefs.programs);
	prefs.programs = prefs_copy_program_list (programs);
	
	for (l = prefs.programs; l != NULL; l = l->next) {
		char *str;
		
		str = get_xml_from_program (l->data);
		if (str != NULL)
			xml = g_slist_append (xml, str);
	}

	gconf_client_set_list (client, PROGRAMS_INFO, GCONF_VALUE_STRING, xml, NULL);

	for (l = xml; l != NULL; l = l->next)
		g_free (l->data);
	g_slist_free (xml);
}

GSList *
prefs_copy_program_list (GSList *list) 
{
	GSList *l, *new = NULL;

	for (l = list; l != NULL; l = l->next) {
		PrefsProgram *pp = l->data;		
		PrefsProgram *pp2 = g_new0 (PrefsProgram, 1);

		pp2->name = g_strdup (pp->name);
		pp2->location = g_strdup (pp->location);

		new = g_slist_append (new, pp2);
	}

	return new;
}

void 
prefs_free_program_list (GSList *list)
{
	GSList *l;
	
	for (l = list; l != NULL; l = l->next) {
		PrefsProgram *pp = l->data;
		
		g_free (pp->name);
		g_free (pp->location);
		g_free (pp);
	}
	
	g_slist_free (list);
}

guint 
prefs_add_notification_servers (GConfClientNotifyFunc func, gpointer data)
{
	return gconf_client_notify_add (client, SERVER_INFO, func, data, NULL, NULL);
}

GSList *
prefs_get_servers (void)
{
	return prefs.servers;;
}

void
prefs_set_servers (GSList *servers)
{
	GSList *xml = NULL, *l;
	
	prefs_free_server_list (prefs.servers);
	prefs.servers = prefs_copy_server_list (servers);
	
	for (l = prefs.servers; l != NULL; l = l->next) {
		char *str;
		
		str = get_xml_from_server (l->data);
		if (str != NULL)
			xml = g_slist_append (xml, str);
	}

	gconf_client_set_list (client, SERVER_INFO, GCONF_VALUE_STRING, xml, NULL);

	for (l = xml; l != NULL; l = l->next)
		g_free (l->data);
	g_slist_free (xml);
}

GSList *
prefs_copy_server_list (GSList *list) 
{
	GSList *l, *new = NULL;

	for (l = list; l != NULL; l = l->next) {
		PrefsServer *ps = l->data;		
		PrefsServer *ps2 = g_new0 (PrefsServer, 1);
		
		ps2->user = g_strdup (ps->user);
		ps2->server = g_strdup (ps->server);
		ps2->port = g_strdup (ps->port);
		ps2->password = g_strdup (ps->password);
		ps2->connect = g_strdup (ps->connect);

		new = g_slist_append (new, ps2);
	}

	return new;
}

void 
prefs_free_server_list (GSList *list)
{
	GSList *l;
	
	for (l = list; l != NULL; l = l->next) {
		PrefsServer *ps = l->data;
		
		g_free (ps->server);
		g_free (ps->port);
		g_free (ps->user);
		g_free (ps->password);
		g_free (ps->connect);
		g_free (ps);
	}
	
	g_slist_free (list);
}

const gchar *
prefs_get_telnetprogram (void)
{
	return prefs.telnet_program;
}

void 
prefs_set_telnetprogram (const gchar *telnet_program)
{
	if (prefs.telnet_program)
		g_free (prefs.telnet_program);

	prefs.telnet_program = g_strdup (telnet_program);

	gconf_client_set_string (client, SERVERS_TELNET_PROGRAM, telnet_program, NULL);
}

void
prefs_get_palette_color (guint color, guint8 *r, guint8 *g, guint8 *b)
{
	GdkColor gc;
	
	g_return_if_fail (color < 18);

	gdk_color_parse (g_slist_nth_data (prefs.palette_list, color), &gc);

	*r = gc.red;
	*g = gc.green;
	*b = gc.blue;
}

void
prefs_set_palette_color (guint color, guint8 r, guint8 g, guint8 b)
{
	g_return_if_fail (color < 18);

	gconf_value_set_string (g_slist_nth_data (prefs.palette_list, color), get_spec (r, g, b));
	gdk_color_parse (get_spec (r, g, b), &prefs.palette[color]);

	gconf_client_set_list (client, PALETTE_COLORS, GCONF_VALUE_STRING, prefs.palette_list, NULL);
}

GdkColor *
prefs_get_palette (void)
{
	return prefs.palette;
}
