/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-registry.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include "engine-registry.h"

GHashTable *engines = NULL;


static void
window_destroyed_cb (gpointer data, GObject *object)
{
	engine_registry_remove (ENGINE (data));
}

static gboolean 
foreach_remove_cb (gpointer key, gpointer value, gpointer data)
{
	Engine *engine = key;
	BoardWindow *window = value;
	
	g_object_unref (engine);
	g_object_weak_unref (G_OBJECT (window), window_destroyed_cb, engine);
	
	return TRUE;
}

static void
exit_cb (void)
{
	g_hash_table_foreach_remove (engines, foreach_remove_cb, NULL);
}

void 
engine_registry_init (void)
{
	engines = g_hash_table_new (g_direct_hash, g_direct_equal);
	g_atexit (exit_cb);
}

void
engine_registry_add (Engine *engine)
{
	BoardWindow *window;

 	window = engine_get_board_window (engine);

	g_object_ref (engine);
	g_object_weak_ref (G_OBJECT (window), window_destroyed_cb, engine);

	g_hash_table_insert (engines, engine, window);
}

void 
engine_registry_remove (Engine *engine)
{
	BoardWindow *window;
	
	window = g_hash_table_lookup (engines, engine);
	if (window == NULL)
		return;
	
	g_hash_table_remove (engines, engine);
	g_object_unref (engine);
	g_object_weak_unref (G_OBJECT (window), window_destroyed_cb, engine);
}

guint
engine_registry_count (void)
{
	return g_hash_table_size (engines);
}


