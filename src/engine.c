/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine.c
 *
 * Copyright (C) 2001  Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Robert Wilhelm
 *          JP Rosevear
 */

#include <gnome.h>
#include "gcmarshal.h"
#include "engine-view.h"
#include "engine.h"

struct _EnginePrivate {
	GtkWidget *engine_view;
};

enum props {
	PROP_0,
	PROP_ENGINE_VIEW
};

enum signals {
	GAME_SIGNAL,
	MOVE_SIGNAL,
	UNMOVE_SIGNAL,
	INFO_SIGNAL,
	ERROR_SIGNAL,
	LAST_SIGNAL
};
static gint signals[LAST_SIGNAL] = { 0 };

/* Prototypes */
static void class_init (EngineClass *klass);
static void init (Engine *engine);
static void set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);
static void get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void dispose (GObject *obj);
static void finalize (GObject *obj);

static GObjectClass *parent_class = NULL;

GType
engine_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (EngineClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof (Engine),
			0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (G_TYPE_OBJECT, "Engine", &info, 0);
	}

	return type;
}

static void
class_init (EngineClass *klass)
{
	GObjectClass *object_class;
	
	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class->set_property = set_property;
	object_class->get_property = get_property;	
	object_class->dispose = dispose;
	object_class->finalize = finalize;
	
	g_object_class_install_property (object_class, PROP_ENGINE_VIEW,
					 g_param_spec_object ("engine_view",
							      NULL,
							      NULL,
							      ENGINE_TYPE_VIEW,
							      G_PARAM_READABLE));
	
	signals[GAME_SIGNAL] =
		g_signal_new ("game",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EngineClass, game),
			      NULL, NULL,
			      gc_marshal_VOID__INT_POINTER, G_TYPE_NONE,
			      2, G_TYPE_INT, G_TYPE_POINTER);
		
	signals[MOVE_SIGNAL] =
		g_signal_new ("move",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EngineClass, move),
			      NULL, NULL,
			      gtk_marshal_NONE__INT_INT, G_TYPE_NONE,
			      2, G_TYPE_INT, G_TYPE_INT);

	signals[UNMOVE_SIGNAL] =
		g_signal_new ("unmove",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EngineClass, unmove),
			      NULL, NULL,
			      gtk_marshal_NONE__INT, GTK_TYPE_NONE,
			      1, GTK_TYPE_INT);

	signals[ERROR_SIGNAL] =
		g_signal_new ("error",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EngineClass, error),
			      NULL, NULL,
			      gtk_marshal_NONE__INT, GTK_TYPE_NONE,
			      1, GTK_TYPE_INT);
}

static void
init (Engine *engine)
{
	EnginePrivate *priv;

	priv = g_new0 (EnginePrivate, 1);

	engine->priv = priv;

	priv->engine_view = engine_view_new ();
}

static void
set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
	Engine *engine = ENGINE (object);
	EnginePrivate *priv;
	
	priv = engine->priv;

	switch (property_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
	Engine *engine = ENGINE (object);
	EnginePrivate *priv;
	
	priv = engine->priv;

	switch (property_id) {
	case PROP_ENGINE_VIEW:
		g_value_set_object (value, priv->engine_view);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
dispose (GObject *obj)
{
	Engine *engine;
	EnginePrivate *priv;

	engine = ENGINE (obj);
	priv = engine->priv;

	G_OBJECT_CLASS (parent_class)->dispose (obj);
}

static void
finalize (GObject *obj)
{
	Engine *engine;
	EnginePrivate *priv;

	engine = ENGINE (obj);
	priv = engine->priv;

	g_free (priv);

	G_OBJECT_CLASS (parent_class)->finalize (obj);
}



EngineView *
engine_get_engine_view (Engine *engine)
{
	EnginePrivate *priv;
	
	g_return_val_if_fail (engine != NULL, NULL);
	g_return_val_if_fail (IS_ENGINE (engine), NULL);

	priv = engine->priv;
	
	return ENGINE_VIEW (priv->engine_view);
}

void 
engine_fill_menu (Engine *engine, GtkMenuShell *shell, gint pos)
{
	EngineClass *klass;

	g_return_if_fail (engine != NULL);
	g_return_if_fail (IS_ENGINE (engine));
	g_return_if_fail (shell != NULL);
	g_return_if_fail (GTK_IS_MENU_SHELL (shell));

	klass = ENGINE_CLASS (G_OBJECT_GET_CLASS (engine));
	
	if (klass->engine_fill_menu)
		klass->engine_fill_menu (engine, shell, pos);
}
