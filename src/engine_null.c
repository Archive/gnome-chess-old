/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-null.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include "board.h"
#include "engine_null.h"

struct _EngineNullPrivate 
{
	GtkWidget *game_view;
};

static EngineClass *parent_class = NULL;

static void class_init (EngineNullClass *klass);
static void init (EngineNull *null);
static void finalize (GObject *obj);

GType
engine_null_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (EngineNullClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof (EngineNull),
			0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (TYPE_ENGINE, "EngineNull", &info, 0);
	}

	return type;
}

static void
class_init (EngineNullClass *klass)
{
	GObjectClass *object_class;
	
	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = finalize;
}

static void
init (EngineNull *null)
{
	EngineNullPrivate *priv;

	priv = g_new0 (EngineNullPrivate, 1);

	null->priv = priv;

	priv->game_view = game_view_new (NULL, NULL);

	board_set_white_lock (BOARD (GAME_VIEW (priv->game_view)->board), BOARD_LOCK_NONE);
	board_set_black_lock (BOARD (GAME_VIEW (priv->game_view)->board), BOARD_LOCK_NONE);	
}

static void
finalize (GObject *obj)
{
	EngineNull *null = ENGINE_NULL (obj);
	EngineNullPrivate *priv;

	priv = null->priv;
	
	g_free (priv);
	null->priv = NULL;
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}


GObject *
engine_null_new (void)
{
	EngineNull *null;
	EngineView *view;
	
	null = g_object_new (ENGINE_TYPE_NULL, NULL);
	view = engine_get_engine_view (ENGINE (null));
	
	engine_view_add_game_view (view, GAME_VIEW (null->priv->game_view));

	return G_OBJECT (null);
}

