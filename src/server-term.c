/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* server-term.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <gnome.h>
#include <string.h>
#include <ctype.h>
#include <vte/vte.h>
#include "prefs.h"
#include "server-term.h"

static void class_init (ServerTermClass *klass);
static void init (ServerTerm *term);

static void st_command_activate_cb (GtkWidget *widget, gpointer data);
static gboolean st_command_key_press_cb (GtkWidget *widget, GdkEventKey *event, gpointer data);

int colconv[] =
{7, 0, 4, 2, 1, 3, 5, 3, 3 + 10, 2 + 10, 6, 6 + 10, 4 + 10, 5 + 10, 10 + 0, 0};

static GtkVBoxClass *parent_class = NULL;

struct _ServerTermPrivate {
	GtkWidget *term;

	GList *above;
	gchar *current;
	GList *below;
	gboolean on_stack;
	
	GtkWidget *prompt;
	GtkWidget *command;
};

/* Class signals */
enum {
	COMMAND_SIGNAL,
	LAST_SIGNAL
};
static gint term_signals[LAST_SIGNAL] = { 0 };

GtkType
server_term_get_type (void)
{
  static GtkType type = 0;

  if (type == 0)
    {
      static const GtkTypeInfo info =
      {
        "ServerTerm",
        sizeof (ServerTerm),
        sizeof (ServerTermClass),
        (GtkClassInitFunc) class_init,
        (GtkObjectInitFunc) init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      type = gtk_type_unique (gtk_vbox_get_type (), &info);
    }

  return type;
}

static void
class_init (ServerTermClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (gtk_vbox_get_type ());

	term_signals[COMMAND_SIGNAL]
		= gtk_signal_new ("command",
				  GTK_RUN_FIRST,
				  GTK_CLASS_TYPE (object_class),
				  GTK_SIGNAL_OFFSET (ServerTermClass, command),
				  gtk_marshal_VOID__STRING,
				  GTK_TYPE_NONE, 1,
				  GTK_TYPE_STRING);
}


static void
init (ServerTerm *term)
{
	ServerTermPrivate *priv;
	GtkWidget *sb, *hbox;
	GdkColor *palette;
	
	priv = g_new0 (ServerTermPrivate, 1);

	term->priv = priv;

	gtk_box_set_spacing (GTK_BOX (term), 4);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	
	priv->term = vte_terminal_new ();
	vte_terminal_set_cursor_blinks (VTE_TERMINAL (priv->term), FALSE);
	vte_terminal_set_scrollback_lines (VTE_TERMINAL (priv->term), 100);
	vte_terminal_set_size (VTE_TERMINAL (priv->term), 80, 10);

	palette = prefs_get_palette ();
	vte_terminal_set_colors (VTE_TERMINAL (priv->term), &palette[16], &palette[17], palette, 16);
	gtk_widget_show (priv->term);

	sb = gtk_vscrollbar_new (GTK_ADJUSTMENT (VTE_TERMINAL (priv->term)->adjustment));
	GTK_WIDGET_UNSET_FLAGS (sb, GTK_CAN_FOCUS);
	gtk_widget_show (sb);

	gtk_box_pack_start (GTK_BOX (hbox), priv->term, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), sb, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (term), hbox, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_widget_show (hbox);

	priv->prompt = gtk_label_new ("Prompt");
	gtk_widget_show (priv->prompt);
	gtk_box_pack_start (GTK_BOX (hbox), priv->prompt, FALSE, FALSE, 0);
	
	priv->command = gtk_entry_new ();
	gtk_widget_show (priv->command);
	gtk_box_pack_start (GTK_BOX (hbox), priv->command, TRUE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (term), hbox, FALSE, FALSE, 0);

	/* Attach the command call backs */
	gtk_signal_connect (GTK_OBJECT (priv->command), "key-press-event",
			    (GtkSignalFunc) st_command_key_press_cb,
			    term);			    
	gtk_signal_connect (GTK_OBJECT (priv->command), "activate",
			    (GtkSignalFunc) st_command_activate_cb,
			    term);
}



GtkWidget *
server_term_new (void)
{
	GtkWidget *widget;
	
	widget = gtk_type_new (server_term_get_type ());

	return widget;
}

void
server_term_get_size (ServerTerm *term, guint *width, guint *height)
{	
	ServerTermPrivate *priv;

	g_return_if_fail (term != NULL);
	g_return_if_fail (IS_SERVER_TERM (term));
	
	priv = term->priv;

	*width = VTE_TERMINAL (priv->term)->column_count;
	*height = VTE_TERMINAL (priv->term)->row_count;
}

void
server_term_hide_prompt (ServerTerm *term)
{
	ServerTermPrivate *priv;

	g_return_if_fail (term != NULL);
	g_return_if_fail (IS_SERVER_TERM (term));
	
	priv = term->priv;

	gtk_widget_hide (priv->prompt);
	gtk_widget_hide (priv->command);
}

void
server_term_show_prompt (ServerTerm *term)
{
	ServerTermPrivate *priv;

	g_return_if_fail (term != NULL);
	g_return_if_fail (IS_SERVER_TERM (term));
	
	priv = term->priv;

	gtk_widget_show (priv->prompt);
	gtk_widget_show (priv->command);
}

void
server_term_set_prompt (ServerTerm *term, gchar *prompt)
{
	ServerTermPrivate *priv;

	g_return_if_fail (term != NULL);
	g_return_if_fail (IS_SERVER_TERM (term));
	g_return_if_fail (prompt != NULL);

	priv = term->priv;

	gtk_label_set (GTK_LABEL (priv->prompt), prompt); 
}

static void
print_raw_text (GtkWidget *widget, unsigned char *text)
{
	char num[8];
	int reverse = 0, under = 0, bold = 0;
	int comma, k, i = 0, j = 0;
	size_t len = strlen (text);
	unsigned char *newtext = malloc (len + 1024);

	while (i < len) {
		switch (text[i]) {
		case 3:
			i++;
			if (!isdigit (text[i])) {
				newtext[j] = 27;
				j++;
				newtext[j] = '[';
				j++;
				newtext[j] = '0';
				j++;
				newtext[j] = 'm';
				j++;
				i--;
				goto jump2;
			}
			k = 0;
			comma = FALSE;
			while (i < len) {
				if (text[i] >= '0' && text[i] <= '9' && k < 2) {
					num[k] = text[i];
					k++;
				} else {
					int col, mirc;
					num[k] = 0;
					newtext[j] = 27;
					j++;
					newtext[j] = '[';
					j++;
					if (k == 0) {
						newtext[j] = '0';
						j++;
						newtext[j] = 'm';
						j++;
					} else {
						if (comma)
							col = 40;
						else
							col = 30;
						mirc = atoi (num);
						mirc = colconv[mirc];
						if (mirc > 9) {
							mirc -= 10;
							sprintf ((char *) &newtext[j], "1;%dm", mirc + col);
						} else {
							sprintf ((char *) &newtext[j], "0;%dm", mirc + col);
						}
						j = strlen (newtext);
					}
					switch (text[i]) {
					case ',':
						comma = TRUE;
						break;
					default:
						goto jump;
					}
					k = 0;
				}
				i++;
			}
			break;
		case '\026':             /* REVERSE */
			if (reverse) {
				reverse = FALSE;
				strcpy (&newtext[j], "\033[27m");
			} else {
				reverse = TRUE;
				strcpy (&newtext[j], "\033[7m");
			}
			j = strlen (newtext);
			break;
		case '\037':             /* underline */
			if (under) {
				under = FALSE;
				strcpy (&newtext[j], "\033[24m");
			} else {
				under = TRUE;
				strcpy (&newtext[j], "\033[4m");
			}
			j = strlen (newtext);
			break;
		case '\002':             /* bold */
			if (bold) {
				bold = FALSE;
				strcpy (&newtext[j], "\033[22m");
			} else {
				bold = TRUE;
				strcpy (&newtext[j], "\033[1m");
			}
			j = strlen (newtext);
			break;
		case '\007':
			newtext[j] = text[i];
			j++;
			break;
		case '\017':             /* reset all */
			strcpy (&newtext[j], "\033[0m");
			j = strlen (newtext);
			reverse = FALSE;
			bold = FALSE;
			under = FALSE;
			break;
		case '\n':
			newtext[j] = '\r';
			j++;
		default:
			newtext[j] = text[i];
			j++;
		}
	jump2:
		i++;
	jump:
		   ;
	}
	newtext[j] = 0;
	vte_terminal_feed (VTE_TERMINAL (widget), newtext, j);
	free (newtext);
}

void
server_term_print (ServerTerm *term, gchar *text)
{
	ServerTermPrivate *priv;
	
	g_return_if_fail (term != NULL);
	g_return_if_fail (IS_SERVER_TERM (term));
	g_return_if_fail (text != NULL);

	priv = term->priv;
	
	print_raw_text (priv->term, text);
}

void
server_term_clear (ServerTerm *term)
{
	ServerTermPrivate *priv;
	
	g_return_if_fail (term != NULL);
	g_return_if_fail (IS_SERVER_TERM (term));

	priv = term->priv;

	vte_terminal_set_scrollback_lines (VTE_TERMINAL (priv->term), 0);
	vte_terminal_set_scrollback_lines (VTE_TERMINAL (priv->term), 5);
	vte_terminal_feed (VTE_TERMINAL (priv->term), "\e[2J\e[1H", 8);
}

static gboolean
st_command_key_press_cb (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	ServerTerm *term = SERVER_TERM (data);
	ServerTermPrivate *priv;
	
	priv = term->priv;

	switch (event->keyval) {
	case GDK_Up:
		if (priv->above != NULL) {
			g_free (priv->current);
			priv->current = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
			priv->below = g_list_prepend (priv->below, priv->current);
			priv->current = priv->above->data;
			priv->above = g_list_remove (priv->above, priv->current);
			priv->on_stack = TRUE;

			gtk_entry_set_text (GTK_ENTRY (widget), priv->current);
		}

		return TRUE;
		break;
	case GDK_Down:
		if (priv->below != NULL) {			
			priv->above = g_list_prepend (priv->above, priv->current);
			priv->current = priv->below->data;
			priv->below = g_list_remove (priv->below, priv->current);

			 gtk_entry_set_text (GTK_ENTRY (widget), priv->current);
		}
		
		return TRUE;
		break;
	}

	return FALSE;
}

static void
st_command_activate_cb (GtkWidget *widget, gpointer data)
{
	ServerTerm *term = SERVER_TERM (data);
	ServerTermPrivate *priv;
	GList *l;
	gchar *command;
	
	priv = term->priv;

        command = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
	if (command == NULL || !strcmp (command, ""))
		return;

	/* Push the current on to the stack */
	if (priv->current != NULL)
		priv->above = g_list_prepend (priv->above, priv->current);
	priv->current = NULL;

	/* Push the remaining items on to the stack */	
	for (l = priv->below; l != NULL; l = l->next) {
		gchar *command = l->data;
		
		priv->above = g_list_prepend (priv->above, command);
	}
	g_list_free (priv->below);
	priv->below = NULL;	

	/* Push the current command on to the stack */
	if (priv->on_stack)
		priv->above = g_list_remove (priv->above, priv->above->data);
	priv->above = g_list_prepend (priv->above, command);
	priv->on_stack = FALSE;
	
	gtk_signal_emit (GTK_OBJECT (term), 
			 term_signals[COMMAND_SIGNAL], 
			 command);

	gtk_entry_set_text (GTK_ENTRY (widget), "");
}


