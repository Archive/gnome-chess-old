/* 
 * Copyright (C) 1999 Robert Wilhelm
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef PGN_H
#define PGN_H

#include <stdio.h>
#include "game-view.h"
#include "movlist.h"

struct _Pgn_Tag
{
	char *event;
	char *site;
	char *date;
	char *round;
	char *white;
	char *black;
	char *result;
};
typedef struct _Pgn_Tag Pgn_Tag;

struct _pgn_info
{
	char *filename;

	GList *game_list;
	GList *game_tags;
};
typedef struct _pgn_info pgn_info;

pgn_info *pgn_open (const char *filename);
void pgn_select_game (pgn_info *pgn_info, GameView *view, gint row);
void pgn_save (const char *fname, GameView *view);

#endif


