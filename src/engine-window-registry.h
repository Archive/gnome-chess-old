/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-window-registry.h
 *j
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#ifndef _ENGINE_WINDOW_REGISTRY_H_
#define _ENGINE_WINDOW_REGISTRY_H_

#include <glib.h>
#include "engine-window.h"

G_BEGIN_DECLS

void engine_window_registry_init (void);
void engine_window_registry_add (EngineWindow *window);
void engine_window_registry_remove (EngineWindow *window);
int engine_window_registry_count (void);

G_END_DECLS

#endif

