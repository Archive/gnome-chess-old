/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* engine-pgn.c
 *
 * Copyright (C) 2001  JP Rosevear.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */

#include <config.h>
#include <gtk/gtk.h>
#include "dialogs.h"
#include "makros.h"
#include "pgn.h"
#include "engine-pgn.h"

static EngineClass *parent_class = NULL;

static void class_init (EnginePgnClass *klass);
static void init (EnginePgn *pgn);
static void finalize (GObject *obj);

static void game_selected_cb (GtkWidget *widget, int row, int column, GdkEventButton *event, gpointer user_data);

static void column_clicked_cb (GtkWidget *widget, int column,  gpointer user_data);

static void fill_menu (GtkMenuShell *shell, gint pos, gpointer data);

struct _EnginePgnPrivate 
{
	GtkWidget *view;

	GtkWidget *sw;
	GtkWidget *games;

	pgn_info *pgn_info;
	Pgn_Tag *tag;
};

GType
engine_pgn_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (EnginePgnClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,
			NULL,
			sizeof (EnginePgn),
			0,
			(GInstanceInitFunc) init
		};

		type = g_type_register_static (TYPE_ENGINE, "EnginePgn", &info, 0);
	}

	return type;
}

static void
class_init (EnginePgnClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = finalize;	
}


static void
init (EnginePgn *engine)
{
	EnginePgnPrivate *priv;
	static char *titles[] ={ N_("White"), N_("Black"), N_("Result")};
	int i;

	priv = g_new0 (EnginePgnPrivate, 1);

	engine->priv = priv;

	priv->sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->sw),
					GTK_POLICY_AUTOMATIC,     
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (priv->sw);
	
	for (i = 0; i < 3; i++)
		titles[i] = _(titles[i]);
	priv->games = gtk_clist_new_with_titles (3, titles);

	gtk_signal_connect (GTK_OBJECT (priv->games), "select-row",
			    (GtkSignalFunc) game_selected_cb,
			    engine);
	gtk_signal_connect (GTK_OBJECT (priv->games), "click-column",
			    (GtkSignalFunc) column_clicked_cb,
			    engine);
	gtk_widget_show (priv->games);
	
	gtk_container_add (GTK_CONTAINER (priv->sw), priv->games);

	priv->view = game_view_new (fill_menu, engine);
	engine_view_add_game_view (engine_get_engine_view (ENGINE (engine)), GAME_VIEW (priv->view));
}

static void
finalize (GObject *obj)
{
	EnginePgn *pgn = ENGINE_PGN (obj);
	EnginePgnPrivate *priv;

	priv = pgn->priv;
	
	g_free (priv);
	pgn->priv = NULL;
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}



GObject *
engine_pgn_new (const char *filename)
{
	EnginePgn *engine;
	EnginePgnPrivate *priv;
	GList *l;

	engine = g_object_new (ENGINE_TYPE_PGN, NULL);
	priv = engine->priv;
	
	priv->pgn_info = pgn_open (filename);

	if (priv->pgn_info) {
		for (l = priv->pgn_info->game_tags; l != NULL; l = l->next) {
			Pgn_Tag *tag = l->data;
			char *text[3] = { "", "","" };
			gint row;
		
			row = gtk_clist_append (GTK_CLIST (priv->games), text);
			gtk_clist_set_row_data(GTK_CLIST (priv->games), row, (gpointer)row);
			gtk_clist_set_text (GTK_CLIST (priv->games), row , 0, tag->white);
			gtk_clist_set_text (GTK_CLIST (priv->games), row , 1, tag->black);
			gtk_clist_set_text (GTK_CLIST (priv->games), row , 2, tag->result);
		}
	}
	engine_view_add_info_view (engine_get_engine_view (ENGINE (engine)),
				   g_basename (filename), priv->sw);

	gtk_clist_columns_autosize(GTK_CLIST(priv->games));

	return G_OBJECT (engine);
}

static void
game_selected_cb (GtkWidget *widget, int row, int column, GdkEventButton *event, gpointer user_data)
{
	EnginePgn *engine = user_data;
	EnginePgnPrivate *priv;
	gchar *text;
	GtkCList *clist = GTK_CLIST(widget);

        row = (int) gtk_clist_get_row_data(clist,row);

	priv = engine->priv;

	pgn_select_game (priv->pgn_info, GAME_VIEW (priv->view), row);
	move_list_move_end (MOVE_LIST (GAME_VIEW (priv->view)->movelist));

	priv->tag = g_list_nth_data(priv->pgn_info->game_tags, row);

	gtk_clist_get_text (GTK_CLIST (priv->games), row, 0, &text);
	game_view_set_white (GAME_VIEW (priv->view), text);
	
	gtk_clist_get_text (GTK_CLIST (priv->games), row, 1, &text);
	game_view_set_black (GAME_VIEW (priv->view), text);
	
	gtk_clist_get_text (GTK_CLIST (priv->games), row, 2, &text);
	
	if (!strcmp (text, "1-0"))
		game_view_set_status (GAME_VIEW (priv->view), GAME_VIEW_STATUS_WHITE);
	else if (!strcmp (text, "0-1"))
		game_view_set_status (GAME_VIEW (priv->view), GAME_VIEW_STATUS_BLACK);
	else if (!strcmp (text, "1/2-1/2"))
		game_view_set_status (GAME_VIEW (priv->view), GAME_VIEW_STATUS_DRAW);
}

static void
column_clicked_cb (GtkWidget *widget, int column,  gpointer user_data)
{
	GtkCList *clist = GTK_CLIST(widget);

	if (column == clist->sort_column) {
		if (clist->sort_type == GTK_SORT_ASCENDING)
			clist->sort_type = GTK_SORT_DESCENDING;
		else
			clist->sort_type = GTK_SORT_ASCENDING;
	}
	else
		gtk_clist_set_sort_column (clist, column);
	
	gtk_clist_set_compare_func (clist, NULL);
	gtk_clist_sort (clist);
}

static void
el_info (GtkWidget *widget, gpointer data)
{
	EnginePgnPrivate *priv;
	EnginePgn *engine = data;

	priv = engine->priv;
	priv->tag = dialog_pgn(priv->tag);
}

static void 
fill_menu (GtkMenuShell *shell, gint pos, gpointer data) 
{
	Engine *engine = data;

	GnomeUIInfo el_menu[] = 
	{

		GNOMEUIINFO_ITEM_DATA (N_("_PGN Info..."), NULL, el_info, engine, NULL),

		GNOMEUIINFO_END  
	};

	
	gnome_app_fill_menu (shell, el_menu, NULL, TRUE, pos);
	

}
