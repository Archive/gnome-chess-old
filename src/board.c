/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* board.c
 *
 * Copyright (C) 2001  JP Rosevear
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: JP Rosevear
 */


#include "makros.h"
#include "prefs.h"
#include "board.h"
#include "CwChessBoard.h"

struct _BoardPrivate {
	GtkWidget *frame;
	GtkWidget *cwboard;

	/* Movement information */
	gboolean moving;
	gint handle;
	gint col_from;
	gint row_from;
	double orig_x;
	double orig_y;
	double curr_x;
	double curr_y;
	Square from;
	Square to;
	Square selected_from;

	BoardLockType white_lock;
	BoardLockType black_lock;
	
	/* Position information */
	Position *pos;
	gboolean flip;	

	/* Board information */
	double size;

	char db[120];
	GdkPixbuf *pieces[12];
	GnomeCanvasItem *squares[64];
	guint light_rgba;
	guint dark_rgba;
};

/* Class signals */
enum {
	MOVE_SIGNAL,
	FLIP_SIGNAL,
	LAST_SIGNAL
};
static gint board_signals[LAST_SIGNAL] = { 0 };

enum {
  ARG_0,
  ARG_LIGHT_COLOR_RGBA,
  ARG_DARK_COLOR_RGBA,
};

/* Prototypes */
static void set_arg (GtkObject *object, GtkArg *arg, guint arg_id);
static void get_arg (GtkObject *object, GtkArg *arg, guint arg_id);
static void destroy (GtkObject *obj);
static void finalize (GObject *obj);

static GdkPixbuf *get_piece (Board *board, Piece piece);
static void draw_position (Board *board);

static void size_allocate_cb (GtkWidget *widget, GtkAllocation *allocation, gpointer data);
static gboolean event_cb (GtkWidget *widget, GdkEvent *event, gpointer data);

static GtkVBoxClass *parent_class = NULL;

static void
class_init (BoardClass *klass)
{
	GObjectClass *gobject_class;
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	gobject_class = G_OBJECT_CLASS (klass);
	object_class = GTK_OBJECT_CLASS (klass);
	widget_class = GTK_WIDGET_CLASS (klass);

	parent_class = gtk_type_class (GTK_TYPE_VBOX);
	
	board_signals[MOVE_SIGNAL]
		= gtk_signal_new ("move",
				  GTK_RUN_FIRST,
				  GTK_CLASS_TYPE (object_class),
				  GTK_SIGNAL_OFFSET (BoardClass, move),
				  gtk_marshal_NONE__INT_INT,
				  GTK_TYPE_NONE, 2,
				  GTK_TYPE_INT, GTK_TYPE_INT);

	board_signals[FLIP_SIGNAL]
		= gtk_signal_new ("flip",
				  GTK_RUN_FIRST,
				  GTK_CLASS_TYPE (object_class),
				  GTK_SIGNAL_OFFSET (BoardClass, flip),
				  gtk_marshal_NONE__BOOL,
				  GTK_TYPE_NONE, 1,
				  GTK_TYPE_BOOL);

	 gtk_object_add_arg_type ("Board::light_color_rgba",
				  GTK_TYPE_UINT,
				  GTK_ARG_READWRITE,
				  ARG_LIGHT_COLOR_RGBA);
	 gtk_object_add_arg_type ("Board::dark_color_rgba",
				  GTK_TYPE_UINT,
				  GTK_ARG_READWRITE,
				  ARG_DARK_COLOR_RGBA);
	
	 gobject_class->finalize = finalize;
	 object_class->set_arg = set_arg;
	 object_class->get_arg = get_arg;
	 object_class->destroy = destroy;
	 
         klass->move = NULL;
}


static void
load_pieces (Board *board)
{
	BoardPrivate *priv;
       	GdkPixbuf *pb;
	
	int i;
	
	char *filenames [] =
	{
		"P.png",
		"N.png",
		"B.png",
		"R.png",
		"Q.png",
		"K.png",
		"p.png",
		"n.png",
		"b.png",
		"r.png",
		"q.png",
		"k.png",
		NULL
	};
	
	priv = board->priv;
	
	for (i = 0; filenames[i] != NULL; i++) {
		char *fname;
		
		fname = g_build_filename (prefs_get_piecedir (), filenames[i], NULL);
		pb = gdk_pixbuf_new_from_file (fname, NULL);
		g_free (fname);
	
		/* If not installed */
		if (pb == NULL) {
			fname = g_build_filename ("../Sets/", filenames[i], NULL);
			pb = gdk_pixbuf_new_from_file (fname, NULL);
			g_free (fname);
		}
		priv->pieces[i] = pb;
	}

	priv->size = 8 * gdk_pixbuf_get_width (pb);
}

static void
init (Board *board)
{
	BoardPrivate *priv;
	int i;
	
	priv = g_new0 (BoardPrivate, 1);

	board->priv = priv;

	priv->frame = gtk_aspect_frame_new (NULL, 0.5, 0.5, 1.0, FALSE);
	gtk_box_pack_end (GTK_BOX (board), priv->frame, TRUE, TRUE, 0);
	gtk_widget_show (GTK_WIDGET (priv->frame));
	
	priv->cwboard = cw_chess_board_new();
	gtk_container_add (GTK_CONTAINER (priv->frame), GTK_WIDGET (priv->cwboard));
	gtk_widget_show (GTK_WIDGET (priv->cwboard));

	gtk_signal_connect (GTK_OBJECT (priv->cwboard), "event",
			    GTK_SIGNAL_FUNC (event_cb),
			    board);
	
	gtk_widget_show (GTK_WIDGET (board));

	priv->white_lock = BOARD_LOCK_COMPLETE;
	priv->black_lock = BOARD_LOCK_COMPLETE;
	
	priv->light_rgba = GNOME_CANVAS_COLOR (144, 238, 144);
	priv->dark_rgba = GNOME_CANVAS_COLOR (0, 100, 0);
	
	priv->flip = FALSE;
	for (i = 0; i < 120; i++)
		priv->db[i] = -1;

	priv->pos = POSITION (position_new_initial ());
	
	load_pieces (board);

	priv->handle = -1;
}



static void
set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
#if 0
	switch (arg_id) {
	case ARG_LIGHT_COLOR_RGBA:
		board_set_light_color (BOARD (object), GTK_VALUE_UINT (*arg));
		break;
	case ARG_DARK_COLOR_RGBA:
		board_set_dark_color (BOARD (object), GTK_VALUE_UINT (*arg));
		break;
	}
#endif
}

static void
get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	switch (arg_id) {
	case ARG_LIGHT_COLOR_RGBA:
		GTK_VALUE_UINT (*arg) = board_get_light_color (BOARD (object));
		break;
	case ARG_DARK_COLOR_RGBA:
		GTK_VALUE_UINT (*arg) = board_get_dark_color (BOARD (object));
		break;
	}
}

static void
destroy (GtkObject *obj)
{
	Board *board;
	BoardPrivate *priv;

	board = BOARD (obj);
	priv = board->priv;

	if (priv->pos != NULL)
		g_object_unref (priv->pos);
	priv->pos = NULL;

	if (GTK_OBJECT_CLASS (parent_class)->destroy)	
		GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}

static void
finalize (GObject *obj)
{
	Board *board;
	BoardPrivate *priv;

	board = BOARD (obj);
	priv = board->priv;

	g_free (priv);
}

GtkType
board_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static const GtkTypeInfo info =
		{
			"Board",
			sizeof (Board),
			sizeof (BoardClass),
			(GtkClassInitFunc) class_init,
			(GtkObjectInitFunc) init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GTK_TYPE_VBOX, &info);
	}

	return type;
}

GtkWidget *
board_new () 
{
	GtkWidget *w;
	
	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());

	w = GTK_WIDGET (gtk_type_new (TYPE_BOARD));

	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();

	return w;
}

Position *
board_get_position (Board *board) 
{
	BoardPrivate *priv;
	
	priv = board->priv;
	
	return priv->pos;
}

void 
board_set_position (Board *board, Position *pos) 
{
	BoardPrivate *priv;
	Position     *old_pos;
	
	priv = board->priv;

	old_pos = priv->pos;
	priv->pos = position_copy (pos);
	g_object_unref (old_pos);

	draw_position (board);
}

static void
set_colors (Board *board) 
{
	BoardPrivate *priv;
	int i, j;
	
	priv = board->priv;

	for (i = 0; i < 8; i++)
		for (j = 0; j < 8; j++)
			gnome_canvas_item_set (priv->squares[i * 8 +j],
					       "fill_color_rgba", 
					       ((i + j) % 2) ? priv->dark_rgba 
					       : priv->light_rgba,
					       NULL);
}

guint32 
board_get_light_color (Board *board)
{
	BoardPrivate *priv;
	
	priv = board->priv;

	return priv->light_rgba;
}

void 
board_set_light_color (Board *board, guint32 rgba)
{
#if 0
	BoardPrivate *priv;
	
	priv = board->priv;

	priv->light_rgba = rgba;

	set_colors (board);

	if (GTK_WIDGET_REALIZED (board))
		gtk_widget_queue_draw (GTK_WIDGET (board));
#endif
}

guint32 
board_get_dark_color (Board *board)
{
#if 0
	BoardPrivate *priv;
	
	priv = board->priv;

	return priv->dark_rgba;
#endif
}

void 
board_set_dark_color (Board *board, guint32 rgba)
{
#if 0
	BoardPrivate *priv;
	
	priv = board->priv;

	priv->dark_rgba = rgba;

	set_colors (board);

	if (GTK_WIDGET_REALIZED (board))
		gtk_widget_queue_draw (GTK_WIDGET (board));
#endif
}

BoardLockType
board_get_white_lock (Board *board)
{

	BoardPrivate *priv;
	
	priv = board->priv;

	return priv->white_lock;

}

void
board_set_white_lock (Board *board, BoardLockType lock)
{

	BoardPrivate *priv;
	
	priv = board->priv;

	priv->white_lock = lock;	

}


BoardLockType
board_get_black_lock (Board *board)
{
	BoardPrivate *priv;
	
	priv = board->priv;

	return priv->black_lock;
}

void
board_set_black_lock (Board *board, BoardLockType lock)
{

	BoardPrivate *priv;
	
	priv = board->priv;

	priv->black_lock = lock;

}


void 
board_set_flip (Board *board, gboolean flipped) 
{
	BoardPrivate *priv;
	
	priv = board->priv;

	priv->flip = flipped;

	gtk_signal_emit (GTK_OBJECT (board),
			 board_signals[FLIP_SIGNAL],
			 priv->flip);
}

void 
board_flip (Board *board) 
{
	BoardPrivate *priv;
	
	priv = board->priv;

	if (priv->flip)
		priv->flip = FALSE;
	else
		priv->flip = TRUE;

	gtk_signal_emit (GTK_OBJECT (board),
			 board_signals[FLIP_SIGNAL],
			 priv->flip);
}

void
board_move (Board *board, Square from, Square to)
{
	BoardPrivate *priv;
	
	priv = board->priv;

	position_move (priv->pos, from, to);
	draw_position (board);
}

static int
get_square (double x, double y, gboolean flip)
{
	Square from;

	if (!flip)
		from = A1 + (Square)x  + 10 * (Square)y;
	else
		from = H8 - (Square)x  - 10 * (Square)y;

	return from;
}

static GdkPixbuf *
get_piece (Board *board, Piece piece) 
{
	BoardPrivate *priv;
	
	priv = board->priv;
	
	if (piece == EMPTY)
		return NULL;
	
	if (WPIECE (piece))
		return priv->pieces[piece - WP];
	else
		return priv->pieces[piece - BP + 6];
}


static gboolean
try_move (Board *board, CwChessBoard *cw_board)
{
	BoardPrivate *priv;
	
	priv = board->priv;
	
	priv->to = position_move_normalize (priv->pos, priv->from, priv->to);
	if (priv->to) {
		position_move (priv->pos, priv->from, priv->to);
		draw_position (board);
		gtk_signal_emit (GTK_OBJECT (board),
				 board_signals [MOVE_SIGNAL],
				 priv->from,
				 priv->to);
		return TRUE;		
	}
	return FALSE;
}

static void
draw_piece(Board *board, int square, int piece)
{
	BoardPrivate *priv = board->priv;


	int col, row;

	col = square % 10 - 1;
	row = square / 10 - 2;

	switch(piece)
	{
		case WP:  piece = white_pawn; break;
		case BP:  piece = black_pawn; break;
		case WN:  piece = white_knight; break;
		case BN:  piece = black_knight; break;
		case WB:  piece = white_bishop; break;
		case BB:  piece = black_bishop; break;
		case WR:  piece = white_rook; break;
		case BR:  piece = black_rook; break;
		case WQ:  piece = white_queen; break;
		case BQ:  piece = black_queen; break;
		case WK:  piece = white_king; break;
		case BK:  piece = black_king; break;
	}

	cw_chess_board_set_square( priv->cwboard, col, row, piece);
}

static void
draw_position (Board *board) 
{
	BoardPrivate *priv;
	int n, n2, i, j;

	priv = board->priv;
	
	for (i = 0; i < 8; i++)
		for (j = 0; j < 8; j++) {
			n  = H8 - i * 10 - j;
			if (!priv->flip) 
				n2 = n;
			else
				n2 = A1 + i*10 + j;

			if (priv->pos->square[n2] != priv->db[n]) {
				draw_piece (board, n, priv->pos->square[n2]);
				priv->db[n] = priv->pos->square[n2];
			}
		}
}

static gboolean
event_cb (GtkWidget *widget, GdkEvent *event, gpointer data)
{
	Board *board;
	BoardPrivate *priv;
	CwChessBoard *chess_board;
	Square from;
	short to_move;
	double drop_x, drop_y;
	double new_x, new_y;

	chess_board = CW_CHESS_BOARD (widget);
	board = BOARD (gtk_widget_get_ancestor (GTK_WIDGET(chess_board), TYPE_BOARD));
	priv = board->priv;
	
	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button != 1)
			break;		

		priv->orig_x = priv->curr_x = event->button.x;
		priv->orig_y = priv->curr_y = event->button.y;

		gint col = cw_chess_board_x2col(chess_board, priv->curr_x);
		gint row = cw_chess_board_y2row(chess_board, priv->curr_y);

		CwChessBoardCode code = cw_chess_board_get_square(chess_board,col,row);


        if (code > 1)   // The least significant bit reflects the color of the piece.
                        // A code <= 1 means an empty square.
        {
          // Make the square empty.
          cw_chess_board_set_square(chess_board, col, row, empty_square);
          priv->col_from = col;
	  priv->row_from = row;
          // Put the piece under the mouse pointer.
          double hsside = 0.5 * chess_board->sside;
          double fraction = hsside - (gint)hsside;
          priv->handle = cw_chess_board_add_floating_piece(chess_board, code,
              priv->curr_x  - fraction, priv->curr_y - fraction, TRUE);

        }

			
		from = get_square (col,
				   row,
				   priv->flip);

		if (WPIECE (priv->pos->square[from]) && priv->white_lock == BOARD_LOCK_COMPLETE)
			break;
		if (BPIECE (priv->pos->square[from]) && priv->black_lock == BOARD_LOCK_COMPLETE)
			break;

		priv->from = from;		
		priv->to = from;
		priv->moving = TRUE;
		break;

	case GDK_BUTTON_RELEASE:

    if ( priv->handle != -1)
    {
      // Find the square under the mouse, if any.
      gint col = cw_chess_board_x2col(chess_board, event->button.x);
      gint row = cw_chess_board_y2row(chess_board, event->button.y);


      priv->to = get_square ( col,
				   row,
				   priv->flip);

      int piece = cw_chess_board_get_floating_piece(chess_board, priv->handle);
      // Remove the piece under the mouse pointer.
      cw_chess_board_remove_floating_piece(chess_board, priv->handle);

      if (!try_move(board, chess_board))
      {
	// Put the piece back on the old square.
	      cw_chess_board_set_square(chess_board, priv->col_from, priv->row_from, piece);
      }

      priv->handle = -1;
      return TRUE;
    }

	default:
		break;
	}
	
	return FALSE;
}
