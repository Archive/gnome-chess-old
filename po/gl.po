# Galician translation of gnome-chess.
# Copyright (C) 1999-2001 Jes�s Bravo �lvarez.
# Jes�s Bravo �lvarez <jba@pobox.com>, 1999-2001.
#
# Proxecto Trasno - Adaptaci�n do software libre � lingua galega:  Se desexas
# colaborar connosco, podes atopar m�is informaci�n en http://www.trasno.net
#
# First Version: 1999-12-30 22:50+0100
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-chess\n"
"POT-Creation-Date: 2002-07-18 14:41-0400\n"
"PO-Revision-Date: 2001-08-19 16:01+0200\n"
"Last-Translator: Jes�s Bravo �lvarez <jba@pobox.com>\n"
"Language-Team: Galician <trasno@ceu.fi.udc.es>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#. Players
#: src/board_info.c:125 src/engine-pgn.c:101 src/movlist.c:130
msgid "White"
msgstr "Brancas"

#: src/board_info.c:129 src/engine-pgn.c:101 src/movlist.c:130
msgid "Black"
msgstr "Negras"

#: src/board_info.c:338 src/dialogs/prefs.glade.h:28
msgid "Playing"
msgstr "Xogando"

#: src/board_info.c:341
msgid "Paused"
msgstr "Pausa"

#: src/board_info.c:344
msgid "Aborted"
msgstr "Abortado"

#: src/board_info.c:347
msgid "1-0"
msgstr "1-0"

#: src/board_info.c:350
msgid "0-1"
msgstr "0-1"

#: src/board_info.c:353
msgid "1/2-1/2"
msgstr "1/2-1/2"

#: src/board-window.c:151
#, c-format
msgid "Board %d"
msgstr "Taboleiro %d"

#: src/board_window_menu.c:48 src/dialogs/prefs.glade.h:31
msgid "Programs"
msgstr "Programas"

#: src/board_window_menu.c:50
msgid "_Servers"
msgstr "_Servidores"

#: src/board_window_menu.c:270
#, fuzzy
msgid "Gnome Chess"
msgstr "Preferencias do Xadrez de Gnome"

#: src/engine-pgn.c:101
msgid "Result"
msgstr "Resultado"

#: src/engine-pgn.c:243
msgid "_PGN Info..."
msgstr "Informaci�n _PGN..."

#: src/engine_ics.c:330
msgid "_Draw"
msgstr "_Empate"

#: src/engine_ics.c:334
msgid "_Flag"
msgstr "_Bandeira"

#: src/engine_ics.c:339
msgid "_Resign"
msgstr "_Abandonar"

#: src/engine_ics.c:345
msgid "Re_match"
msgstr "Xogar de _novo"

#: src/engine_local.c:356
msgid "Computer _White"
msgstr "Ordenador _brancas"

#: src/engine_local.c:360
msgid "Computer _Black"
msgstr "Ordenador _negras"

#: src/engine_local.c:364
msgid "_Analyze Mode"
msgstr "Modo de _an�lise"

#: src/engine_local.c:381
msgid "_Mode"
msgstr "_Modo"

#: src/engine_local.c:383
msgid "_Level..."
msgstr "_Nivel..."

#: src/engine_local.c:387
msgid "_Take Back"
msgstr "Voltar _atr�s"

#: src/engine_local.c:392
msgid "Move _Now"
msgstr "Mover _agora"

#: src/engine_local.c:597
msgid "Local Engine connection died"
msgstr ""

#: src/main.c:48
msgid "Enables some debugging functions"
msgstr "Activa algunhas funci�ns de debug"

#: src/main.c:48
msgid "LEVEL"
msgstr "NIVEL"

#: src/main.c:50
msgid "First Chess Program"
msgstr "Primeiro Programa de Xadrez"

#: src/main.c:50
msgid "PROGRAM"
msgstr "PROGRAMA"

#: src/main.c:52
msgid "Start in ICS mode"
msgstr "Comezar en modo ICS"

#: src/main.c:54
msgid "ICS Server"
msgstr "Servidor ICS"

#: src/main.c:54
msgid "SERVER"
msgstr "SERVIDOR"

#: src/main.c:56
msgid "ICS User"
msgstr "Usuario ICS"

#: src/main.c:56
msgid "USERNAME"
msgstr "USUARIO"

#: src/main.c:58
msgid "ICS Password"
msgstr "Contrasinal ICS"

#: src/main.c:58
msgid "PASSWORD"
msgstr "CONTRASINAL"

#: src/main.c:60
msgid "ICS Port"
msgstr "Porto ICS"

#: src/main.c:60
msgid "PORT"
msgstr "PORTO"

#: src/main.c:62
msgid "ICS Telnet Programm"
msgstr "Programa de Telnet ICS"

#: src/main.c:62
msgid "TELNET"
msgstr "TELNET"

#: src/movlist.c:130
msgid "Number"
msgstr "N�mero"

#: src/dialogs/level.glade.h:1
msgid "Level"
msgstr "Nivel"

#: src/dialogs/level.glade.h:2
msgid "Number of moves"
msgstr "N�mero de movementos"

#: src/dialogs/level.glade.h:3
msgid "Time (in minutes)"
msgstr "Tempo (en minutos)"

#: src/dialogs/login.glade.h:1
msgid "Chess Server Login"
msgstr "Login no servidor de xadrez"

#: src/dialogs/login.glade.h:2
msgid "Password:"
msgstr "Contrasinal:"

#: src/dialogs/login.glade.h:3
msgid "Username:"
msgstr "Nome de usuario:"

#: src/dialogs/prefs.glade.h:1
msgid "*"
msgstr ""

#: src/dialogs/prefs.glade.h:2
msgid "Add"
msgstr "Engadir"

#: src/dialogs/prefs.glade.h:3
msgid "Always promote to queen"
msgstr "Promocionar sempre a dama"

#: src/dialogs/prefs.glade.h:4
msgid "Autoflag"
msgstr "Autoflag"

#: src/dialogs/prefs.glade.h:5
msgid "Background Color:"
msgstr ""

#: src/dialogs/prefs.glade.h:6
msgid "Beep when a move is made"
msgstr "Pitar ao facer un movemento"

#: src/dialogs/prefs.glade.h:7
msgid "Board"
msgstr "Taboleiro"

#: src/dialogs/prefs.glade.h:8
msgid "Board & Pieces"
msgstr "Taboleiro e pezas"

#: src/dialogs/prefs.glade.h:9
msgid "Color Palette:"
msgstr ""

#: src/dialogs/prefs.glade.h:10
msgid "Colors"
msgstr ""

#: src/dialogs/prefs.glade.h:11
msgid "Connect Program"
msgstr "Programa de conexi�n"

#: src/dialogs/prefs.glade.h:12
msgid "Dark Coloured Squares"
msgstr "Cadros de cor escura"

#: src/dialogs/prefs.glade.h:13
msgid "Default Connect Program"
msgstr "Programa de conexi�n por omisi�n"

#: src/dialogs/prefs.glade.h:14
msgid "Delete"
msgstr "Borrar"

#: src/dialogs/prefs.glade.h:15
msgid "Foreground Color:"
msgstr ""

#: src/dialogs/prefs.glade.h:16
msgid "Game Play"
msgstr "Xogo"

#: src/dialogs/prefs.glade.h:17
msgid "Gnome Chess Preferences"
msgstr "Preferencias do Xadrez de Gnome"

#: src/dialogs/prefs.glade.h:18
msgid "Light Coloured Squares"
msgstr "Cadros de cor clara"

#: src/dialogs/prefs.glade.h:19
msgid "Location"
msgstr "Localizaci�n"

#: src/dialogs/prefs.glade.h:20
msgid "Modify"
msgstr "Modificar"

#: src/dialogs/prefs.glade.h:21
msgid "Move list in Coordinate Notation"
msgstr "Lista de movementos en notaci�n coordinada"

#: src/dialogs/prefs.glade.h:22
msgid "Move list in Standard Algebraic Notation"
msgstr "Lista de movementos en notaci�n alxebraica est�ndar"

#: src/dialogs/prefs.glade.h:23
msgid "Name"
msgstr "Nome"

#: src/dialogs/prefs.glade.h:24
msgid "Password"
msgstr "Contrasinal"

#: src/dialogs/prefs.glade.h:25
msgid "Pick a color"
msgstr "Escoller unha cor"

#: src/dialogs/prefs.glade.h:26
msgid "Piece directory..."
msgstr "Directorio de pezas..."

#: src/dialogs/prefs.glade.h:27
msgid "Pieces"
msgstr "Pezas"

#: src/dialogs/prefs.glade.h:29
msgid "Port"
msgstr "Porto"

#: src/dialogs/prefs.glade.h:30
#, fuzzy
msgid "Program Information"
msgstr "Promoci�n"

#: src/dialogs/prefs.glade.h:32
msgid "Server"
msgstr "Servidor"

#: src/dialogs/prefs.glade.h:33
msgid "Server Information"
msgstr ""

#: src/dialogs/prefs.glade.h:34
msgid "Servers"
msgstr "Servidores"

#: src/dialogs/prefs.glade.h:35
msgid "Username"
msgstr "Nome de usuario"

#: src/dialogs/promotion.glade.h:1
msgid "Bishop"
msgstr "Alfil"

#: src/dialogs/promotion.glade.h:2
msgid "Knight"
msgstr "Cabaleiro"

#: src/dialogs/promotion.glade.h:3
msgid "Promote to which piece?"
msgstr "�Promocionar a que peza?"

#: src/dialogs/promotion.glade.h:4
msgid "Promotion"
msgstr "Promoci�n"

#: src/dialogs/promotion.glade.h:5
msgid "Queen"
msgstr "Dama"

#: src/dialogs/promotion.glade.h:6
msgid "Rook"
msgstr "Torre"

#~ msgid "Path"
#~ msgstr "Cami�o"
