# English (British) translation.
# Copyright (C) 2004 THE gnome-chess'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-chess package.
# Gareth Owen <gowen72@yahoo.com>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-chess\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-04-10 10:28-0400\n"
"PO-Revision-Date: 2004-04-10 10:30-0400\n"
"Last-Translator: Gareth Owen <gowen72@yahoo.comg>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gnome-chess.desktop.in.h:1
msgid "Chess"
msgstr "Chess"

#: gnome-chess.desktop.in.h:2
msgid "Play Chess"
msgstr "Play Chess"

#: src/board-window.c:208 src/engine-view.c:157
#, c-format
msgid "Board %d"
msgstr "Board %d"

#: src/board_window_menu.c:52 src/dialogs/prefs.glade.h:30
msgid "Programs"
msgstr "Programs"

#: src/board_window_menu.c:54 src/engine-window.c:349
msgid "_Servers"
msgstr "_Servers"

#: src/clock.c:66
msgid "Unknown"
msgstr "Unknown"

#: src/clock.c:145 src/engine-pgn.c:94 src/game-view.c:165 src/movlist.c:130
msgid "White"
msgstr "White"

#: src/dialogs/level.glade.h:1
msgid "Level"
msgstr "Level"

#: src/dialogs/level.glade.h:2
msgid "Number of moves"
msgstr "Number of moves"

#: src/dialogs/level.glade.h:3
msgid "Time (in minutes)"
msgstr "Time (in minutes)"

#: src/dialogs/login.glade.h:1
msgid "Chess Server Login"
msgstr "Chess Server Login"

#: src/dialogs/login.glade.h:2
msgid "Password:"
msgstr "Password:"

#: src/dialogs/login.glade.h:3
msgid "Username:"
msgstr "Username:"

#: src/dialogs/pgn.glade.h:1
msgid "Black:"
msgstr "Black:"

#: src/dialogs/pgn.glade.h:2
msgid "Date:"
msgstr "Date:"

#: src/dialogs/pgn.glade.h:3
msgid "Event:"
msgstr "Event:"

#: src/dialogs/pgn.glade.h:4
msgid "PGN Header"
msgstr "PGN Header"

#: src/dialogs/pgn.glade.h:5
msgid "Result:"
msgstr "Result:"

#: src/dialogs/pgn.glade.h:6
msgid "Round:"
msgstr "Round:"

#: src/dialogs/pgn.glade.h:7
msgid "Site:"
msgstr "Site:"

#: src/dialogs/pgn.glade.h:8
msgid "White:"
msgstr "White:"

#: src/dialogs/prefs.glade.h:1
msgid "*"
msgstr "*"

#: src/dialogs/prefs.glade.h:2
msgid "Add"
msgstr "Add"

#: src/dialogs/prefs.glade.h:3
msgid "Always promote to queen"
msgstr "Always promote to queen"

#: src/dialogs/prefs.glade.h:4
msgid "Autoflag"
msgstr "Autoflag"

#: src/dialogs/prefs.glade.h:5
msgid "Background Color:"
msgstr "Background Colour:"

#: src/dialogs/prefs.glade.h:6
msgid "Beep when a move is made"
msgstr "Beep when a move is made"

#: src/dialogs/prefs.glade.h:7
msgid "Board"
msgstr "Board"

#: src/dialogs/prefs.glade.h:8
msgid "Board & Pieces"
msgstr "Board & Pieces"

#: src/dialogs/prefs.glade.h:9
msgid "Color Palette:"
msgstr "Colour Palette:"

#: src/dialogs/prefs.glade.h:10
msgid "Colors"
msgstr "Colours"

#: src/dialogs/prefs.glade.h:11 src/gnome-chess.schemas.in.in.h:5
msgid "Connect Program"
msgstr "Connect Program"

#: src/dialogs/prefs.glade.h:12
msgid "Coordinate Notation"
msgstr "Coordinate Notation"

#: src/dialogs/prefs.glade.h:13
msgid "Dark Coloured Squares"
msgstr "Dark Coloured Squares"

#: src/dialogs/prefs.glade.h:14
msgid "Default Connect Program"
msgstr "Default Connect Program"

#: src/dialogs/prefs.glade.h:15
msgid "Delete"
msgstr "Delete"

#: src/dialogs/prefs.glade.h:16
msgid "Foreground Color:"
msgstr "Foreground Colour:"

#: src/dialogs/prefs.glade.h:17
msgid "Game Play"
msgstr "Game Play"

#: src/dialogs/prefs.glade.h:18
msgid "Gnome Chess Preferences"
msgstr "Gnome Chess Preferences"

#: src/dialogs/prefs.glade.h:19
msgid "Light Coloured Squares"
msgstr "Light Coloured Squares"

#: src/dialogs/prefs.glade.h:20
msgid "Location"
msgstr "Location"

#: src/dialogs/prefs.glade.h:21
msgid "Modify"
msgstr "Modify"

#: src/dialogs/prefs.glade.h:22
msgid "Name"
msgstr "Name"

#: src/dialogs/prefs.glade.h:23
msgid "Password"
msgstr "Password"

#: src/dialogs/prefs.glade.h:24
msgid "Pick a color"
msgstr "Pick a colour"

#: src/dialogs/prefs.glade.h:25
msgid "Piece directory..."
msgstr "Piece directory..."

#: src/dialogs/prefs.glade.h:26
msgid "Pieces"
msgstr "Pieces"

#: src/dialogs/prefs.glade.h:27 src/game-view.c:418
msgid "Playing"
msgstr "Playing"

#: src/dialogs/prefs.glade.h:28
msgid "Port"
msgstr "Port"

#: src/dialogs/prefs.glade.h:29
msgid "Program Information"
msgstr "Program Information"

#: src/dialogs/prefs.glade.h:31
msgid "Server"
msgstr "Server"

#: src/dialogs/prefs.glade.h:32
msgid "Server Information"
msgstr "Server Information"

#: src/dialogs/prefs.glade.h:33
msgid "Servers"
msgstr "Servers"

#: src/dialogs/prefs.glade.h:34
msgid "Standard Algebraic Notation"
msgstr "Standard Algebraic Notation"

#: src/dialogs/prefs.glade.h:35
msgid "Username"
msgstr "Username"

#: src/dialogs/promotion.glade.h:1
msgid "Bishop"
msgstr "Bishop"

#: src/dialogs/promotion.glade.h:2
msgid "Knight"
msgstr "Knight"

#: src/dialogs/promotion.glade.h:3
msgid "Promote to which piece?"
msgstr "Promote to which piece?"

#: src/dialogs/promotion.glade.h:4
msgid "Promotion"
msgstr "Promotion"

#: src/dialogs/promotion.glade.h:5
msgid "Queen"
msgstr "Queen"

#: src/dialogs/promotion.glade.h:6
msgid "Rook"
msgstr "Rook"

#: src/engine-pgn.c:94 src/game-view.c:140 src/movlist.c:130
msgid "Black"
msgstr "Black"

#: src/engine-pgn.c:94
msgid "Result"
msgstr "Result"

#: src/engine-pgn.c:243
msgid "_PGN Info..."
msgstr "_PGN Info..."

#: src/engine-window.c:219
msgid "Gnome Chess"
msgstr "Gnome Chess"

#: src/engine-window.c:347
msgid "_Programs"
msgstr "_Programs"

#: src/engine_ics.c:369
msgid "_Draw"
msgstr "_Draw"

#: src/engine_ics.c:373
msgid "_Flag"
msgstr "_Flag"

#: src/engine_ics.c:378
msgid "_Resign"
msgstr "_Resign"

#: src/engine_ics.c:384
msgid "Re_match"
msgstr "Re_match"

#: src/engine_local.c:398
msgid "Computer _White"
msgstr "Computer _White"

#: src/engine_local.c:402
msgid "Computer _Black"
msgstr "Computer _Black"

#: src/engine_local.c:406
msgid "_Analyze Mode"
msgstr "_Analyse Mode"

#: src/engine_local.c:423
msgid "_Mode"
msgstr "_Mode"

#: src/engine_local.c:425
msgid "_Level..."
msgstr "_Level..."

#: src/engine_local.c:429
msgid "_Take Back"
msgstr "_Take Back"

#: src/engine_local.c:434
msgid "Move _Now"
msgstr "Move _Now"

#: src/engine_local.c:618
msgid "Local Engine connection died"
msgstr "Local Engine connection died"

#. Game Information
#: src/game-view.c:173
msgid "White v. Black"
msgstr "White v. Black"

#: src/game-view.c:275
#, c-format
msgid "%s v. %s"
msgstr "%s v. %s"

#: src/game-view.c:421
msgid "Viewing"
msgstr "Viewing"

#: src/game-view.c:424
msgid "Paused"
msgstr "Paused"

#: src/game-view.c:427
msgid "Aborted"
msgstr "Aborted"

#: src/game-view.c:430
msgid "1-0"
msgstr "1-0"

#: src/game-view.c:433
msgid "0-1"
msgstr "0-1"

#: src/game-view.c:436
msgid "1/2-1/2"
msgstr "1/2-1/2"

#: src/gnome-chess.schemas.in.in.h:1
msgid "Auto Flag"
msgstr "Auto Flag"

#: src/gnome-chess.schemas.in.in.h:2
msgid "Beep on Move"
msgstr "Beep on Move"

#: src/gnome-chess.schemas.in.in.h:3
msgid "Color of dark squares on board"
msgstr "Colour of dark squares on board"

#: src/gnome-chess.schemas.in.in.h:4
msgid "Color of light squares on board"
msgstr "Colour of light squares on board"

#: src/gnome-chess.schemas.in.in.h:6
msgid "Determines whether or not to beep when there is a move on the board"
msgstr "Determines whether or not to beep when there is a move on the board"

#: src/gnome-chess.schemas.in.in.h:7
msgid "Directory containing pieces"
msgstr "Directory containing pieces"

#: src/gnome-chess.schemas.in.in.h:8
msgid "Notation"
msgstr "Notation"

#: src/gnome-chess.schemas.in.in.h:9
msgid "Notation type to use in the move list"
msgstr "Notation type to use in the move list"

#: src/gnome-chess.schemas.in.in.h:10
msgid "Palette Colors"
msgstr "Palette Colours"

#: src/gnome-chess.schemas.in.in.h:11
msgid "Promote to Queen"
msgstr "Promote to Queen"

#: src/main.c:46
msgid "Enables some debugging functions"
msgstr "Enables some debugging functions"

#: src/main.c:46
msgid "LEVEL"
msgstr "LEVEL"

#: src/main.c:48
msgid "First Chess Program"
msgstr "First Chess Program"

#: src/main.c:48
msgid "PROGRAM"
msgstr "PROGRAM"

#: src/main.c:50
msgid "Start in ICS mode"
msgstr "Start in ICS mode"

#: src/main.c:52
msgid "ICS Server"
msgstr "ICS Server"

#: src/main.c:52
msgid "SERVER"
msgstr "SERVER"

#: src/main.c:54
msgid "ICS User"
msgstr "ICS User"

#: src/main.c:54
msgid "USERNAME"
msgstr "USERNAME"

#: src/main.c:56
msgid "ICS Password"
msgstr "ICS Password"

#: src/main.c:56
msgid "PASSWORD"
msgstr "PASSWORD"

#: src/main.c:58
msgid "ICS Port"
msgstr "ICS Port"

#: src/main.c:58
msgid "PORT"
msgstr "PORT"

#: src/main.c:60
msgid "ICS Telnet Programm"
msgstr "ICS Telnet Programm"

#: src/main.c:60
msgid "TELNET"
msgstr "TELNET"

#: src/movlist.c:130
msgid "Number"
msgstr "Number"
