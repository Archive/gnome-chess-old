AC_INIT(src/engine.h)

AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(gnome-chess, 0.5.0)
AM_MAINTAINER_MODE

AC_PROG_INTLTOOL

AC_PROG_CC
AC_ISC_POSIX
AC_STDC_HEADERS
AC_ARG_PROGRAM
AM_PROG_LIBTOOL
AM_PROG_LEX

GNOME_COMPILE_WARNINGS
CFLAGS="$CFLAGS $WARN_CFLAGS -std=c99"

GCONF_REQUIRED="gconf-2.0 >= 2.2.0"
GDK_PIXBUF_REQUIRED="gdk-pixbuf-2.0 >= 2.0.5"
LIBGNOME_REQUIRED="libgnome-2.0 >= 2.0.0"
LIBGNOMECANVAS_REQUIRED="libgnomecanvas-2.0 >= 2.0.0"
LIBGNOMEUI_REQUIRED="libgnomeui-2.0 >= 2.0.0"
LIBGLADE_REQUIRED="libglade-2.0 >= 2.0.0"
VTE_REQUIRED="vte >= 0.10.15"
CAIRO_REQUIRED=1.2.0

PKG_CHECK_MODULES(GNOME_CHESS, 
[
	$GCONF_REQUIRED
	$GDK_PIXBUF_REQUIRED
	$LIBGNOME_REQUIRED
	$LIBGNOMECANVAS_REQUIRED
	$LIBGNOMEUI_REQUIRED
	$LIBGLADE_REQUIRED
	$VTE_REQUIRED
])

AC_SUBST(GNOME_CHESS_CFLAGS)
AC_SUBST(GNOME_CHESS_LIBS)

dnl ***********
dnl GConf stuff
dnl ***********
AC_PATH_PROG(GCONFTOOL, gconftool-2, no)

if test x"$GCONFTOOL" = xno; then
  AC_MSG_ERROR([gconftool-2 executable not found in your path - should be installed with GConf])
fi

AM_GCONF_SOURCE_2

dnl **************************************************
dnl * Marshaller generation
dnl **************************************************
GLIB_GENMARSHAL="`$PKG_CONFIG --variable=glib_genmarshal glib-2.0`"
AC_SUBST(GLIB_GENMARSHAL)

dnl **************************************************
dnl * internationalization support
dnl **************************************************
GETTEXT_PACKAGE=gnome-chess
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [The gettext package])

ALL_LINGUAS="az ca cs da de el en_CA en_GB es fr ga gl hr it ja lt mn ms nb nl nn pa pl pt pt_BR ro ru sq sr sr@Latn sv tr uk vi zh_CN zh_TW"
AM_GLIB_GNU_GETTEXT
AM_GLIB_DEFINE_LOCALEDIR([GNOME_CHESS_LOCALEDIR])

dnl ******************************
dnl Scrollkeeper checking
dnl ******************************
AC_PATH_PROG(SCROLLKEEPER_CONFIG, scrollkeeper-config,no)
if test x$SCROLLKEEPER_CONFIG = xno; then
  AM_CONDITIONAL(BUILD_DOCS, false)
else
  AM_CONDITIONAL(BUILD_DOCS, true)
fi
AM_CONDITIONAL(BUILD_DOCS, false)

AC_OUTPUT(
gnome-chess.spec
Makefile
src/Makefile
src/dialogs/Makefile
Sets/Makefile
omf-install/Makefile
po/Makefile.in
doc/Makefile
doc/C/Makefile
)

echo "

Configuration:

	CFLAGS:	${GNOME_CHESS_CFLAGS}

	LIBS:	${GNOME_CHESS_LIBS}

"
